---json
{
    "title": "Đặt trước Honor 10 Xanh Lá - Phiên bản màu đặc biệt, cấu hình mạnh",
    "slug": "",
    "description": "Từ ngày 01/07 - 06/07 đặt trước Honor 10 phiên bản màu xanh đặc biệt chụp hình chuyên nghiệp với camera AI 24MP tại Viễn Thông A nhận ngay bộ quà trị giá khủng lên đến 3tr7 cùng ưu đãi  trả góp 0%. Liên hệ : 1900 6766",
    "layout": "honor_10.html",
    "permalink" : false,
    "version": '1.2.9'
}
---