---json
{
    "title": "Đặt trước Huawei P20 Pro - Smartphone sở hữu 3 camera chụp hình chuyên nghiệp",
    "slug": "",
    "description": "Từ ngày 15/05 - 25/5 đặt trước smartphone Huawei P20 Pro sở hữu 3 camera chụp hình chuyên nghiệp tại Viễn Thông A nhận ngay bộ quà trị giá khủng lên đến 6tr cùng ưu đãi  trả góp 0%. Liên hệ : 1900 6766",
    "layout": "huawei_pro.html",
    "permalink" : false,
    "version": '1.2.3'
}
---