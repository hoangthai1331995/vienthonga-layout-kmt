---json
{
    "title": "Đặt Trước Samsung Galaxy S8 Plus Orchid Gray",
    "slug": "",
    "description": "Phiên bản màu tím khói của Galaxy S8 Plus vừa lên kệ tại Việt Nam. Viễn Thông A đang cho đặt trước Samsung Galaxy S8 Plus màu tím khói với bộ quà tặng cực kỳ hấp dẫn.",
    "layout": "s8tim.html",
    "permalink" : false,
    "version": '1.1.1'
}
---