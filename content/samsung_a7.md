---json
{
    "title": "Đặt Trước Samsung Galaxy A7 - Bộ 3 camera góc siêu rộng, Chụp đêm đỉnh cao",
    "slug": "",
    "description": "Từ ngày 16/10 - 26/10 đặt trước Galaxy A7 - 3 camera góc chụp siêu rộng, xóa phông chuyên nghiệp tại Viễn Thông A nhận ngay 1.000 điểm VinID cùng ưu đãi trả góp 0%. Liên hệ : 1900 6766",
    "layout": "samsung_a7.html",
    "permalink" : false,
    "version": '1.3.1'
}
---