(function ($) {
    "use strict";

    /*Scripts initialization*/

    $(window).on('load',function () {

        $(window).trigger("scroll");
        $(window).trigger("resize");
    });

    $(document).ready(function () {
        $(window).trigger("resize");

        init_main_menu();

    });

    $(window).resize(function () {
        init_main_menu_resize(1000);

    });


    /*Platform detect*/

    var mobileTest;

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        mobileTest = true;
        $("html").addClass("mobile");
    }
    else {
        mobileTest = false;
        $("html").addClass("no-mobile");
    }

    var mozillaTest;
    if (/mozilla/.test(navigator.userAgent)) {
        mozillaTest = true;
    }
    else {
        mozillaTest = false;
    }
    var safariTest;
    if (/safari/.test(navigator.userAgent)) {
        safariTest = true;
    }
    else {
        safariTest = false;
    }

    // Detect touch devices
    if (!("ontouchstart" in document.documentElement)) {
        document.documentElement.className += " no-touch";
    }

    //Fuction for block height 100%
    function height_line(height_object, height_donor) {
        height_object.height(height_donor.height());
        height_object.css({
            "line-height": height_donor.height() + "px"
        });
    }

    // Function equal height
    !function (a) {
        a.fn.equalHeights = function () {
            var b = 0, c = a(this);
            return c.each(function () {
                var c = a(this).innerHeight();
                c > b && (b = c)
            }), c.css("height", b)
        }, a("[data-equal]").each(function () {
            var b = a(this), c = b.data("equal");
            b.find(c).equalHeights()
        })
    }(jQuery);



    /*Main Navigation script*/
    var mobile_nav = $(".mobile-nav");
    var desktop_nav = $(".desktop-nav");
    var main_nav = $(".main-nav");

    function init_main_menu_resize(menuBreakpoint) {
        //Mobile menu max-height
        $(".mobile-on .desktop-nav > ul").css("max-height", $(window).height() - main_nav.height() - 20 + "px");

        //Mobile menu style toggle
        if ($(window).width() <= menuBreakpoint) {
            //main_nav.addClass("mobile-on").removeClass("menu-scroll");
            main_nav.addClass("mobile-on");
            $("#main-nav").appendTo("#position-nav-top");


        } else {
            if ($(window).width() > menuBreakpoint) {
                main_nav.removeClass("mobile-on");
                $("#main-nav").appendTo("#position-nav");
                desktop_nav.show();

            }
        }
    }

    function init_main_menu() {

        //height_line(mobile_nav, main_nav);
        //Mobile menu toggle

        $(window).scroll(function() {
            // if($(window).scrollTop()>10 && !main_nav.hasClass("mobile-on")){
            if ($(window).scrollTop() > 10) {
                main_nav.addClass("menu-scroll");
                $(".mn-has-sub").removeClass("hover");

                $("#main-nav").appendTo("#position-nav-top");
            } else {
                main_nav.removeClass("menu-scroll");

                if(!main_nav.hasClass("mobile-on")){
                    $("#main-nav").appendTo("#position-nav");
                }


            }
        });


        mobile_nav.click(function() {
            if (desktop_nav.hasClass("js-opened")) {
                desktop_nav.slideUp("normal", "easeOutExpo").removeClass("js-opened");
                mobile_nav.removeClass("active");
            } else {
                desktop_nav.slideDown("normal", "easeOutQuart").addClass("js-opened");
                mobile_nav.addClass("active");
            }
        })

        //Hide menu when don't clicj sub menu
        desktop_nav.find("a:not(.mn-has-sub)").click(function() {
            if (mobile_nav.hasClass("active")) {
                desktop_nav.slideUp("slow", "easeOutExpo").removeClass("js-opened");
                mobile_nav.removeClass("active");
            }
        });


        // Sub menu

        var mnHasSub = $(".mn-has-sub");
        var mnThisLi;

        mnThisLi = mnHasSub.parent("li");




        mnThisLi.hover(function() {

            if (!(main_nav.hasClass("mobile-on")) && !(main_nav.hasClass("menu-scroll"))) {
                $(this).find(".mn-sub:first").stop(true, true).delay(300).fadeIn("fast");
            }



        }, function() {

            if (!(main_nav.hasClass("mobile-on")) && !(main_nav.hasClass("menu-scroll"))) {

                $(this).find(".mn-sub:first").stop(true, true).fadeOut("fast");
            }
        });

        mnHasSub.on('click', function(e) {
            'use strict';
            var link = $(this);

            if (link.hasClass('hover') || main_nav.hasClass("mobile-on") || $("html").hasClass("no-touch")) {
                return true;
            } else {

                link.addClass('hover');

                mnHasSub.not(this).removeClass('hover');

                if (!main_nav.hasClass("menu-scroll")) {
                    e.preventDefault();
                    return false;
                } else {}
                return true;
            }
        });
    }

})(jQuery); // End of use strict
