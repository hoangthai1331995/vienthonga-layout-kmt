
var VtaJs = VtaJs || {} //Namespace
var VtaJsPC = VtaJsPC || {} //Namespace
VtaJsPC.main = {
    BindDataDropdown: function (dropdownId, ent, haveStore) {
        var cityId = $(ent).val();
        $('#CityName').val($(ent).find('option:selected').text());
        VtaJsPC.service.ListDistrictByCityId(cityId, haveStore, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn quận huyện==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.DistrictId).text(this.Districtname);
                $('#' + dropdownId).append(option);
            });
        });
    },
    BindDataDropdownCity: function (dropdownId, haveStore) {
        $('#' + dropdownId).html("");
        VtaJsPC.service.ListCity(haveStore, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn thành phố==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.Cityid).text(this.Cityname);
                $('#' + dropdownId).append(option);
            });
        });
    },
    BindDataDropdownBranch: function (dropdownId, ent) {
        var districtId = $(ent).val();
        $('#DistrictName').val($(ent).find('option:selected').text());
        VtaJsPC.service.ListBranchByDistrictId(districtId, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn chi nhánh==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.BranchsCodePos).text(this.BranchsAdd);
                $('#' + dropdownId).append(option);
            });
        });
    },
    SetDistrictName: function (ent) {
        $('#DistrictName').val($(ent).find('option:selected').text());
    },

    FormatNumber: function (number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length > prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        // Add this number to the element as text.
        return s.join(dec);
    },
    AddName: function (ctr, ent) {
        $('#' + ctr).val($(ent).find('option:selected').text());
    },
    BindDataDropdownBranchId: function (dropdownId, ent) {
        var districtId = $(ent).val();
        $('#DistrictName').val($(ent).find('option:selected').text());
        VtaJsPC.service.ListBranchByDistrictId(districtId, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn chi nhánh==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.BranchsId);
                option.attr('storecode', this.BranchsCodePos);
                option.text(this.BranchsAdd);
                $('#' + dropdownId).append(option);
            });
        });
    },

    CreateCookieBranch: function (branchCode, branchAddress, groupId) {
        Cookies.set('VtaCookieMobile_CurrentBranch', branchCode, { expires: 7 });
        Cookies.set('VtaCookieMobile_CurrentBranchAddress', branchAddress, { expires: 7 });
        Cookies.set('VtaCookieMobile_CurrentBranchGroupId', groupId, { expires: 7 });
        ga('send', {
            'hitType': 'event',          // Required.
            'eventCategory': 'pc_chon_chi_nhanh',   // Required.
            'eventAction': 'click',      // Required.
            'eventLabel': branchCode
        });
        var url = window.location.origin + window.location.pathname;
        window.location.href = url + "?cn=" + branchCode;
    },

    SearchProduct: function () {
        window.location.href = "../../tim-kiem?q=" + $('#searchvta').val();
    }
};

VtaJsPC.service = {
    ListDistrictByCityId: function (cityId, haveStore, callBack) {
        var url = '/api/vta/listdistrictbycity/' + cityId;
        if (haveStore != undefined) url = url + "/" + haveStore;
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },
    ListBranchByDistrictId: function (districtId, callBack) {
        $.ajax({
            type: 'get',
            dataType: 'json',
            //url: '/Vta.Service.Externel.Branch/V1/R/GetBranchCity',
            url: '/api/vta/listbranchbydistrict/' + districtId,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },
    ListSubCategoryById: function (parentId, callBack) {
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: 'api/vta/listcategorymodeldatabyid/' + parentId,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },
    ListProductById: function (subCategoryId, callBack) {
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: 'api/vta/listproductmodelbyid/' + subCategoryId,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },
    GetProductDetailById: function (productId, storecode, callBack) {
     
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: 'api/vta/getproductdetailbyid/' + productId + "/" + storecode,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },


    GetProductsSearch: function (strSearch) {
        if (strSearch.length < 2 || strSearch.length > 50) {
            $(".searchContaint").html("");
            return;
        }

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: '/api/searchproduct/' + strSearch,
            success: function (data) {
           
                try {
                    $(".searchContaint").html(VtaJs.main.createHtmSearchProductRespondV2(data));
                } catch (e) {
                }

            }
        });
    },

    ListCity: function (haveStore, callBack) {
        var url = '/api/vta/listcityapi';
        if (haveStore != undefined) url = url + "/" + haveStore;
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },

    RenderMoreSearchProduct: function (ent, callBack) {
        var page = $(ent).attr("page");
        var q = $('#keysearch').val();
        $.ajax({
            type: 'get',
            data: { "q": q, "page": page },
            url: '/search/LoadMoreProductSearch',
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },


}

VtaJs.main = {


    createHtmSearchProductRespond: function (jsonObject) {
        if (JSON.stringify(jsonObject) === '{}' || jsonObject.length === 0) {
            return "<li ><span class='no-result'>Không tìm thấy kết quả nào!!!</span></li>";
        }
        var html = '';
        jsonObject.forEach(function (val) {
            try {
                var price = val["price"];
                var prefix = '<sup>đ<sup>';
                if (price === 0) {
                    price = "[Liên hệ]";
                    prefix = '';
                } else {
                    price = VtaJs.main.formartCurrency(val["price"], "").replace('.00', '');
                }
                html = html + '<li>' +
                    '<a href="' + val["fullurl"] + '" rel="external" data-ajax="false">' +
                    '<div class="quicks-image">' +
                    '<img alt="" src="' + val["cdn_image_path"] + '" class="img-responsive">' +
                    '</div>' +
                    '<div class="quicks-content">' +
                    '<span class="quicks-product-name">' + val["product"] + '</span>' +

                    '<span class="quicks-product-price">' + price + prefix + ' </span>' +
                    '</div>' +
                    '</a>' +
                    '</li>';

            } catch (e) {
            }
        });
        return html;
    },
    createHtmSearchProductRespondV2: function (jsonObject) {

        if (JSON.stringify(jsonObject) === '{}' || jsonObject.length === 0) {
            return "<li ><span class='no-result'>Không tìm thấy kết quả nào!!!</span></li>";
        }

        var html = '';
        jsonObject.forEach(function (val) {
            try {
                var price = val["ProductPrice"];
                var prefix = '';
                if (price === "-1đ") {
                    price = "[Liên hệ]";
                } else {
                    price = val["ProductPrice"];
                }
                html = html + '<li>' +
                    '<a href="' + val["ProductUrl"] + '" rel="external" data-ajax="false">' +
                    '<div class="quicks-image">' +
                    '<img alt="" src="' + val["ImageUrl"] + '" class="img-responsive">' +
                    '</div>' +
                    '<div class="quicks-content">' +
                    '<span class="quicks-product-name">' + val["ProductName"] + '</span>' +

                    '<span class="quicks-product-price">' + price + ' </span>' +
                    '</div>' +
                    '</a>' +
                    '</li>';

            } catch (e) {
            }
        });
        return html;
    },
    createHtmSearchSosanhRespond: function (jsonObject) {
        if (JSON.stringify(jsonObject) === '{}' || jsonObject.length === 0) {
            return "<li ><span class='no-result'>Không tìm thấy kết quả nào!!!</span></li>";
        }
        var html = '';
        jsonObject.forEach(function (val) {
            try {
                html = html + '<li>' +
                    '<a href="" rel="external" data-ajax="false"> asdf' +
                    '</a>' +
                    '</li>';

            } catch (e) {
            }
        });
        return html;
    },
    ShowSeletedBranchWhenPostBack: function () {
        try {
            var branchCode = Cookies.get('VtaCookieMobile_CurrentBranch');
            if (branchCode != undefined && branchCode !== "CS_0000042") {
                var branchAdress = Cookies.get('VtaCookieMobile_CurrentBranchAddress');
                $('.dropdown-vta-search').html(branchAdress);
                //$('.branch-views').show();
            }
        } catch (e) {
        }
    },
    createFullBranchsTreeHtml: function (jsonObjectData, jsonObjectCities, stridroot) {
        var html = '';
        if (jsonObjectData !== undefined && jsonObjectCities !== undefined) {
            $.each(jsonObjectCities, function (index, val) {

                html = html + '<div data-role="collapsible" class="left-menu-city-collapse" >' +
                                '<h4 class="h4-city">' + val.CityName + '</h4>' +
                                VtaJs.main.createHtmlNodeDistrictAndBranchs(val.CityId, jsonObjectData)
                                + '</div>';
            });
        } else {
            return null;
        }

        return html;
    },
    createHtmlNodeDistrictAndBranchs: function (strCitiId, jsonObject) {
        var html = '';
        var districtArr = VtaJs.main.getListDistrictIdByCity(strCitiId, jsonObject);
        if (districtArr.length <= 0) {
            return null;
        }

        $.each(districtArr, function (i, v) {
            /*
            ProcessLink with brachs
            */
            var branchsArr = VtaJs.main.getListBranchIdByCity(strCitiId, v.id, jsonObject);
            if (branchsArr.length > 0) {
                html = html + '<div id="city-' + v.id + '" data-role="collapsible" class="left-menu-district-collapse" >' // Add first node is district
                        + '<h4 class="h4-district"><span > &diams; </span>' + v.name + '</h4>' +
                        VtaJs.main.createHtmlListBranch(branchsArr, jsonObject)
                        + '</div>';
            } else {
                return null;
            }
        });
        return html;
    },
    getListDistrictIdByCity: function (strCityId, jsonObject) {
        var rs = [];
        $.each(jsonObject, function (index, val) {
            if (strCityId === val.CityId) {
                try {
                    var isExistDistrictId = false;

                    $.each(rs, function (i, v) {

                        try {
                            if (v.id === val.DistrictId) {
                                isExistDistrictId = true;
                            }
                        } catch (e) {
                        }
                    });

                    if (isExistDistrictId !== true) {
                        rs.push({ id: val.DistrictId, name: val.DistrictName });
                    };
                } catch (e) {
                }
            };
        });
        return rs;
    },

    getListBranchIdByCity: function (strCityId, strDistrictId, jsonObject) {
        var rs = [];
        $.each(jsonObject, function (index, val) {
            if (strCityId === val.CityId && strDistrictId == val.DistrictId) {
                rs.push(val.BranchId);
            }
        });
        return rs;
    },
    createHtmlListBranch: function (branchsArr, jsonObject) {
        var html = '<div id="wrap-branch-ul">';
        html = html + '<ul class="ul-branchList" data-role="listview" data-inset="true" data-collapsed="true">';
        $.each(branchsArr, function (i, v) {

            $.each(jsonObject, function (index, val) {
                if (val.BranchId === v) {
                    var stringPara = "'" + val.BranchCodePos + "'";
                    html = html + '<li  class="li-branchItem"><span onclick="VtaJs.main.CreateCookieBranch(' + stringPara + ',' + '\'' + val.BranchAdress + '\'' + ');" id="branch-' + val.BranchCodePos + '" class="span-branch"> &bull; ' + val.BranchAdress + '</span></li>';
                }
            });

        });
        html = html + '</ul></div>';
        return html;

    },

    BindDataDropdownBranchId: function (dropdownId, ent) {
        var districtId = $(ent).val();
        $('#DistrictName').val($(ent).find('option:selected').text());
        VtaJs.service.ListBranchByDistrictId(districtId, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn chi nhánh==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.BranchsId).text(this.BranchsAdd);
                $('#' + dropdownId).append(option);
            });
        });
    },


    CreateCookieBranch: function (branchCode, branchAddress) {
        Cookies.set('VtaCookieMobile_CurrentBranch', branchCode, { expires: 7 });
        Cookies.set('VtaCookieMobile_CurrentBranchAddress', branchAddress, { expires: 7 });
        ga('send', {
            'hitType': 'event',          // Required.
            'eventCategory': 'pc_chon_chi_nhanh',   // Required.
            'eventAction': 'click',      // Required.
            'eventLabel': branchCode
        });
        var url = window.location.origin + window.location.pathname;
        window.location.href = url + "?cn=" + branchCode;
    },
    BindDataDropdown: function (dropdownId, ent) {
        var cityId = $(ent).val();
        $('#CityName').val($(ent).find('option:selected').text());
        VtaJs.service.ListDistrictByCityId(cityId, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn quận huyện==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.DistrictId).text(this.Districtname);
                $('#' + dropdownId).append(option);
            });
        });
    },
    BindDataDropdownBranch: function (dropdownId, ent) {
        var districtId = $(ent).val();
        $('#DistrictName').val($(ent).find('option:selected').text());
        VtaJs.service.ListBranchByDistrictId(districtId, function (data) {
            $('#' + dropdownId).html("");
            $('#' + dropdownId).append($('<option />').attr('value', '0').text('===Chọn chi nhánh==='));
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.BranchsCodePos).text(this.BranchsAdd);
                $('#' + dropdownId).append(option);
            });
        });
    },
    SetDistrictName: function (ent) {
        $('#DistrictName').val($(ent).find('option:selected').text());
    },

    FormatNumber: function (number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length > prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        // Add this number to the element as text.
        return s.join(dec);
    }
};
VtaJs.service = {
    GetProductsSearch: function (strSearch) {
        if (strSearch.length < 2 || strSearch.length > 50) {
            return;
        }

        window._fbq.push(['track', 'Search']);

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: '/api/searchproduct/' + strSearch,
            success: function (data) {
                //console.log(data);
                try {
                    $(".searchContaint").html(VtaJs.main.createHtmSearchProductRespond(data));
                } catch (e) {
                }

            },
        });
    },



    ListDistrictByCityId: function (cityId, haveStore, callBack) {
        var url = '/api/vta/listdistrictbycity/' + cityId;
        if (haveStore != undefined) url = url + "/" + haveStore;
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },
    ListCity: function (haveStore, callBack) {

        var url = '/api/vta/listcityapi';
        if (haveStore != undefined) url = url + "/" + haveStore;
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },

    ListBranchByDistrictId: function (districtId, callBack) {
        $.ajax({
            type: 'get',
            dataType: 'json',
            //url: '/Vta.Service.Externel.Branch/V1/R/GetBranchCity',
            url: '/api/vta/listbranchbydistrict/' + districtId,
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },

    RenderMoreSearchProduct: function (ent, callBack) {
        var page = $(ent).attr("page");
        var q = $('#keysearch').val();
        $.ajax({
            type: 'get',
            data: { "q": q, "page": page },
            url: '/search/LoadMoreProductSearch',
            success: function (data) {
                if (typeof (callBack) == "function") {
                    callBack(data);
                }
            }
        });
    },
};

// LoadchinhanhHeader();


function LoadchinhanhHeader() {
    
    var strHtml = "";
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/api/getlistallbranch',
        success: function (data) {
            $.each(data.ListArea, function (i, itemListArea) {

                strHtml += '<div data-location="mien-' + itemListArea.AreaId + '-v" class="sector-name">';
                strHtml += '<a href="javascript:void(0);" onclick="ga(\'send\', \'event\', { eventCategory: \'Chon cua hang ' + itemListArea.AreaName + '\', eventAction: \'Click ' + itemListArea.AreaName + '\', eventLabel: \'' + itemListArea.AreaName + '\'});" >' + itemListArea.AreaName + '</a>';
                strHtml += '</div>';
                strHtml += '<ul class="clearlist scroll location-list sector" id="mien-' + itemListArea.AreaId + '-v">';
                $.each(data.ListCityBranch, function (i, itemListCityBranch) {
                    if (itemListCityBranch.AreaId === itemListArea.AreaId) {
                        strHtml += '<li class="city">';
                        strHtml += itemListCityBranch.Cityname;
                        strHtml += '<ul class="clearlist">';
                        $.each(data.ListDistrictBranch, function (i, itemListDistrictBranch) {
                            if (itemListDistrictBranch.CityId === itemListCityBranch.Cityid) {
                                strHtml += '<li class="district">';
                                strHtml += itemListDistrictBranch.Districtname;
                                strHtml += '      <ul class="clearlist">';
                                $.each(data.ListBranchs, function (i, itemListBranchs) {
                                    if (itemListBranchs.Districtid === itemListDistrictBranch.DistrictId) {
                                        var strscript = "onclick=\"VtaJsPC.main.CreateCookieBranch('" + itemListBranchs.BranchsCodePos + "','" + itemListBranchs.BranchsAdd + "','" + itemListBranchs.GroupId + "')\"";
                                        strHtml += '<li>';
                                        strHtml += '  <a href="javascript:void(0);" ' + strscript + '>';
                                        if (itemListBranchs.GroupId === 4) {
                                            strHtml += '<span class="icon-vbox"></span>';
                                        } else {
                                            strHtml += '<span class="icon-vta"></span>';
                                        }
                                        strHtml += itemListBranchs.BranchsShortAdd;
                                        strHtml += '  </a>';
                                        strHtml += ' </li>';
                                    }
                                });
                                strHtml += ' </ul>';
                                strHtml += '</li>';
                            }

                        });
                        strHtml += ' </ul>';
                        strHtml += '</li>';
                    }

                });
                strHtml += '</ul>';
            });
            $("#showbranch").html(strHtml);
            $("#loaderbranch").hide();
            $("#showbranch").show();

            var globalCityButton = $("#location-global").find(".location-list").children('li');
            var globalCitySubButton = $("#location-global").find(".location-list").children('li').find("li");
            var globalTabButton = $("#location-global").find('.dropdown-vta-tabs').children('div');

            globalTabButton.click(function (e) {
                e.preventDefault();
                var $this = $(this),
                    tabID = $this.data('location');

                if (!$this.hasClass("active")) {
                    $this.addClass("active").siblings().removeClass("active");

                    $("#location-global .location-list").hide();
                    $("#location-global #" + tabID).fadeIn(function () {

                        $('.scroll').perfectScrollbar('update');
                    });
                }
            });


            globalCityButton.click(function () {
                var $this = $(this);


                if (!$this.hasClass("active")) {
                    $this.addClass("active").siblings().removeClass("active");
                } else {
                    $this.removeClass("active");
                }
                $('.scroll').perfectScrollbar('update');
            });

            globalCitySubButton.click(function (e) {
                var $this = $(this);

                e.stopPropagation();
                if (!$this.hasClass("active")) {
                    $this.addClass("active").siblings().removeClass("active");
                }

                $('.scroll').perfectScrollbar('update');
            });







            var sectorList = $("#location-global .sector");
            var cityList = $("#location-global .city");
            var districtList = $("#location-global .district");

            var allList = $(".location-list a");



            $("#filter-s").keyup(function () {
                var filter = locdau($(this).val());

                delay(function () {


                    allList.each(function () {

                        $this = $(this);
                        $s_text = locdau($this.text());
                        if ($s_text.search(new RegExp(filter, "i")) < 0) {
                            $this.fadeOut().addClass('not', function () {
                            });
                        } else {
                            $this.fadeIn().removeClass('not', function () {
                            });
                        }
                    });

                    districtList.each(function () {
                        $this = $(this);

                        if ($this.find("a").not(".not").length > 0) {
                            $this.addClass("active").fadeIn();
                        } else {
                            $this.removeClass("active").fadeOut();
                        }
                    });

                    cityList.each(function () {
                        $this = $(this);

                        if ($this.find(".district.active").length > 0) {
                            $this.addClass("active").fadeIn();
                        } else {
                            $this.removeClass("active").fadeOut();
                        }
                    });


                    sectorList.each(function () {
                        $this = $(this);
                        if ($this.find(".city.active").length > 0) {
                            $this.prev().removeClass("hidden");
                        } else {
                            $this.prev().addClass("hidden");
                        }
                        $(".sector-name").next().hide();
                        $(".sector-name").not(".hidden").first().next().show();
                    });

                    num = $(".sector-name").length;
                    if ($(".sector-name.hidden").length == num) {
                        $(".dropdown-vta-content").addClass("hethang");
                    } else {
                        $(".dropdown-vta-content").removeClass("hethang");
                    }



                    if (filter == "") {
                        districtList.each(function () {
                            $this = $(this);
                            $this.removeClass("active");
                        });

                        cityList.each(function () {
                            $this = $(this);
                            $this.removeClass("active");
                        });
                        $(".sector-name").next().hide();
                    }

                }, 400);
            });






        }
    });

}


$("#searchvta").bind('keydown', function (e) {

    var next, current;

    if ([40, 38, 13].indexOf(e.which) > -1 &&
      $li.filter('.selected').length) {

        current = getIndex($li, '.selected');

        if (e.which === 38) {

            next = (((current - 1) < 0) &&
              $li.length - 1) || current - 1;

        }

        if (e.which === 40) {

            next = ((current === $li.length) &&
              current - $li.length) || current + 1;

            if (next === $li.length) {
                next = 0;
            }
        }

        if (e.which === 13) {

            next = current;
            /*
            if($li.filter('.no-choose')){
                return;
            }*/

            var url = $($li[next]).children("a").attr("href");


            window.location.href = url;
        }


        $($li[next]).trigger('click');
    }
});

$('#searchvta').keypress(function (e) {
    if (e.which == 13) {
        window.location.href = "tim-kiem?q=" + $('#searchvta').val();
    }
});

$(document).mouseup(function (e) {
    var container = $(".search-global");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.find(".searchContaint").addClass("hidden").removeClass("visible");
    } else {
        container.find(".searchContaint").addClass("visible").removeClass("hidden");
    }
});

$('.search-global .input-search').keyup(function (e) {

    window._fbq.push(['track', 'Search']);

    VtaJsPC.service.GetProductsSearch($(this).val());
    //$('.search-global .input-search').val($(this).val());


});

$('.location-global-style .dropdown-vta-clear').bind('click', function () {
    Cookies.set('VtaCookieMobile_CurrentBranch', "", { expires: -1 });
    Cookies.set('VtaCookieMobile_CurrentBranchAddress', "", { expires: -1 });
    $('.location-global-form').html("<div class='location-dropdown-icon'><i class='vta-arrow-down'></i></div><span class='location-icon'><i class='vta-location'></i></span> Chọn chi nhánh để xem giá và khuyến mãi");
    //$('.branch-views').hide();
    window.location.href = window.location.origin + window.location.pathname;
});




var globalTabButton = $("#location-global").find('.dropdown-vta-tabs').children('div');

var globalCityButton = $("#location-global").find(".location-list").children('li');
var globalCitySubButton = $("#location-global").find(".location-list").children('li').find("li");

var globalSearch = {
    button: $("#location-global").find(".location-global-form"),
    container: $("#location-global").find(".dropdown-vta-content"),
    init: function () {
        globalSearch.button.on('click', this.show);
    },
    show: function () {
        globalSearch.container.slideToggle(200, function () {
            $('.scroll').perfectScrollbar('update');
        });

        globalSearch.container.mouseleave(function () {
            //globalSearch.close();
        })

        $(document).on("click touch", function (e) {
            if (!$(e.target).parents().addBack().is('#location-global')) {
                globalSearch.close();
            }
        });
    },
    close: function () {
        globalSearch.container.slideUp(200);
    }
}
globalSearch.init();

//Mien nam show first
$('#miennam').show();

globalTabButton.click(function (e) {
    e.preventDefault();
    var $this = $(this),
        tabID = $this.data('location');

    if (!$this.hasClass("active")) {
        $this.addClass("active").siblings().removeClass("active");

        $("#location-global .location-list").hide();
        $("#location-global #" + tabID).fadeIn(function () {

            $('.scroll').perfectScrollbar('update');
        });
    }
});


globalCityButton.click(function () {
    var $this = $(this);


    if (!$this.hasClass("active")) {
        $this.addClass("active").siblings().removeClass("active");
    } else {
        $this.removeClass("active")
    }
    $('.scroll').perfectScrollbar('update');
});

globalCitySubButton.click(function (e) {
    var $this = $(this);

    e.stopPropagation();
    if (!$this.hasClass("active")) {
        $this.addClass("active").siblings().removeClass("active");
    }

    $('.scroll').perfectScrollbar('update');
});

/*end Global search*/