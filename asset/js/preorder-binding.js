/**
 * Created by admin on 10/20/2016.
 */

/**
 * Vue Vee Validate
 */
// Vue.use(VeeValidate);


/**
 * Detect browser scripts
 */

    //Check device operation system
    function setUserOS() {
        var OSName = "";
        if (navigator.appVersion.indexOf("Win") != -1) OSName = "windows";
        if (navigator.appVersion.indexOf("Mac") != -1) OSName = "mac";
        if (navigator.appVersion.indexOf("X11") != -1) OSName = "unix";
        if (navigator.appVersion.indexOf("Linux") != -1) OSName = "linux";
        document.body.classList.add(OSName);``
      }
  
      //Check device type
      function setUserAgent() {
        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
          document.body.classList.add('mobile');
        } else {
          document.body.classList.add('desktop');
          if (navigator.userAgent.match(/MSIE 9.0/)) {
            document.body.classList.add('ie9');
          }
        }
      }
  
      //Run function
      setUserOS();
      setUserAgent();
  
  
  /**
   * Main scripts
   */
  
  
  Array.prototype.unique = function(mutate) {
    var unique = this.reduce(function(accum, current) {
        if (accum.indexOf(current) < 0) {
            accum.push(current);
        }
        return accum;
    }, []);
    if (mutate) {
        this.length = 0;
        for (let i = 0; i < unique.length; ++i) {
            this.push(unique[i]);
        }
        return this;
    }
    return unique;
}
  window.onload = function () {
      if( $(document).width() < 1204){
          app2.mobileCheck = true;
      }
      if( $(document).width() < 768){
          app2.mobileOrder = true;
      }
      $.ajax({
          type: 'get',
          url: 'https://assets.vienthonga.vn/html/header.html',
          success: function (data) {
              var $items = data;
              $("body").prepend($items);
              $.getScript("js/generalPc.js");
              // $.getScript("js/generalPc.js").done(LoadchinhanhHeader());
              $.getScript("https://assets.vienthonga.vn/js/headerfooter.js");
  
          }
      });
      $.ajax({
          type: 'get',
          url: 'https://assets.vienthonga.vn/html/footer.html',
          success: function (data) {
              var $items = data;
              $("body").append($items);
          }
      });
      app2.getPreLoad();
      app2.getProduct();
      app2.getTragop();
      app2.getListDatTruoc();
      // app2.filterCityKhuyenMai();
      // app2.getProductTabBoSung();
      // app2.SearchByCustomerPhone();
      app2.filterCity();
      app2.createCookies();
  }
  
  /*var publicURL="http://www.webapipublicvta.com";*/
  var currentUrl ="https://vienthonga.vn" + window.location.pathname,
      /*var currentUrl = "https://vienthonga.vn/lich-nghi-tet-dinh-dau-2017-tai-vien-thong-a.html";*/
      publicURLPre = "https://publicapi.vienthonga.vn",
      // publicURLPre = "http://www.apipublic.com",
      // publicURLPre = "http://10.32.43.40:8383",
      campaignID = "295", // get backend 281 // 286
      campaignIdCoc = "294", // get backend 280  // 282
      campaignIDtemp ='294',
      listGiaDuKien= {
        "SAMSUNGNOTE9": {productCode:"10130714",price:5490000,productid:'187985'},
        // "A6Plus": {productCode:"14567",price:8990000,productid:'187864'}
      },
      
      productCode = "10131724", // get api
      productId = "188023", // get api  188014
   // productCode = "10130876,14601", // get api
   // productId = "187893,187915", // get api
      
      dateStart = "2018/10/10 17:00:00",
      dateEnd = "2018/10/22 00:00:00",
      isSearch = false,
      isSearchCoc = false,
  
  
      keyWord = "oppo+a7"; // keyWord to get News
      var app2 = new Vue({
          delimiters: ['${', '}'],
          el: '#vue-element',
          data: {
              HinhAnhSanPhamPC: [],
              HinhAnhSanPhamMobile: [],
              ThongTinTraGop: '',
              phaseStatus:'',
              paymentMethod: [{
                  name: "visa",
                  paymentCode: "visa",
                  imageLink: "https://cdn1.vienthonga.vn/image/2016/11/3/100000_visa.png"
              }, {
                  name: "master",
                  paymentCode: "master",
                  imageLink: "https://cdn1.vienthonga.vn/image/2016/11/3/100000_mastercard.png"
              }, {
                  name: "123PAY",
                  paymentCode: "123PAY",
                  imageLink: "https://cdn1.vienthonga.vn/image/2016/11/3/100000_atm.png"
              }],
              
              comboList: [{
                  name: "Loa Bonsai 3 in 1, kính thực tế ảo Samsung VR 2017. giá đỡ điện thoại",
                  value: "bonsai",                
              },{
                  name: "Loa Samsung K350, ốp lưng Samsung Hyperknit, giá đỡ điện thoại",
                  value: "k350",
                }],
              listGiaDuKien:listGiaDuKien,
              giaDienthoai:{
                    HUEWEIP20PRO: 24490000,
                    // a6plus: 8990000
              },
                 
              chonSanpham: {
                  name: 'OPPO A7',
              },
              TragopHC: [
                {
                    name: 'Có',
                    value: 'Y'
                },
                {
                    name: 'Không',
                    value: 'N'
                }
            ],
            chonTragopHC:"N",
              Tragop: [
                  {
                      name: 'Có',
                      value: 'Y'
                  },
                  {
                      name: 'Không',
                      value: 'N'
                  }
              ],
              comboListV4: [{
                name: "Dàn Loa Samsung K350 ",
                value: "K350",                
            },{
                name: "Tai nghe  Power Beat, Ốp lưng Samsung Hyperknit, Giá đỡ điện thoại đa năng Popsocket",
                value: "Hyperknit",
                
            }],
                
              Wow: [
                  {
                      name: 'Giảm ngay 9.000.000đ với gói WOW1399',
                      value: 'wow1399'
                  },
                  {
                      name: 'Giảm ngay 6.500.000đ với gói WOW999',
                      value: 'wow999'
                  }
              ],
              giaThanhtoan: {
                  giaMay: 5990000,
                  traTruoc: 0,
                  tienThanhtoan: 0,
                  tragopThang: 0,
                  tienwow: 0,
                  tongtien: 0,
                  khuyenmai: 0,
              },
              hinhWow1399:"https://cdn1.vienthonga.vn/image/2018/2/1/100000_tooltip-s9-2.jpg",
              hinhWow999: "https://cdn1.vienthonga.vn/image/2018/2/1/100000_tooltip-s9-2.jpg",
              chonWow: 'wow599',
              chonTragop: 'N',
              searchCusPhone: "",
              searchCusPhoneCoc: "",
              searchCusPhoneTabBoSung: "",
              searchCusPhoneCocTabBoSung: "",
              bgBlack: 'black',
              bgWhite: 'white',
              tienCoc: '300,000đ',
              giaDuKien: '',
              message: 'Hello Vue!',
              payments: [{
                  name: "Trả tiền mặt tại chi nhánh gần nhất",
                  value: '14'
              }, 
                // {
                //     name: "Thanh toán trực tuyến",
                //     value: '16'
                // }
              ],
              genders: [{
                  name: "Nam",
                  id: "anh",
                  value: 'M'
              }, {
                  name: "Nữ",
                  id: "chị",
                  value: 'F'
              }],
              deliveryType: [{
                  name: "Nhận hàng tại siêu thị",
                  id: "shiping_sieuthi",
                  value: '8'
              }, {
                  name: "Nhận hàng tại nhà",
                  id: "shiping_tannoi",
                  value: '6'
              }],
              registerInfo: {
                  ListUserRegister: []
              },
            
              productDatTruoc: {
                  ListNews: [],
                  TotalRegister: 0,
                  
              },
              maComboGia: '',
              depositInfo: {},
              chekcaptcha: 'False',
              depositInfoTabBoSung: {},
              pushObj: {
                  MoneyByStore:500000,
                  Store_ID:95,
                  StorePrice:178,
                  Color: "OPPO A7 - Xanh lam ngọc",
                  Gender: "M",
                  Name: "",
                  Phone: "",
                  Email: "",
                  Shiping: "8",
                  City: "",
                  District: "",
                  Address: "",
                  Payment: "14",
                  CampaignId: campaignID,
                  OrderShareId: 0,
                  Productid:188023,
                  Productcode: "10131724",
                  ProductColorcode: "10131724", // hien tại do no bi rỗng chứ phải là HH0012000
                  Price: 500000,
                  Product: "OPPO A7",
                  MaCombo: "",
                  MotaCombo: "",
                  Cmnd: "",
                  CurrentUrl: currentUrl,
                  PaymentMethod: "",
                  AffiliateSource: "",
                  Description:"Không chọn cả 2 phương thức trả góp",
                  // Note: "Đặt trước OPPO A7",
                  DetailAddress: '',
              },
              campaignInfo: {
                  EndDate: '',
                  CampaignStatus: false,
                  TotalRegister: 0,
                  TotalDatCoc: 0,
                  BannerPC: '',
                  BannerMobile: '',
                  Note: '',
                  ListNews: []
              },
           
              ListProduct :   {},
              listCityOriginal: listCityOriginal,
              listCity: [],
              selectedCity: '',
              listDistrict: '',
              selectedDistrict: '',
              listBranch: '',
              selectedProduct: '',
              selectedMemory: '',
              checkComboCode: '',
              selectedProduct: '',
              selectedPaymentMethod: '',
              mobileCheck: false,
              mobileOrder : false,
              checkCombo: 'S9wow1399',
              chonCombo: 'K350',
              listCityKhuyenMaiModel: ['CS_0000027','CS_0000097','CS_0000200','CS_0000322','CS_0000198','CS_0000032','CS_0000311','CS_0000317','CS_0000095','CS_0000243','CS_0000194','CS_0000045','CS_0000313','CS_0000213','CS_0000374','CS_0000130'],
              listCuahang: ['CS_0000027','CS_0000432','CS_0000022','CS_0000097','CS_0000237','CS_0000004','CS_0000014','CS_0000011','CS_0000025','CS_0000234',
                                'CS_0000118','CS_0000200','CS_0000322','CS_0000001','CS_0000181','CS_0000198','CS_0000032','CS_0000311','CS_0000248','CS_0000317',
                                'CS_0000267','CS_0000095','CS_0000121','CS_0000243','CS_0000194','CS_0000045','CS_0000313','CS_0000213','CS_0000374','CS_0000130'],
            listCityKhuyenMai:[],
            listDistrictKhuyenMai:'',
            selectedCityKhuyenMai: '',
            selectedDistrictKhuyenMai: '',
            chonKM: {
                City:'',
                District: []
            }
  
          },
          
          /* mounted: function(){
           var self = this;
           self.getPreLoad()
           self.getProduct();
           self.SearchByCustomerPhone();
           self.filterCity();
           },*/
           filters: {
              formatMoney: function (number) {
                  var a = parseInt(number);
                  return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + '';
                  // return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
              }
          },   
          methods: {
              changeSlide: function(number) {
                $(".tab-slide .active").removeClass("active");
                $( "#tab-slide-" + number ).addClass( "active" );
                $(".sp-slide4").slick('slickGoTo', number);
              },
              playvideo: function(){
                $("#video").removeClass("d-none");
                // $("#video2").removeClass("d-none");
                $("#play-video").addClass("d-none");
                $("#play-video").removeClass("d-block");
                $("#video")[0].src += "&autoplay=1";
              },
              transferForm: function(){
                  window.open("/oppo-a7.html/datmua/","_self");
              },
              resetGia: function(){
                // this.giaThanhtoan.giaMay = 24490000;
                this.giaThanhtoan.tienThanhtoan = 0;
                this.giaThanhtoan.khuyenmai = 0;
                this.giaThanhtoan.tragopThang = 0;
                this.giaThanhtoan.traTruoc = 0;
                this.giaThanhtoan.tongtien =  0;
                this.pushObj.Description = ''; 
              },
              handletienThanhtoan: function(){
                  var self = this;                 
                  if (this.chonSanpham.name == 'OPPO A7') {
                      this.giaThanhtoan.giaMay = 5990000;
                      this.pushObj.Productcode = 10131724;
                      this.pushObj.Productid = 188023;
                      this.pushObj.Product = 'OPPO A7';
                  } else {
                      this.giaThanhtoan.giaMay = 21990000;
                      this.pushObj.Productcode = 10130952;
                      this.pushObj.Productid = 188007;
                      this.pushObj.Product = 'Huawei Mate 20 Pro';
                      // this.checkCombo = 'S9+';
                  } 
                  if(this.chonTragop == 'Y' && this.chonTragopHC == 'N'){
                        this.giaThanhtoan.khuyenmai = 0;
                        this.giaThanhtoan.tragopThang = (this.giaThanhtoan.giaMay - this.giaThanhtoan.khuyenmai) / 12;
                        this.giaThanhtoan.tongtien = (this.giaThanhtoan.giaMay - this.giaThanhtoan.khuyenmai) / 12;
                        this.pushObj.Description = 'Trả góp thẻ tín dụng 12 tháng'; 
                  } 

                  if(this.chonTragop == 'N' && this.chonTragopHC == 'Y'){
                    this.giaThanhtoan.traTruoc = this.giaThanhtoan.giaMay * 0.4;
                    this.giaThanhtoan.tienThanhtoan = 11000;
                    this.giaThanhtoan.khuyenmai = 0;
                    this.giaThanhtoan.tragopThang = (this.giaThanhtoan.giaMay - this.giaThanhtoan.traTruoc)/6;
                    this.giaThanhtoan.tongtien =  this.giaThanhtoan.tragopThang + this.giaThanhtoan.tienThanhtoan;
                    this.pushObj.Description = 'Trả góp Home Credit 6 tháng'; 
                    } 

                    if(this.chonTragop == 'Y' && this.chonTragopHC == 'Y'){
                       this.resetGia();
                        this.chonTragop = 'N';
                        this.chonTragopHC = 'N';
                        this.handletienThanhtoan();
                        window.swal({
                            title: "",
                            text: "Vui lòng chỉ chọn 1 loại hình trả góp. Xin cảm ơn!",
                            confirmButtonText: "Đóng cửa sổ",
                            confirmButtonColor: "#1F5ECC"
                        });
                        return false;
                        
                    }
                                    
                    if(this.chonTragop == 'N' && this.chonTragopHC == 'N'){
                        this.resetGia();
                        this.pushObj.Description = 'Không chọn cả 2 phương thức trả góp'; 
                    }
                  
              },
              checkTragop: function(giaMay){
                if(this.chonTragop == 'N' && this.chonTragopHC == 'N'){                       
                    this.giaThanhtoan.lephitragopThang =  0;
                    this.giaThanhtoan.tragopThang = 0;
                    
                    if(this.chonWow == 'wow599'){
                        this.giaThanhtoan.troGia = 3800000; 
                        this.giaThanhtoan.tienwow = 599000;  
                        this.giaThanhtoan.tongtien = 599000;
                    } else {
                        this.giaThanhtoan.troGia = 0; 
                        this.giaThanhtoan.tienwow = 0;  
                        this.giaThanhtoan.tongtien = giaMay;
                    }
                    this.giaThanhtoan.traTruoc = 0;
                    this.giaThanhtoan.tienThanhtoan = giaMay - this.giaThanhtoan.troGia;
                    
                    this.pushObj.Description = 'Không chọn cả 2 phương thức trả góp'; 
                    }

                    if(this.chonTragop == 'Y' && this.chonTragopHC == 'N'){
                        this.giaThanhtoan.lephitragopThang =  0;
                        
                        if(this.chonWow == 'wow599'){
                            this.giaThanhtoan.troGia = 3800000; 
                            this.giaThanhtoan.tienwow = 599000;  
                            
                        } else {
                            this.giaThanhtoan.troGia = 0; 
                            this.giaThanhtoan.tienwow = 0;                                  
                        }
                        this.giaThanhtoan.traTruoc = 0;
                        this.giaThanhtoan.tienThanhtoan = giaMay - this.giaThanhtoan.troGia;
                        this.giaThanhtoan.tragopThang = this.giaThanhtoan.tienThanhtoan / 12;
                        
                        this.giaThanhtoan.tongtien = this.giaThanhtoan.tragopThang +  this.giaThanhtoan.tienwow;
                        this.pushObj.Description = 'Trả góp thẻ tín dụng 12 tháng'; 
                    } 

                    if(this.chonTragop == 'N' && this.chonTragopHC == 'Y'){
                        this.giaThanhtoan.lephitragopThang =  11000;
                        if(this.chonWow == 'wow599'){
                            this.giaThanhtoan.troGia = 3800000; 
                            this.giaThanhtoan.tienwow = 599000;  
                            
                        } else {
                            this.giaThanhtoan.troGia = 0; 
                            this.giaThanhtoan.tienwow = 0;                                
                        }
                        this.giaThanhtoan.tienThanhtoan = giaMay -  this.giaThanhtoan.troGia;
                        this.giaThanhtoan.traTruoc = this.giaThanhtoan.tienThanhtoan * 0.3;
                     
                        this.giaThanhtoan.tragopThang = (this.giaThanhtoan.tienThanhtoan - this.giaThanhtoan.traTruoc)/6;
                        this.giaThanhtoan.tongtien =  this.giaThanhtoan.lephitragopThang + this.giaThanhtoan.tragopThang + this.giaThanhtoan.tienwow;
                        this.pushObj.Description = 'Trả góp tài chính 6 tháng'; 
                    } 

                    
                    if(this.chonTragop == 'Y' && this.chonTragopHC == 'Y'){                            
                            this.chonTragop = 'N';
                            this.chonTragopHC = 'N';
                            this.handletienThanhtoan();
                            window.swal({
                                title: "",
                                text: "Vui lòng chỉ chọn 1 loại hình trả góp. Xin cảm ơn!",
                                confirmButtonText: "Đóng cửa sổ",
                                confirmButtonColor: "#1F5ECC"
                            });
                            return false;
                            
                        }
  
              },             
             
              getTragop: function(){
                  var self = this;
                  $.get(publicURLPre + "/dattruoc-api/GetProductDatTruoc?id=" + campaignIDtemp + "&listId=" + productId + "&keyWord=" + keyWord  + "&skip=0&take=20" , function (data) {
                      if (data.Data != null) {                   
                          
                          self.ThongTinTraGop = data.Data.Note;                          
                          
                      }
                  });
              },
             
              Load_More_Dat_Coc: function () {
                  var self = this;
                  var page = parseInt($("#btn-loadmore").attr("data-page"));
                  var take = (page + 1) * 20;
                  if (isSearch == true) {
                      var url = publicURLPre + "/dattruoc-api/GetListRegisterByPhone?campaignId=" + campaignID + "&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=" + take + "&customerPhone=" + self.searchCusPhone;
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: url,
                          success: function (data) {
                              self.registerInfo = data.Data;
                              $("#btn-loadmore").attr("data-page", page + 1);
                          }
                      });
                  } else {
                      var url = publicURLPre + "/dattruoc-api/GetListRegisterAll?campaignId=" + campaignID + "&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=" + take;
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: url,
                          success: function (data) {
                              self.registerInfo = data.Data;
                              $("#btn-loadmore").attr("data-page", page + 1);
                          }
                      });
                  }
              },
              Load_More_Coc_Post: function () {
                  var self = this;
                  var page = parseInt($("#btn-loadpos").attr("data-page"));
                  var take = (page + 1) * 20;
                  if (isSearchCoc == true) {
                      var url = publicURLPre + "/dattruoc-api/ListDatCocPosByPhone?&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=" + take + "&customerPhone=" + this.searchCusPhoneCoc;
  
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: url,
                          success: function (data) {
                              self.depositInfo = data.Data;
  
                              $("#btn-loadpos").attr("data-page", page + 1);
                          }
                      });
                  } else {
                      var url = publicURLPre + "/dattruoc-api/ListDatCocPos?&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=" + take;
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: url,
                          success: function (data) {
                              self.depositInfo = data.Data;
  
                              $("#btn-loadpos").attr("data-page", page + 1);
                          }
                      });
                  }
              },
              SearchByCustomerPhone: function () {
                  var self = this;
                  if (this.searchCusPhone != "") {
                      isSearch = true;
                      $("#btn-loadmore").attr("data-page", "1");
                      var url = publicURLPre + "/dattruoc-api/GetListRegisterByPhone?campaignId=" + campaignID + "&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=20&customerPhone=" + self.searchCusPhone;
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: url,
                          success: function (data) {
                              self.registerInfo = data.Data;
                          }
                      });
                  } else {
                      isSearch = false;
                      $("#btn-loadmore").attr("data-page", "1");
                      $.ajax({
                          dataType: 'json',
                          url: publicURLPre + "/dattruoc-api/GetListRegisterAll?campaignId=" + campaignID + "&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=20",
                          success: function (data) {
  
                              self.registerInfo = data.Data;
                          }
                      });
                  }
              },
              SearchCocPostByCustomerPhone: function () {
                  var self = this;
                  if (this.searchCusPhoneCoc != "") {
                      isSearchCoc = true;
                      $("#btn-loadpos").attr("data-page", "1");
                      var url = publicURLPre + "/dattruoc-api/ListDatCocPosByPhone?&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=20&customerPhone=" + this.searchCusPhoneCoc;
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: url,
                          success: function (data) {
                              self.depositInfo = data.Data;
  
                              /*   campaignInfo.TotalDatCoc*/
                          }
                      });
                  } else {
                      isSearchCoc = false;
                      $("#btn-loadpos").attr("data-page", "1");
                      $.ajax({
                          type: 'get',
                          dataType: 'json',
                          url: publicURLPre + "/dattruoc-api/ListDatCocPos?&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=20",
                          success: function (data) {
                              self.depositInfo = data.Data;
  
                          }
                      });
                  }
              },
              filterCity: function () {
                  var self = this;
                  if (self.pushObj.Shiping == 8) {
                      self.listCity = self.listCityOriginal.filter(function (v) {
                          return v.ListBranchDistrictRivets.filter(function (a) {
                              return a.LiBranchRivets.length > 0;
                          }).length > 0;
                      })
                      var checkCityExist = self.listCity.filter(function (c) {
                          return c.Cityid == self.selectedCity.Cityid;
                      });
                      if (checkCityExist.length > 0) {
                          return false;
                      } else {
                          self.selectedCity = "";
                          self.pushObj.City = "";
                      }
                  } else {
                      self.listCity = self.listCityOriginal;
                  }
              },
              filterDistrict: function (city) {
                  if (this.selectedCity.ListBranchDistrictRivets != undefined) {
                      if (this.pushObj.Shiping == '8') {
  
                          this.pushObj.City = this.selectedCity.Cityid;
  
                          this.listDistrict = this.selectedCity.ListBranchDistrictRivets.filter(function (v) {
                              return v.LiBranchRivets.length > 0;
                          })
                      } else {
                          this.pushObj.City = this.selectedCity.Cityid;
                          this.listDistrict = this.selectedCity.ListBranchDistrictRivets;
                      }
                  } else {
                      return false;
                  }
              },
              filterBranch: function (dis) {
                  if (this.selectedDistrict.LiBranchRivets != undefined) {
                      if (this.pushObj.Shiping == '8') {
  
                          this.pushObj.District = this.selectedDistrict.DistrictId;
                          this.listBranch = this.selectedDistrict.LiBranchRivets;
                      } else {
                          this.pushObj.District = this.selectedDistrict.DistrictId;
                          this.listBranch = this.selectedDistrict.LiBranchRivets;
                      }
                  }
              },
              addColor: function (col) {
                  var self = this;
                 
                      self.pushObj.Color = self.chonSanpham.name + ' - ' + col.ColorName;
                 
               
  
              },            
              getProduct: function () {
                  var self = this;
                  $.get(publicURLPre + "/dattruoc-api/GetProductDatTruoc?id=" + campaignID + "&listId=" + productId + "&keyWord=" + keyWord  + "&skip=0&take=20" + "&campaignIdCoc=" + campaignIdCoc, function (data) {
                      if (data.Data != null) {
                          /*console.log(JSON.stringify(data.Data.ListNews));*/
                        
                          self.ListProduct = data.Data.ListProduct;
                          self.giaDuKien = data.Data.CampPhase.substring(3, data.Data.CampPhase.length - 4);
                          self.campaignInfo.TotalRegister = data.Data.TotalRegister;
                          self.campaignInfo.TotalDatCoc = data.Data.TotalDatCoc;
                          self.campaignInfo.CampaignStatus = data.Data.CampaignStatus;
                          self.campaignInfo.Note = data.Data.Note;
                          self.productDatTruoc.ListNews = data.Data.ListNews;
                          
                          self.campaignInfo.BannerPC = data.Data.BannerPC.split('|-|')[0];
                          self.HinhAnhSanPhamPC = data.Data.BannerPC.split('|-|').slice(1, data.Data.BannerPC.split('|-|').length);
                          self.campaignInfo.BannerMobile = data.Data.BannerMobile.split('|-|')[0];
                          self.HinhAnhSanPhamMobile = data.Data.BannerMobile.split('|-|').slice(1, data.Data.BannerMobile.split('|-|').length);
                          
                          self.setTime = function () {
                              $(".sp-slide2").slick({
                                  dots: true,
                                  infinite: false,
                                  speed: 300,
                                  slidesToShow: 4,
                                  slidesToScroll: 1,
                                  adaptiveHeight: true,
                                  arrows: true,
  
                                  responsive: [{
                                          breakpoint: 1024,
                                          settings: {
                                              slidesToShow: 4,
                                              slidesToScroll: 1,
                                          }
                                      }, {
                                          breakpoint: 600,
                                          settings: {
                                              slidesToShow: 3,
                                              slidesToScroll: 1
                                          }
                                      }, {
                                          breakpoint: 480,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }
  
                                  ]
                              });
                              $(".sp-slide3").slick({
                                  dots: true,
                                  infinite: false,
                                  speed: 300,
                                  slidesToShow: 1,
                                  slidesToScroll: 1,
                                  adaptiveHeight: true,
                                  arrows: true,
  
                                  responsive: [{
                                          breakpoint: 1024,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1,
                                          }
                                      }, {
                                          breakpoint: 600,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }, {
                                          breakpoint: 480,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }
  
                                  ]
                              });
                              $(".sp-slide4").slick({
                                  // dots: true,
                                  infinite: false,
                                  // speed: 300,
                                  slidesToShow: 1,
                                  slidesToScroll: 1,
                                  adaptiveHeight: true,
                                  arrows: true,
                                  autoplay:true,
                                  autoplaySpeed:5000,
                                  responsive: [{
                                          breakpoint: 1024,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1,
                                          }
                                      }, {
                                          breakpoint: 600,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }, {
                                          breakpoint: 480,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }
  
                                  ]
                              });
                              $(".sp-slide5").slick({
                                  dots: true,
                                  infinite: false,
                                  speed: 300,
                                  slidesToShow: 1,
                                  slidesToScroll: 1,
                                  adaptiveHeight: true,
                                  arrows: true,
                                  autoplay:true,
                                  autoplaySpeed:5000,
                                  responsive: [{
                                          breakpoint: 1024,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1,
                                          }
                                      }, {
                                          breakpoint: 600,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }, {
                                          breakpoint: 480,
                                          settings: {
                                              slidesToShow: 1,
                                              slidesToScroll: 1
                                          }
                                      }
  
                                  ]
                              });
                              $(".sp-slide5 .new").parent().css( "width", "100%" );
                              $('#slick1').slick({
                                  rows: 4,
                                    dots: true,
                                    arrows: false,
                                    infinite: false,
                                    speed: 300,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    adaptiveHeight: true,
                                    customPaging : function(slider, i) {
                                      var thumb = $(slider.$slides[i]).data();
                                      return '<a class="dots-number">' + (i + 1) + '</a>';
                                    },
                                    responsive: [
                                        {
                                            breakpoint: 768,
                                            settings: {
                                                
                                                rows: 4,
                                                slidesToScroll: 1,
                                                slidesToShow: 1,
                                                dots: true
                                            }
                                        },
                                        {
                                            breakpoint: 480,
                                            settings: {
                                                rows: 4,
                                                
                                                slidesToScroll: 1,
                                                slidesToShow: 1,
                                                dots: true
                                            }
                                        }
                                    ]
                              });
                          }
                      }
                  });
              },
              getProductTabBoSung: function () {
                  var self = this;
                  $.get(publicURLPre + "/dattruoc-api/GetProductDatTruoc?id=" + campaignIdTabBoSung + "&listId=" + productId + "&keyWord=" + keyWord + "&campaignIdCoc=" + campaignIdCocTabBoSung + "&skip=0&take=20", function (data) {
                      if (data.Data != null) {
                          /*console.log(JSON.stringify(data.Data.ListNews));*/
                          self.ListProduct = data.Data.ListProduct;
                          self.campaignInfoTabBoSung.TotalRegister = data.Data.TotalRegister;
                          self.campaignInfoTabBoSung.TotalDatCoc = data.Data.TotalDatCoc;
  
                      }
                  });
              },
              getPreLoad: function () {
                  var self = this;
                  $.get(publicURLPre + "/dattruoc-api/GetThongTinCoBanDatTruoc?campaignId=" + campaignID, function (data) {
                      if (data.Data != null) {
  
                          self.campaignInfo = data.Data;
                          self.phaseStatus = data.Data.BannerPC;
                          /*var text = 'https://cdn1.vienthonga.vn/image/2018/5/6/100000_1920x650.jpg|-|https://cdn1.vienthonga.vn/image/2018/5/5/100000_blue.png|-|https://cdn1.vienthonga.vn/image/2018/5/5/100000_sp-selfie.png';*/
                          self.setTime = function () {
                              // if ($('.countdown').length) {
                              //     $('.countdown').each(function () {
                              //         var date = $(this).attr('data-date');
                              //         $(this).countdown(date, function (event) {
                              //             $(this).html(event.strftime(
                              //                 '<div class="element"> <div class="num">%D</div> <div class="txt">Ngày</div> </div>' +
                              //                 '<div class="element"> <div class="num">%H</div> <div class="txt">Giờ</div> </div>' +
                              //                 '<div class="element"> <div class="num">%M</div> <div class="txt">Phút</div> </div>' +
                              //                 '<div class="element"> <div class="num">%S</div> <div class="txt">Giây</div> </div>'
                              //             ));
                              //         });
                              //     });
                              // }
  
                              if ($('.countdown2').length) {
                                  $('.countdown2').each(function () {
                                      var date = $(this).attr('data-date');
                                      $(this).countdown(date, function (event) {
                                          var $this = $(this).html(event.strftime('' +
                   '<div class="element"><span class="box-timer">%D</span>  <span class="label">Ngày</span></div> ' +
                   '<div class="element"><span class="box-timer">%H</span>  <span class="label">Giờ</span></div> ' +
                   '<div class="element"><span class="box-timer">%M</span> <span class="label">Phút</span></div> ' +
                   '<div class="element"><span class="box-timer">%S</span> <span class="label">Giây</span></div>'));
                                      });
                                  });
                              }
  
                              handlePerfectscroll();
  
                          }
                      }
                  });
              },
              getListDatTruoc: function () {
  
                  var self = this;
                  $.get(publicURLPre + "/dattruoc-api/GetListRegisterAll?campaignId=" + campaignID + "&fromDate=" + dateStart + "&toDate=" + dateEnd + "&productCode=" + productCode + "&campaignIdCoc=" + campaignIdCoc + "&skip=0&take=20", function (data) {
                      if (data.Data != null) {
                          self.registerInfo = data.Data;
                      }
                  })
              },
              registerDatTruoc: function () {
                  // window._fbq.push(['track', 'Lead']);
                  var self = this;
  
                  self.pushObj.AffiliateSource = self.getCookie("CkAffiliateSource");
                  if (self.chonSanpham.name === "") {
                      window.swal({
                          title: "",
                          text: "Vui lòng chọn sản phẩm!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  } else  if (self.pushObj.ProductColorcode === "") {
                      window.swal({
                          title: "",
                          text: "Vui lòng chọn màu sắc sản phẩm!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
  
                  } else if (self.pushObj.Gender === "") {
                      window.swal({
                          title: "",
                          text: "Vui lòng chọn Giới tính",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  }
                  var emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                  var phoneReg = /^[0-9]{6,11}$/;
                  if (self.pushObj.Description === "" ) {
                      window.swal({
                          title: "",
                          text: "Bạn chưa chọn phương thức thanh toán!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  }
                  else if (self.pushObj.Name === "") {
                      window.swal({
                          title: "",
                          text: "Bạn chưa nhập họ tên!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  }
                     else if (self.pushObj.Email === "" || !emailReg.test(self.pushObj.Email)) {
                      window.swal({
                          title: "",
                          text: "Bạn chưa nhập email hoặc email bạn nhập chưa đúng!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  }
                   else if (self.pushObj.Phone === "" || !phoneReg.test(self.pushObj.Phone)) {
                      window.swal({
                          title: "",
                          text: "Bạn chưa nhập số điện thoại hoặc số điện thoại bạn nhập chưa đúng!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  } else if (self.pushObj.Shiping === "") {
                      window.swal({
                          title: "",
                          text: "Bạn chưa chọn hình thức nhận hàng!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  } 
               
                  else if (self.pushObj.City === "") {
                      window.swal({
                          title: "",
                          text: "Bạn chưa chọn tỉnh thành!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  } else if (self.pushObj.District === "") {
                      window.swal({
                          title: "",
                          text: "Bạn chưa chọn quận!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  } else if (self.pushObj.Address === "") {
                      if (self.pushObj.Shiping === '6') {
                          window.swal({
                              title: "",
                              text: "Bạn chưa nhập địa chỉ giao hàng!",
                              confirmButtonText: "Đóng cửa sổ",
                              confirmButtonColor: "#1F5ECC"
                          });
                      }
                      if (self.pushObj.Shiping === '8') {
                          window.swal({
                              title: "",
                              text: "Bạn chưa chọn chi nhánh!",
                              confirmButtonText: "Đóng cửa sổ",
                              confirmButtonColor: "#1F5ECC"
                          });
                      }
                      return false;
                  } else if (self.pushObj.Payment === "") {
                      window.swal({
                          title: "",
                          text: "Bạn chưa chọn phương thức đặt cọc!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  } else if (self.pushObj.Payment === "16" && self.pushObj.PaymentMethod === "") {
                      window.swal({
                          title: "",
                          text: "Bạn chưa chọn phương thức thẻ!",
                          confirmButtonText: "Đóng cửa sổ",
                          confirmButtonColor: "#1F5ECC"
                      });
                      return false;
                  }
                  else {
                      if (self.pushObj.Shiping === '8') {
                      	var textAddress = self.listBranch.filter(function (v) {
                      			return v.BranchsCodePos === self.pushObj.Address;
                          })
                          self.pushObj.DetailAddress = self.pushObj.Address+ '-' + textAddress[0].BranchsShortAdd;
                      } else if (self.pushObj.Shiping === '6'){
                      		self.pushObj.DetailAddress = self.pushObj.Address + '-' + self.selectedDistrict.Districtname + '-' + self.selectedCity.Cityname;
                      }
                      $("#loading-icon").removeClass("d-none");
                      $("#loading-icon").addClass("d-block");
                      $('.loading-section').show();
                      // console.log(1010, self.pushObj);
                      // return;
                      $.ajax({
                          type: 'post',
                          dataType: 'json',
                          data: self.pushObj,
                          url: publicURLPre + '/dattruoc-api/InsertRegister_V2',
                          success: function (data) {
  
                              try {
  
                                  if (data.Data.Result === true && data.Data.PaymentType === null) {
                                      
                                      window.location.href ="https://vienthonga.vn/cam-on.html?preorderid=" +data.Data.PreorderId +"&from_url="+ currentUrl;
                                                                   }
                                  else if (data.Data.Result === true && data.Data.PaymentType == "PAYMENTONLINE") {
                                   
                                      window.location.href = data.Data.RedirectUrl;
                                  } else {
                                      $("#loading-icon").removeClass("d-block");
                                      $("#loading-icon").addClass("d-none");
                                      $('.loading-section').hide();
                                      window.swal({
                                          title: "",
                                          text: "Bạn đặt mua không thành công!",
                                          confirmButtonText: "Đóng cửa sổ",
                                          confirmButtonColor: "#1F5ECC"
                                      });
                                  }
                              } catch (e) {
  
                              }
                          }
                      });
                  }
              },
              createCampaignTracking: function () {
                  var camTrackString = "<!-- Google Code for Tra Tien Mat Conversion Page --\><script type=\'text/javascript\'>/* <![CDATA[ */var google_conversion_id = 854892788;var google_conversion_language = 'en';var google_conversion_format = '3';var google_conversion_color = 'ffffff';var google_conversion_label = 'nj-6CMLWq3AQ9MHSlwM';var google_remarketing_only = false;/* ]]> */</script><script type='text/javascript' src='//www.googleadservices.com/pagead/conversion.js'></script><noscript><div style='display:inline;'><img height='1' width='1' style='border-style:none;' alt='' src='//www.googleadservices.com/pagead/conversion/854892788/?label=nj-6CMLWq3AQ9MHSlwM&amp;guid=ON&amp;script=0'/></div></noscript>"
                  return camTrackString;
              },
              setTime: function () {},
                       resetPushObj: function (me) {
                  me.Color = "";
                  me.Gender = "M";
                  me.Name = "";
                  me.Phone = "";
                  me.Email = "";
                  me.Shiping = "8";
                  me.City = "";
                  me.District = "";
                  me.Address = "";
                  me.Payment = "";
                  me.CampaignId = campaignID;
                  me.OrderShareId = 0;
                  me.Productid = productId;
                  me.Productcode = productCode;
                  me.ProductColorcode = "";
                  me.Price = 200000;
                  me.Product = "Vivo V7+";
                  me.MaCombo = "";
                  me.MotaCombo = "";
                  me.Cmnd = "0";
                  me.CurrentUrl = currentUrl;
                  me.Description = "";
              },
              ChonSanPham: function (item) {
                  var self = this;
                  self.selectedProduct = item;
                  self.pushObj.Product = item.ProductName;
                  self.pushObj.Productcode = item.ProductCode;
                  /* self.pushObj.Productid = item.ProductId;*/
                  self.pushObj.Color = "";
                  self.pushObj.ProductColorcode = "";
                  // console.log(self.chonSanpham.name);
              },
              chonCombo: function (item) {
                  this.pushObj.MotaCombo = item.description;
              },
              getPaymentMethodCode: function (me) {
                  var self = this;
                  if (me.paymentCode == "visa" || me.paymentCode == "master") {
                      self.pushObj.PaymentMethod = "123PCC";
                  } else {
                      self.pushObj.PaymentMethod = "123PAY";
                  }
  
              },
              getCookie: function getCookie(cname) {
                  var name = cname + "=";
                  var decodedCookie = decodeURIComponent(document.cookie);
                  var ca = decodedCookie.split(';');
                  for (var i = 0; i < ca.length; i++) {
                      var c = ca[i];
                      while (c.charAt(0) == ' ') {
                          c = c.substring(1);
                      }
                      if (c.indexOf(name) == 0) {
                          return c.substring(name.length, c.length);
                      }
                  }
                  return "";
              },
              createCookies: function () {
                  var self = this;
                  var utm_affiliate = self.getParameterByName('utm_affiliate');
                  if (utm_affiliate != "" && utm_affiliate != null && utm_affiliate == "vienthonga_aff") {
                      var utm_source = self.getParameterByName('utm_source'),
                          aff_sid = self.getParameterByName('aff_sid'),
                          traffic_id = self.getParameterByName('traffic_id'),
                          utm_id = self.getParameterByName('utm_id'),
                          utm_uid = self.getParameterByName('utm_uid'),
                          a_aid = self.getParameterByName('a_aid'),
                          a_bid = self.getParameterByName('a_bid'),
                  
                          d = new Date();
  
  
                      utm_source == 'freetel.vn' ? d.setTime(d.getTime() + (60 * 60 * 1000)) : d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
                      self.setCookie("CkAffiliateSource", utm_source, d.toUTCString());
                      if (traffic_id != "" && traffic_id != null) {
                          var c = new Date();
                          c.setTime(c.getTime() + (30 * 24 * 60 * 60 * 1000))
                          self.setCookie("CkAffiliateMOTraffic", traffic_id, c.toUTCString());
                      }
                      if (aff_sid != "" && aff_sid != null) {
                          var c = new Date();
                          c.setTime(c.getTime() + (30 * 24 * 60 * 60 * 1000))
                          self.setCookie("CkAffiliateATTraffic", aff_sid, c.toUTCString());
                      }
                        if (utm_id != "" && utm_id != null) {
                          var c = new Date();
                          c.setTime(c.getTime() + (30 * 24 * 60 * 60 * 1000))
                          self.setCookie("CkAffiliateMgiftId", utm_id, c.toUTCString());
                      }
                        if (utm_uid != "" && utm_uid != null) {
                          var c = new Date();
                          c.setTime(c.getTime() + (30 * 24 * 60 * 60 * 1000))
                          self.setCookie("CkAffiliateMgiftUId", utm_uid, c.toUTCString());
                      }
                        if (a_aid != "" && a_aid != null) {
                          var c = new Date();
                          c.setTime(c.getTime() + (30 * 24 * 60 * 60 * 1000))
                          self.setCookie("CkAffiliateYeah1AId", a_aid, c.toUTCString());
                      }
                        if (a_bid != "" && a_bid != null) {
                          var c = new Date();
                          c.setTime(c.getTime() + (30 * 24 * 60 * 60 * 1000))
                          self.setCookie("CkAffiliateYeah1BId", a_bid, c.toUTCString());
                      }
                  }
              },
              setCookie: function (name, value, time) {
                  var expires = "expires=" + time;
                  document.cookie = name + "=" + value + ";" + expires + ";path=/";
              },
              getParameterByName: function (name, url) {
                  if (!url) {
                      url = window.location.href;
                  }
                  name = name.replace(/[\[\]]/g, "\\$&");
                  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                      results = regex.exec(url);
                  if (!results) return null;
                  if (!results[2]) return '';
                  return decodeURIComponent(results[2].replace(/\+/g, " "));
              },
              createScript: function (dataObj) {
                  var self = this;
                  var traffic_id = self.getCookie("CkAffiliateMOTraffic");
                  var scriptString = '<script type="text/javascript">masoffer_order_info = {transaction_id: \'PRE' + dataObj.MOOrderModel.OrderId + '\',offer_id: \'vienthonga\',transaction_time:\'' + dataObj.MOTimespan + '\',signature:\'' + dataObj.MOMd5Hash + '\',transaction_discount: \'0\',traffic_id: \'' + traffic_id + '\',customer: {city: \'VienThongA\',country: \'Vietnam\'},products: [{id: \'' + dataObj.MOOrderModel.ListOrderDetail[0].ProductId + '\',sku: \'' + dataObj.MOOrderModel.ListOrderDetail[0].ProductCode + '\',url:\'' + 'https://vienthonga.vn/' + dataObj.MOOrderModel.ListOrderDetail[0].Slug + '.html' + '\',price: ' + dataObj.MOOrderModel.ListOrderDetail[0].PriceExpected + ',name:\'' + dataObj.MOOrderModel.ListOrderDetail[0].ProductName + '\',category:\'' + dataObj.MOOrderModel.ListOrderDetail[0].CategoryName + '\',category_id : ' + dataObj.MOOrderModel.ListOrderDetail[0].RootCategoryId + ',is_apple :' + dataObj.MOOrderModel.ListOrderDetail[0].IsApple + ',status_code: 0,quantity: 1},]};</script><script type="text/javascript" src="https://static.masoffer.net/js/vienthonga/tracking.js?v=1.0.0" async></script>';
                  return scriptString;
              },
                    
           
          },
          updated: function () {
              this.setTime();
              this.setTime = function () {
  
              }
          },
          watch: {
              'pushObj.Shiping': function () {
                  var self = this;
                  self.filterCity();
  
                  if (self.selectedCity.ListBranchDistrictRivets != undefined) {
                      if (self.pushObj.Shiping == '8') {
                          self.pushObj.Address = "";
                          self.listDistrict = self.selectedCity.ListBranchDistrictRivets.filter(function (v) {
                              return v.LiBranchRivets.length > 0;
                          })
                          var selectedCity = self.selectedDistrict;
                          var checkExist = self.listDistrict.filter(function (v) {
                              return v.DistrictId == selectedCity.DistrictId;
                          });
                          if (checkExist.length > 0) {
                              return false;
                          } else {
                              self.selectedDistrict = "";
                              self.pushObj.District = "";
                          }
                      } else {
                          self.pushObj.Address = "";
                          self.listDistrict = self.selectedCity.ListBranchDistrictRivets;
                      }
                  } else {
                      return false;
                  }
              },
              
              selectedCity: function () {
                  var self = this;
                  self.selectedDistrict = "";
                  self.pushObj.Address = "";
              },
              'chonSanpham.name': function (){
                  var self = this;
                  self.pushObj.ProductColorcode = '';
                
              }
  
          }
      });
