var listCityOriginal = 
    [{
        "Cityid": "SGN",
        "Cityname": "Hồ Chí Minh",
        "AreaId": 1,
        "Top": 1,
        "ListBranchDistrictRivets": [{
            "DistrictId": "SGNA",
            "CityId": "SGN",
            "Districtname": "Quận 1",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SGNB",
            "CityId": "SGN",
            "Districtname": "Quận 2",
            "Top": 3,
            "LiBranchRivets": [{
                "BranchsId": 19,
                "BranchsAdd": "139 Trần Não, P. Bình An, Q.2, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000022",
                "BranchsShortAdd": "139 Trần Não, P. Bình An, Q.2",
                "Districtid": "SGNB",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 73,
                "BranchsAdd": "121A Nguyễn Duy Trinh, P. Bình Trưng Tây, Q.2, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000109",
                "BranchsShortAdd": "121A Nguyễn Duy Trinh, P. Bình Trưng Tây, Q.2",
                "Districtid": "SGNB",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNC",
            "CityId": "SGN",
            "Districtname": "Quận 3",
            "Top": 2,
            "LiBranchRivets": [{
                "BranchsId": 42,
                "BranchsAdd": "74 Cách Mạng Tháng 8, P.6, Q.3, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000047",
                "BranchsShortAdd": "74 Cách Mạng Tháng 8, P.6, Q.3",
                "Districtid": "SGNC",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGND",
            "CityId": "SGN",
            "Districtname": "Quận 4",
            "Top": 4,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SGNE",
            "CityId": "SGN",
            "Districtname": "Quận 5",
            "Top": 5,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SGNF",
            "CityId": "SGN",
            "Districtname": "Quận 6",
            "Top": 6,
            "LiBranchRivets": [{
                "BranchsId": 5,
                "BranchsAdd": "460 Nguyễn Văn Luông, P.12, Q.6, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000007",
                "BranchsShortAdd": "460 Nguyễn Văn Luông, P.12, Q.6",
                "Districtid": "SGNF",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 28,
                "BranchsAdd": "505 Hậu Giang, P.11, Q.6, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000031",
                "BranchsShortAdd": "505 Hậu Giang, P.11, Q.6",
                "Districtid": "SGNF",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 375,
                "BranchsAdd": "1081 Hậu Giang, P. 11, Q.6",
                "BranchsCodePos": "CS_0000441",
                "BranchsShortAdd": "1081 Hậu Giang, P. 11, Q.6",
                "Districtid": "SGNF",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNG",
            "CityId": "SGN",
            "Districtname": "Quận 7",
            "Top": 7,
            "LiBranchRivets": [{
                "BranchsId": 8,
                "BranchsAdd": "473 Huỳnh Tấn Phát, P. Tân Thuận Đông, Q.7, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000010",
                "BranchsShortAdd": "473 Huỳnh Tấn Phát, P. Tân Thuận Đông, Q.7",
                "Districtid": "SGNG",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 137,
                "BranchsAdd": "443 Nguyễn Thị Thập, P. Tân Phong, Q.7, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000118",
                "BranchsShortAdd": "443 Nguyễn Thị Thập, P. Tân Phong, Q.7",
                "Districtid": "SGNG",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 172,
                "BranchsAdd": "592 Lê Văn Lương, P. Tân Phong, Q.7, TP. HCM",
                "BranchsCodePos": "CS_0000224",
                "BranchsShortAdd": "592 Lê Văn Lương, P. Tân Phong",
                "Districtid": "SGNG",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 237,
                "BranchsAdd": "Lotte Nam Sài Gòn - 469 Nguyễn Hữu Thọ, P. Tân Hưng, Q.7, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000300",
                "BranchsShortAdd": "Lotte Nam Sài Gòn - 469 Nguyễn Hữu Thọ, P. Tân Hưng",
                "Districtid": "SGNG",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 374,
                "BranchsAdd": "1058 Nguyễn Văn Linh, P. Tân Phong, Q.7, TP.HCM",
                "BranchsCodePos": "CS_0000440",
                "BranchsShortAdd": "1058 Nguyễn Văn Linh, P. Tân Phong, Q.7, TP.HCM",
                "Districtid": "SGNG",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "SGNH",
            "CityId": "SGN",
            "Districtname": "Quận 8",
            "Top": 8,
            "LiBranchRivets": [{
                "BranchsId": 93,
                "BranchsAdd": "298 - 300 Dương Bá Trạc, P.1, Q.8, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000122",
                "BranchsShortAdd": "298 - 300 Dương Bá Trạc, P.1, Q.8",
                "Districtid": "SGNH",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNI",
            "CityId": "SGN",
            "Districtname": "Quận 9",
            "Top": 9,
            "LiBranchRivets": [{
                "BranchsId": 90,
                "BranchsAdd": "197B Lê Văn Việt, P. Hiệp Phú, Q.9, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000119",
                "BranchsShortAdd": "197B Lê Văn Việt, P. Hiệp Phú, Q.9",
                "Districtid": "SGNI",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 264,
                "BranchsAdd": "337 Đỗ Xuân Hợp, P. Phước Long B, Q.9, TP. HCM",
                "BranchsCodePos": "CS_0000331",
                "BranchsShortAdd": "337 Đỗ Xuân Hợp, P. Phước Long B",
                "Districtid": "SGNI",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 367,
                "BranchsAdd": "07 Hoàng Hữu Nam, P.Tân Phú, Q.9, TP.HCM",
                "BranchsCodePos": "CS_0000433",
                "BranchsShortAdd": "07 Hoàng Hữu Nam, P.Tân Phú, Q.9, TP.HCM",
                "Districtid": "SGNI",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNITX",
            "CityId": "SGN",
            "Districtname": "Quận Thủ Đức",
            "Top": 22,
            "LiBranchRivets": [{
                "BranchsId": 15,
                "BranchsAdd": "226 Đặng Văn Bi, P. Bình Thọ, Q. Thủ Đức, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000018",
                "BranchsShortAdd": "226 Đặng Văn Bi, P. Bình Thọ, Q. Thủ Đức",
                "Districtid": "SGNITX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 196,
                "BranchsAdd": "39 Quốc Lộ 1K, P. Linh Xuân, Q. Thủ Đức, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000253",
                "BranchsShortAdd": "39 Quốc Lộ 1K, P. Linh Xuân",
                "Districtid": "SGNITX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 199,
                "BranchsAdd": "1235 Tỉnh lộ 43, KP. 2, P. Bình Chiểu, Q. Thủ Đức, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000256",
                "BranchsShortAdd": "1235 Tỉnh lộ 43, KP. 2, P. Bình Chiểu",
                "Districtid": "SGNITX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 218,
                "BranchsAdd": "565 Quốc lộ 13, P. Hiệp Bình Phước, Q. Thủ Đức, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000274",
                "BranchsShortAdd": "565 Quốc lộ 13, P. Hiệp Bình Phước",
                "Districtid": "SGNITX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNJ",
            "CityId": "SGN",
            "Districtname": "Quận 10",
            "Top": 1,
            "LiBranchRivets": [{
                "BranchsId": 1,
                "BranchsAdd": "328-330 Đường 3/2, P.12, Q.12, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000004",
                "BranchsShortAdd": "326-330 Đường 3/2, P.12, Q.12",
                "Districtid": "SGNJ",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNK",
            "CityId": "SGN",
            "Districtname": "Quận 11",
            "Top": 11,
            "LiBranchRivets": [{
                "BranchsId": 11,
                "BranchsAdd": "382 Lãnh Binh Thăng, P.11, Q.11, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000014",
                "BranchsShortAdd": "382 Lãnh Binh Thăng, P.11, Q.11",
                "Districtid": "SGNK",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 91,
                "BranchsAdd": "3/1  Bình Thới, P.11, Q.11, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000121",
                "BranchsShortAdd": "3/1  Bình Thới, P.11, Q.11",
                "Districtid": "SGNK",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 257,
                "BranchsAdd": "Lầu 2 Lotte Phú Thọ, Toàn nhà Everrich, 968 đường 3/2, P.15, Q.11, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000325",
                "BranchsShortAdd": "Lầu 2 Lotte Phú Thọ, Toàn nhà Everrich, 968 đường 3/2, P.15, Q.11",
                "Districtid": "SGNK",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 363,
                "BranchsAdd": "134 Tạ Uyên, Phường 4, Quận 11",
                "BranchsCodePos": "CS_0000429",
                "BranchsShortAdd": "134 Tạ Uyên, Phường 4, Quận 11",
                "Districtid": "SGNK",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNL",
            "CityId": "SGN",
            "Districtname": "Quận 12",
            "Top": 12,
            "LiBranchRivets": [{
                "BranchsId": 7,
                "BranchsAdd": "1 Phan Văn Hớn, P. Tân Thới Nhất, Q. 12, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000009",
                "BranchsShortAdd": "01 Phan Văn Hớn, P Tân Thới Nhất, Q.12",
                "Districtid": "SGNL",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 24,
                "BranchsAdd": "12/6 Tô Ký, P. Tân Chánh Hiệp, Q.12, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000027",
                "BranchsShortAdd": "12/6 Tô Ký, P. Tân Chánh Hiệp, Q.12",
                "Districtid": "SGNL",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 92,
                "BranchsAdd": "1H-1K Nguyễn Ảnh Thủ, P. Hiệp Thành, Q.12, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000117",
                "BranchsShortAdd": "1H-1K Nguyễn Ảnh Thủ, P. Hiệp Thành, Q.12",
                "Districtid": "SGNL",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 362,
                "BranchsAdd": "369A Nguyễn Ảnh Thủ, K/P 1, P.Trung Mỹ Tây, Quận 12",
                "BranchsCodePos": "CS_0000428",
                "BranchsShortAdd": "369A Nguyễn Ảnh Thủ, K/P 1, P.Trung Mỹ Tây, Quận 12",
                "Districtid": "SGNL",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNM",
            "CityId": "SGN",
            "Districtname": "Quận Gò Vấp",
            "Top": 17,
            "LiBranchRivets": [{
                "BranchsId": 27,
                "BranchsAdd": "792 Nguyễn Kiệm, P.3, Q. Gò Vấp, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000030",
                "BranchsShortAdd": "792 Nguyễn Kiệm, P.3, Q. Gò Vấp",
                "Districtid": "SGNM",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 94,
                "BranchsAdd": "33/8 Phạm Văn Chiêu, P.9, Q. Gò Vấp, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000123",
                "BranchsShortAdd": "33/8 Phạm Văn Chiêu, P.9, Q. Gò Vấp",
                "Districtid": "SGNM",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 113,
                "BranchsAdd": "96/1072B Nguyễn Oanh, P.17, Q. Gò Vấp, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000136",
                "BranchsShortAdd": "96/1072B Nguyễn Oanh, P.17, Q. Gò Vấp",
                "Districtid": "SGNM",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 290,
                "BranchsAdd": "18 Phan Văn Trị - Nguyễn Văn Lượng, P.10, Q. Gò Vấp, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000361",
                "BranchsShortAdd": "18 Phan Văn Trị - Nguyễn Văn Lượng, P.10",
                "Districtid": "SGNM",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 304,
                "BranchsAdd": "Số 649 Quang Trung, P.11, Q. Gò Vấp, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000372",
                "BranchsShortAdd": "649 Quang Trung, P.11",
                "Districtid": "SGNM",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNN",
            "CityId": "SGN",
            "Districtname": "Quận Tân Bình",
            "Top": 21,
            "LiBranchRivets": [{
                "BranchsId": 2,
                "BranchsAdd": "415A Hoàng Văn Thụ, P. 2, Q. Tân Bình, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000002",
                "BranchsShortAdd": "415A Hoàng Văn Thụ, P.2, Q. Tân Bình",
                "Districtid": "SGNN",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 3,
                "BranchsAdd": "190B Hoàng Văn Thụ, P. 4, Q. Tân Bình, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000001",
                "BranchsShortAdd": "190B Hoàng Văn Thụ, P.4, Q. Tân Bình",
                "Districtid": "SGNN",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 12,
                "BranchsAdd": "320-320A Trường Chinh, P.13, Q. Tân Bình, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000017",
                "BranchsShortAdd": "320-320A Trường Chinh, P.13, Q. Tân Bình",
                "Districtid": "SGNN",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 86,
                "BranchsAdd": "678 Trường Chinh, P.15, Q. Tân Bình, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000116",
                "BranchsShortAdd": "678 Trường Chinh, P.15, Q. Tân Bình",
                "Districtid": "SGNN",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 235,
                "BranchsAdd": "Lầu 1 Lotte Tân Bình - Pico Plaza, 20 Cộng Hòa, P.12, Q. Tân Bình, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000301",
                "BranchsShortAdd": "Lầu 1 Lotte Tân Bình - Pico Plaza, 20 Cộng Hòa, P.12",
                "Districtid": "SGNN",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "SGNO",
            "CityId": "SGN",
            "Districtname": "Quận Bình Thạnh",
            "Top": 15,
            "LiBranchRivets": [{
                "BranchsId": 135,
                "BranchsAdd": "382 Bạch Đằng, P.14, Q. Bình Thạnh, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000181",
                "BranchsShortAdd": "382 Bạch Đằng, P.14, Q. Bình Thạnh",
                "Districtid": "SGNO",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNP",
            "CityId": "SGN",
            "Districtname": "Quận Phú Nhuận",
            "Top": 20,
            "LiBranchRivets": [{
                "BranchsId": 9,
                "BranchsAdd": "172 Phan Đăng Lưu, P.3, Q. Phú Nhuận, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000011",
                "BranchsShortAdd": "172 Phan Đăng Lưu, P.3, Q. Phú Nhuận",
                "Districtid": "SGNP",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNR",
            "CityId": "SGN",
            "Districtname": "Huyện Củ Chi",
            "Top": 16,
            "LiBranchRivets": [{
                "BranchsId": 128,
                "BranchsAdd": "867 Quốc lộ 22, KP. 5, TT. Củ Chi, H. Củ Chi, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000170",
                "BranchsShortAdd": "867 Quốc lộ 22, KP. 5, H. Củ Chi",
                "Districtid": "SGNR",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNS",
            "CityId": "SGN",
            "Districtname": "Huyện Hóc Môn",
            "Top": 18,
            "LiBranchRivets": [{
                "BranchsId": 82,
                "BranchsAdd": "4/39 Quang Trung, Ấp Nam Thới, Xã Thới Tam Thôn, H. Hóc Môn, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000112",
                "BranchsShortAdd": "4/39 Quang Trung, X. Thới Tam Thôn, H. Hóc Môn",
                "Districtid": "SGNS",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 110,
                "BranchsAdd": "102/7B Ấp 7, Xã Xuân Thới Thượng, H. Hóc Môn, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000141",
                "BranchsShortAdd": "102/7B Ấp 7, X. Xuân Thới Thượng, H. Hóc Môn",
                "Districtid": "SGNS",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNT",
            "CityId": "SGN",
            "Districtname": "Huyện Bình Chánh",
            "Top": 13,
            "LiBranchRivets": [{
                "BranchsId": 52,
                "BranchsAdd": "907 Phạm Hùng, X. Bình Hưng, H. Bình Chánh, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000056",
                "BranchsShortAdd": "907 Phạm Hùng, Xã Bình Hưng, H. Bình Chánh",
                "Districtid": "SGNT",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 186,
                "BranchsAdd": "A10/7 Quốc lộ 50, ấp 2, xã Bình Hưng, huyện Bình Chánh, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000239",
                "BranchsShortAdd": "A10/7 Quốc lộ 50, ấp 2, xã Bình Hưng",
                "Districtid": "SGNT",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 284,
                "BranchsAdd": "A2/20 Trần Đại Nghĩa - Ấp 1, X. Tân Kiên, H. Bình Chánh, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000354",
                "BranchsShortAdd": "A2/20 Trần Đại Nghĩa - Ấp 1, X. Tân Kiên",
                "Districtid": "SGNT",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 315,
                "BranchsAdd": "Số 2110 đường Vĩnh Lộc, Ấp 4, Xã Vĩnh Lộc B, H. Bình Chánh, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000382",
                "BranchsShortAdd": "Số 2110 Vĩnh Lộc, Ấp 4, X. Vĩnh Lộc B",
                "Districtid": "SGNT",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNU",
            "CityId": "SGN",
            "Districtname": "Huyện Nhà Bè - An Khánh",
            "Top": 19,
            "LiBranchRivets": [{
                "BranchsId": 228,
                "BranchsAdd": "770 Nguyễn Văn Tạo, ấp 1, xã Hiệp Phước, H. Nhà Bè, TP. Hồ Chí Minh ",
                "BranchsCodePos": "CS_0000282",
                "BranchsShortAdd": "770 Nguyễn Văn Tạo, ấp 1, xã Hiệp Phước",
                "Districtid": "SGNU",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 311,
                "BranchsAdd": "1132 Lê Văn Lương, Ấp 3, X. Phước Kiển, H. Nhà Bè, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000381",
                "BranchsShortAdd": "1132 Lê Văn Lương, Ấp 3, X.Phước Kiển",
                "Districtid": "SGNU",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNV",
            "CityId": "SGN",
            "Districtname": "Huyện Cần Giờ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SGNW",
            "CityId": "SGN",
            "Districtname": "Quận Bình Tân",
            "Top": 14,
            "LiBranchRivets": [{
                "BranchsId": 22,
                "BranchsAdd": "500 Kinh Dương Vương, P. An Lạc, Q. Bình Tân, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000025",
                "BranchsShortAdd": "500 Kinh Dương Vương, P. An Lạc, Q. Bình Tân",
                "Districtid": "SGNW",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 114,
                "BranchsAdd": "1355 Tỉnh Lộ 10, P. Tân Tạo, Q. Bình Tân, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000138",
                "BranchsShortAdd": "1355 Tỉnh Lộ 10, P. Tân Tạo, Q. Bình Tân",
                "Districtid": "SGNW",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 165,
                "BranchsAdd": "229 Nguyễn Thị Tú, P. Bình Hưng Hòa B, Q. Bình Tân, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000214",
                "BranchsShortAdd": "229 Nguyễn Thị Tú, P. Bình Hưng Hòa B",
                "Districtid": "SGNW",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SGNX",
            "CityId": "SGN",
            "Districtname": "Quận Tân Phú",
            "Top": 23,
            "LiBranchRivets": [{
                "BranchsId": 59,
                "BranchsAdd": "408 Lê Trọng Tấn, P. Tây Thạnh, Q. Tân Phú, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000064",
                "BranchsShortAdd": "408 Lê Trọng Tấn, P. Tây Thạnh, Q. Tân Phú",
                "Districtid": "SGNX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 181,
                "BranchsAdd": "172 - 174 Hòa Bình, P. Hiệp Tân, Q. Tân Phú, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000234",
                "BranchsShortAdd": "172 - 174 Hòa Bình, P. Hiệp Tân",
                "Districtid": "SGNX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 206,
                "BranchsAdd": "827 Âu Cơ, P. Tân Thành, Q. Tân Phú, TP. Hồ Chí Minh",
                "BranchsCodePos": "CS_0000255",
                "BranchsShortAdd": "827 Âu Cơ, P. Tân Thành",
                "Districtid": "SGNX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 366,
                "BranchsAdd": "307 Nguyễn Sơn, P. Phú Thạnh, Q.Tân Phú, TP. HCM",
                "BranchsCodePos": "CS_0000432",
                "BranchsShortAdd": "307 Nguyễn Sơn, P. Phú Thạnh, Q.Tân Phú, TP. HCM",
                "Districtid": "SGNX",
                "Cityid": "SGN",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "HAN",
        "Cityname": "Hà Nội",
        "AreaId": 3,
        "Top": 2,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HANA",
            "CityId": "HAN",
            "Districtname": "Ba Đình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANB",
            "CityId": "HAN",
            "Districtname": "Tây Hồ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANC",
            "CityId": "HAN",
            "Districtname": "Hoàn Kiếm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HAND",
            "CityId": "HAN",
            "Districtname": "Hai Bà Trưng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANE",
            "CityId": "HAN",
            "Districtname": "Đống Đa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 241,
                "BranchsAdd": "Lầu 2, TTTM Đống Đa - 229, Tây Sơn, P. Ngã Tư Sở, Q. Đống Đa, Hà Nội",
                "BranchsCodePos": "CS_0000299",
                "BranchsShortAdd": "Lầu 2, TTTM Đống Đa - 229, Tây Sơn, P. Ngã Tư Sở",
                "Districtid": "HANE",
                "Cityid": "HAN",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "HANF",
            "CityId": "HAN",
            "Districtname": "Thanh Xuân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANG",
            "CityId": "HAN",
            "Districtname": "Cầu Giấy",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 23,
                "BranchsAdd": "07 Hồ Tùng Mậu, P. Mai Dịch, Q. Cầu Giấy, TP. Hà Nội",
                "BranchsCodePos": "CS_0000045",
                "BranchsShortAdd": "07 Hồ Tùng Mậu, P. Mai Dịch, Q. Cầu Giấy",
                "Districtid": "HANG",
                "Cityid": "HAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HANH",
            "CityId": "HAN",
            "Districtname": "Huyện Sóc Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANI",
            "CityId": "HAN",
            "Districtname": "Huyện Đông Anh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANJ",
            "CityId": "HAN",
            "Districtname": "Huyện Gia Lâm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANK",
            "CityId": "HAN",
            "Districtname": "Huyện Từ Liêm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANL",
            "CityId": "HAN",
            "Districtname": "Huyện Thanh Trì",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANM",
            "CityId": "HAN",
            "Districtname": "Hoàng Mai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HANN",
            "CityId": "HAN",
            "Districtname": "Long Biên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TXST",
            "CityId": "HAN",
            "Districtname": "TX. Sơn Tây",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 173,
                "BranchsAdd": "136 Phùng Khắc Khoan, P. Quang Trung, TX. Sơn Tây, Hà Nội",
                "BranchsCodePos": "CS_0000223",
                "BranchsShortAdd": "136 Phùng Khắc Khoan, P. Quang Trung",
                "Districtid": "TXST",
                "Cityid": "HAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HNBV",
            "CityId": "HAN",
            "Districtname": "Huyện Ba Vì",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNCM",
            "CityId": "HAN",
            "Districtname": "Huyện Chương Mỹ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNDP",
            "CityId": "HAN",
            "Districtname": "Huyện Đan Phượng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNHD",
            "CityId": "HAN",
            "Districtname": "Quận Hà Đông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNHP",
            "CityId": "HAN",
            "Districtname": "Huyện Hoài Đức",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNML",
            "CityId": "HAN",
            "Districtname": "Huyện Mê Linh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 320,
                "BranchsAdd": "VBox Big C Mê Linh – Km8 đường Thăng Long, Nội Bài, Hà Nội",
                "BranchsCodePos": "CS_0000388",
                "BranchsShortAdd": "VBox Big C Mê Linh – Km8 đường Thăng Long, Nội Bài",
                "Districtid": "HNML",
                "Cityid": "HAN",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "HNMD",
            "CityId": "HAN",
            "Districtname": "Huyện Mỹ Đức",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNPX",
            "CityId": "HAN",
            "Districtname": "Huyện Phú Xuyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNPT",
            "CityId": "HAN",
            "Districtname": "Huyện Phúc Thọ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNTT",
            "CityId": "HAN",
            "Districtname": "Huyện Thạch Thất",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNTO",
            "CityId": "HAN",
            "Districtname": "Huyện Thanh Oai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNTH",
            "CityId": "HAN",
            "Districtname": "Huyện Thường Tín",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HNUH",
            "CityId": "HAN",
            "Districtname": "Huyện Ứng Hòa",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DAD",
        "Cityname": "Đà Nẵng",
        "AreaId": 2,
        "Top": 3,
        "ListBranchDistrictRivets": [{
            "DistrictId": "DAD",
            "CityId": "DAD",
            "Districtname": "TP. Đà Nẵng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DADA",
            "CityId": "DAD",
            "Districtname": "Quận Hải Châu",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 16,
                "BranchsAdd": "197 Hùng Vương, P. Hải Châu 2, Q. Hải Châu, TP. Đà Nẵng",
                "BranchsCodePos": "CS_0000019",
                "BranchsShortAdd": "197 Hùng Vương, P. Hải Châu 2, Q. Hải Châu",
                "Districtid": "DADA",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 256,
                "BranchsAdd": "Lầu 2 Lotte Đà Nẵng, số 6 Nại Nam, P. Hòa Cường Bắc, Q. Hải Châu, TP. Đà Nẵng",
                "BranchsCodePos": "CS_0000323",
                "BranchsShortAdd": "Lầu 2 Lotte Đà Nẵng, số 6 Nại Nam, P.Hòa Cường Bắc",
                "Districtid": "DADA",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "DADB",
            "CityId": "DAD",
            "Districtname": "Quận Thanh Khê",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 159,
                "BranchsAdd": "327-329 Điện Biên Phủ, P. An Khê, Q. Thanh Khê, TP. Đà Nẵng",
                "BranchsCodePos": "CS_0000206",
                "BranchsShortAdd": "327-329 Điện Biên Phủ, P. An Khê",
                "Districtid": "DADB",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 211,
                "BranchsAdd": "170 Nguyễn Văn Linh, P. Thạc Gián, Q. Thanh Khê, Đà Nẵng",
                "BranchsCodePos": "CS_0000267",
                "BranchsShortAdd": "170 Nguyễn Văn Linh, P. Thạc Gián",
                "Districtid": "DADB",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DADC",
            "CityId": "DAD",
            "Districtname": "Quận Sơn Trà",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 253,
                "BranchsAdd": "Lô A8 đường Võ Văn Kiệt, P. An Hải Đông, Q.Sơn Trà, Đà Nẵng",
                "BranchsCodePos": "CS_0000317",
                "BranchsShortAdd": "Lô A8 đường Võ Văn Kiệt, P. An Hải Đông",
                "Districtid": "DADC",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DADD",
            "CityId": "DAD",
            "Districtname": "Quận Ngũ Hành Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DADE",
            "CityId": "DAD",
            "Districtname": "Quận Liên Chiểu",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 244,
                "BranchsAdd": "277, Tôn Đức Thắng, P. Hoà Minh, Q. Liên Chiểu, Đà Nẵng",
                "BranchsCodePos": "CS_0000310",
                "BranchsShortAdd": "277, Tôn Đức Thắng, P. Hoà Minh",
                "Districtid": "DADE",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 321,
                "BranchsAdd": "842 Tôn Ðức Thắng, P. Hòa Khánh Bắc, Q. Liên Chiểu, Ðà Nẵng",
                "BranchsCodePos": "CS_0000390",
                "BranchsShortAdd": "842 Tôn Ðức Thắng, P. Hòa Khánh Bắc",
                "Districtid": "DADE",
                "Cityid": "DAD",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DADF",
            "CityId": "DAD",
            "Districtname": "Huyện Hòa Vang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DADG",
            "CityId": "DAD",
            "Districtname": "Huyện đảo Hoàng Sa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNCL",
            "CityId": "DAD",
            "Districtname": "Huyện Cẩm Lệ",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "LXN",
        "Cityname": "An Giang",
        "AreaId": 1,
        "Top": 4,
        "ListBranchDistrictRivets": [{
            "DistrictId": "LXNA",
            "CityId": "LXN",
            "Districtname": "Thành Phố Long Xuyên",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 77,
                "BranchsAdd": "123 Trần Hưng Đạo, P. Mỹ Xuyên, TP. Long Xuyên, An Giang",
                "BranchsCodePos": "CS_0000066",
                "BranchsShortAdd": "123 Trần Hưng Đạo, P. Mỹ Xuyên, TP. Long Xuyên",
                "Districtid": "LXNA",
                "Cityid": "LXN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LXNB",
            "CityId": "LXN",
            "Districtname": "Thị Xã Châu Đốc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNC",
            "CityId": "LXN",
            "Districtname": "Huyện An Phú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXND",
            "CityId": "LXN",
            "Districtname": "Huyện Tân Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNE",
            "CityId": "LXN",
            "Districtname": "Huyện Phú Tân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNF",
            "CityId": "LXN",
            "Districtname": "Huyện Châu Phú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNG",
            "CityId": "LXN",
            "Districtname": "Huyện Tịnh Biên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNH",
            "CityId": "LXN",
            "Districtname": "Huyện Tri Tôn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNI",
            "CityId": "LXN",
            "Districtname": "Huyện Chợ Mới",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 344,
                "BranchsAdd": "Ấp Long Hòa, TT. Chợ Mới, H. Chợ Mới, An Giang",
                "BranchsCodePos": "CS_0000411",
                "BranchsShortAdd": "Ấp Long Hòa, Thị trấn Chợ Mới",
                "Districtid": "LXNI",
                "Cityid": "LXN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LXNJ",
            "CityId": "LXN",
            "Districtname": "Huyện Châu Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LXNK",
            "CityId": "LXN",
            "Districtname": "Huyện Thoại Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "VUT",
        "Cityname": "Bà Rịa - Vũng Tàu",
        "AreaId": 1,
        "Top": 5,
        "ListBranchDistrictRivets": [{
            "DistrictId": "VUT1",
            "CityId": "VUT",
            "Districtname": "Huyện Đất Đỏ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VUT2",
            "CityId": "VUT",
            "Districtname": "Huyện Long Điền",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 263,
                "BranchsAdd": "Khu Phố Hải Bình, TT. Long Hải, H. Long Điền, Bà Rịa - Vũng Tàu",
                "BranchsCodePos": "CS_0000332",
                "BranchsShortAdd": "KP Hải Bình, TT. Long Hải, H. Long Điền, Bà Rịa - Vũng Tàu",
                "Districtid": "VUT2",
                "Cityid": "VUT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VUTA",
            "CityId": "VUT",
            "Districtname": "Thành Phố Vũng Tàu",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 151,
                "BranchsAdd": "165 Lê Hồng Phong, P.8, TP. Vũng Tàu, Bà Rịa - Vũng Tàu",
                "BranchsCodePos": "CS_0000196",
                "BranchsShortAdd": "165 Lê Hồng Phong, P.8, TP. Vũng Tàu",
                "Districtid": "VUTA",
                "Cityid": "VUT",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 242,
                "BranchsAdd": "Tầng 2, Góc đường 3/2 và Thi Sách, P.8, TP. Vũng Tàu, Bà Rịa - Vũng Tàu",
                "BranchsCodePos": "CS_0000302",
                "BranchsShortAdd": "Tầng 2, Góc đường 3/2 và Thi Sách, P.8, Vũng Tàu",
                "Districtid": "VUTA",
                "Cityid": "VUT",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "VUTB",
            "CityId": "VUT",
            "Districtname": "Thành Phố Bà Rịa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 140,
                "BranchsAdd": "92 Nguyễn Hữu Thọ, P. Phước Trung, TP. Bà Rịa, Bà Rịa - Vũng Tàu",
                "BranchsCodePos": "CS_0000187",
                "BranchsShortAdd": "92 Nguyễn Hữu Thọ, P. Phước Trung, Bà Rịa",
                "Districtid": "VUTB",
                "Cityid": "VUT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VUTC",
            "CityId": "VUT",
            "Districtname": "Huyện Châu Đức",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VUTD",
            "CityId": "VUT",
            "Districtname": "Huyện Xuyên Mộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VUTE",
            "CityId": "VUT",
            "Districtname": "Huyện Tân Thành",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 239,
                "BranchsAdd": "Ngã 4, Quốc lộ 51, Long Hậu, xã Mỹ Xuân, H. Tân Thành, Bà Rịa - Vũng Tàu",
                "BranchsCodePos": "CS_0000309",
                "BranchsShortAdd": "Ngã 4, Quốc lộ 51, Long Hậu, X. Mỹ Xuân",
                "Districtid": "VUTE",
                "Cityid": "VUT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VUTG",
            "CityId": "VUT",
            "Districtname": "Huyện Côn Đảo",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "BGG",
        "Cityname": "Bắc Giang",
        "AreaId": 3,
        "Top": 6,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BGGA",
            "CityId": "BGG",
            "Districtname": "TP.  Bắc Giang",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 125,
                "BranchsAdd": "135 Hùng Vương, P. Hoàng Văn Thụ, TP. Bắc Giang",
                "BranchsCodePos": "CS_0000165",
                "BranchsShortAdd": "135 Hùng Vương, P. Hoàng Văn Thụ, Bắc Giang",
                "Districtid": "BGGA",
                "Cityid": "BGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BGGB",
            "CityId": "BGG",
            "Districtname": "Huyện Yên Thế",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 373,
                "BranchsAdd": "Phố Cả Trọng, T.T Cầu Gồ, H. Yên Thế, Bắc Giang",
                "BranchsCodePos": "CS_0000439",
                "BranchsShortAdd": "Phố Cả Trọng, T.T Cầu Gồ, H. Yên Thế, Bắc Giang",
                "Districtid": "BGGB",
                "Cityid": "BGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BGGC",
            "CityId": "BGG",
            "Districtname": "Huyện Tân Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGD",
            "CityId": "BGG",
            "Districtname": "Huyện Lục Ngạn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGE",
            "CityId": "BGG",
            "Districtname": "Huyện Hiệp Hòa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGF",
            "CityId": "BGG",
            "Districtname": "Huyện Lạng Giang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGG",
            "CityId": "BGG",
            "Districtname": "Huyện Sơn Động",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGH",
            "CityId": "BGG",
            "Districtname": "Huyện Lục Nam",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGI",
            "CityId": "BGG",
            "Districtname": "Huyện Việt Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BGGJ",
            "CityId": "BGG",
            "Districtname": "Huyện Yên Dũng",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "BKN",
        "Cityname": "Bắc Kạn",
        "AreaId": 3,
        "Top": 7,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BKNA",
            "CityId": "BKN",
            "Districtname": "Thành Phố Bắc Kạn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 245,
                "BranchsAdd": "50-52, Võ Nguyên Giáp, Tổ 1, P. Sông Cầu, TP. Bắc Kạn",
                "BranchsCodePos": "CS_0000312",
                "BranchsShortAdd": "50-52, Võ Nguyên Giáp, Tổ 1, P. Sông Cầu",
                "Districtid": "BKNA",
                "Cityid": "BKN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BKNB",
            "CityId": "BKN",
            "Districtname": "Huyện Ba Bể",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BKNC",
            "CityId": "BKN",
            "Districtname": "Huyện Ngân Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BKND",
            "CityId": "BKN",
            "Districtname": "Huyện Chợ Đồn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BKNE",
            "CityId": "BKN",
            "Districtname": "Huyện Na Rì",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BKNF",
            "CityId": "BKN",
            "Districtname": "Huyện Bạch Thông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BKNG",
            "CityId": "BKN",
            "Districtname": "H.Chợ Mới",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BKNH",
            "CityId": "BKN",
            "Districtname": "Huyện Pác Nặm",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "BLU",
        "Cityname": "Bạc Liêu",
        "AreaId": 1,
        "Top": 8,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BLU",
            "CityId": "BLU",
            "Districtname": "Huyện Hòa Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BLUA",
            "CityId": "BLU",
            "Districtname": "Thị Xã Bạc Liêu",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 327,
                "BranchsAdd": "24-26 Trần Phú, P.3, TP. Bạc Liêu",
                "BranchsCodePos": "CS_0000394",
                "BranchsShortAdd": "24-26 Trần Phú, P.3, TP. Bạc Liêu",
                "Districtid": "BLUA",
                "Cityid": "BLU",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BLUB",
            "CityId": "BLU",
            "Districtname": "Huyện Phước Long",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 301,
                "BranchsAdd": "226B-228B Đường cầu số 02, Ấp Long Thành, H. Phước Long, Bạc Liêu",
                "BranchsCodePos": "CS_0000370",
                "BranchsShortAdd": "226B-228B Đường cầu số 02, Ấp Long Thành",
                "Districtid": "BLUB",
                "Cityid": "BLU",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BLUC",
            "CityId": "BLU",
            "Districtname": "Huyện Hồng Dân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BLUD",
            "CityId": "BLU",
            "Districtname": "Huyện Vĩnh Lợi",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BLUE",
            "CityId": "BLU",
            "Districtname": "Gia Rai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BLUF",
            "CityId": "BLU",
            "Districtname": "Huyện Đông Hải",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "BNH",
        "Cityname": "Bắc Ninh",
        "AreaId": 3,
        "Top": 9,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BNHA",
            "CityId": "BNH",
            "Districtname": "Thành Phố Bắc Ninh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 146,
                "BranchsAdd": "1A Nguyễn Trãi, P. Ninh Xá, TP. Bắc Ninh",
                "BranchsCodePos": "CS_0000194",
                "BranchsShortAdd": "1A Nguyễn Trãi, P. Ninh Xá",
                "Districtid": "BNHA",
                "Cityid": "BNH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BNHB",
            "CityId": "BNH",
            "Districtname": "Huyện Yên Phong",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BNHC",
            "CityId": "BNH",
            "Districtname": "Huyện Quế Võ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BNHD",
            "CityId": "BNH",
            "Districtname": "Huyện Tiên Du",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BNHE",
            "CityId": "BNH",
            "Districtname": "Huyện Từ Sơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 201,
                "BranchsAdd": "Khu phố Xuân Thụ, P. Đông Ngàn, TX. Từ Sơn, Bắc Ninh",
                "BranchsCodePos": "CS_0000260",
                "BranchsShortAdd": "Khu phố Xuân Thụ, P. Đông Ngàn",
                "Districtid": "BNHE",
                "Cityid": "BNH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BNHF",
            "CityId": "BNH",
            "Districtname": "Huyện Thuận Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BNHG",
            "CityId": "BNH",
            "Districtname": "Huyện Lương Tài",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BNHH",
            "CityId": "BNH",
            "Districtname": "Huyện Gia Bình",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "BTE",
        "Cityname": "Bến Tre",
        "AreaId": 1,
        "Top": 10,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BTEA",
            "CityId": "BTE",
            "Districtname": "TP Bến Tre",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 96,
                "BranchsAdd": "49C Đại Lộ Đồng Khởi, P. Phú Khương, TP. Bến Tre",
                "BranchsCodePos": "CS_0000129",
                "BranchsShortAdd": "49C Đại Lộ Đồng Khởi, P. Phú Khương, Bến Tre",
                "Districtid": "BTEA",
                "Cityid": "BTE",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BTEB",
            "CityId": "BTE",
            "Districtname": "H.Châu Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTEC",
            "CityId": "BTE",
            "Districtname": "Huyện Chợ Lách",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTEE",
            "CityId": "BTE",
            "Districtname": "Huyện Giồng Trôm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTEF",
            "CityId": "BTE",
            "Districtname": "Huyện Bình Đại",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTEG",
            "CityId": "BTE",
            "Districtname": "Huyện Ba Tri",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTEH",
            "CityId": "BTE",
            "Districtname": "Huyện Thạnh Phú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTMCB",
            "CityId": "BTE",
            "Districtname": "Huyện Mõ Cày Bắc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTMCN",
            "CityId": "BTE",
            "Districtname": "Huyện Mõ Cày Nam",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BTDB",
            "CityId": "BTE",
            "Districtname": "Huyện Đại Bình",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "QNN",
        "Cityname": "Bình Định",
        "AreaId": 2,
        "Top": 11,
        "ListBranchDistrictRivets": [{
            "DistrictId": "QNNC",
            "CityId": "QNN",
            "Districtname": "Huyện Hoài Nhơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 313,
                "BranchsAdd": "30 Trần Hưng Đạo, TT. Bồng Sơn, H. Hoài Nhơn, Bình Định",
                "BranchsCodePos": "CS_0000380",
                "BranchsShortAdd": "30 Trần Hưng Đạo, TT. Bồng Sơn",
                "Districtid": "QNNC",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNND",
            "CityId": "QNN",
            "Districtname": "Huyện Hoài Ân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNNG",
            "CityId": "QNN",
            "Districtname": "Huyện Phù Cát",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNNH",
            "CityId": "QNN",
            "Districtname": "Huyện Tây Sơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 359,
                "BranchsAdd": "388-390-392 Quang Trung, TT. Phú Phong, H. Tây Sơn, Bình Ðịnh",
                "BranchsCodePos": "CS_0000425",
                "BranchsShortAdd": "388-390-392 Quang Trung, TT. Phú Phong, H. Tây Sơn, Bình Ðịnh",
                "Districtid": "QNNH",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNNI",
            "CityId": "QNN",
            "Districtname": "Huyện An Nhơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 352,
                "BranchsAdd": "162 Trần Phú, P. Bình Ðịnh, TX. An Nhơn, Bình Ðịnh",
                "BranchsCodePos": "CS_0000418",
                "BranchsShortAdd": "162 Trần Phú, P. Bình Ðịnh",
                "Districtid": "QNNI",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNNJ",
            "CityId": "QNN",
            "Districtname": "Huyện Tuy Phước",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 280,
                "BranchsAdd": "47 Trần Phú, TT. Diêu Trì, H. Tuy Phước, Bình Định",
                "BranchsCodePos": "CS_0000348",
                "BranchsShortAdd": "47 Trần Phú, TT. Diêu Trì",
                "Districtid": "QNNJ",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNNK",
            "CityId": "QNN",
            "Districtname": "Huyện Vân Cảnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNNPHU",
            "CityId": "QNN",
            "Districtname": "Huyện Phú Mỹ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNNUIH",
            "CityId": "QNN",
            "Districtname": "Thành Phố Qui Nhơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 158,
                "BranchsAdd": "New SIS Quy Nhơn - KĐT Xanh Vũng Chua, Ghềnh Ráng, Quy Nhơn, Bình Định",
                "BranchsCodePos": "CS_0000203",
                "BranchsShortAdd": "New SIS Quy Nhơn - Vũng Chua, Ghềnh Ráng",
                "Districtid": "QNNUIH",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 3
            }, {
                "BranchsId": 305,
                "BranchsAdd": "438 Trần Hưng Đạo, TP. Quy Nhơn, Bình Định",
                "BranchsCodePos": "CS_0000371",
                "BranchsShortAdd": "438 Trần Hưng Đạo, TP. Quy Nhơn",
                "Districtid": "QNNUIH",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 318,
                "BranchsAdd": "374B Nguyễn Thái Học, P. Ngô Mây, TP. Qui Nhơn, Bình Định",
                "BranchsCodePos": "CS_0000385",
                "BranchsShortAdd": "374B Nguyễn Thái Học, P. Ngô Mây",
                "Districtid": "QNNUIH",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 365,
                "BranchsAdd": "GH B7A, TTTM Quy Nhơn, 07 Lê Duẩn, P. Lý Thường Kiệt, Quy Nhơn, Bình Ðịnh",
                "BranchsCodePos": "CS_0000431",
                "BranchsShortAdd": "GH B7A, TTTM Quy Nhơn, 07 Lê Duẩn, P. Lý Thường Kiệt, Quy Nhơn, Bình Ðịnh",
                "Districtid": "QNNUIH",
                "Cityid": "QNN",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "BDAL",
            "CityId": "QNN",
            "Districtname": "Huyện  An Lão",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBVT",
            "CityId": "QNN",
            "Districtname": "Huyện  Vĩnh Thạnh",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "BDG",
        "Cityname": "Bình Dương",
        "AreaId": 1,
        "Top": 12,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BDGA",
            "CityId": "BDG",
            "Districtname": "Thành phố Thủ Dầu Một",
            "Top": 2,
            "LiBranchRivets": [{
                "BranchsId": 20,
                "BranchsAdd": "521 Đại lộ Bình Dương, P. Hiệp Thành, TP. Thủ Dầu Một, Bình Dương",
                "BranchsCodePos": "CS_0000023",
                "BranchsShortAdd": "521 Đại lộ Bình Dương, P. Hiệp Thành, TP. Thủ Dầu Một",
                "Districtid": "BDGA",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 62,
                "BranchsAdd": "19 Nguyễn Đình Chiểu, P. Phú Cường, TP. Thủ Dầu Một, Bình Dương",
                "BranchsCodePos": "CS_0000097",
                "BranchsShortAdd": "19 Nguyễn Đình Chiểu, P. Phú Cường, Thủ Dầu Một",
                "Districtid": "BDGA",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 370,
                "BranchsAdd": "316 Tạo Lực 5, P. Hòa Phú, Tp Thủ Dầu Một, Bình Dương",
                "BranchsCodePos": "CS_0000436",
                "BranchsShortAdd": "316 Tạo Lực 5, P. Hòa Phú, Tp Thủ Dầu Một, Bình Dương",
                "Districtid": "BDGA",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BDGB",
            "CityId": "BDG",
            "Districtname": "Huyện Dầu Tiếng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BDGC",
            "CityId": "BDG",
            "Districtname": "Huyện Bến Cát",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 183,
                "BranchsAdd": "Bưu điện thị xã Bến Cát, KP. 2, P. Mỹ Phước, TX. Bến Cát, Bình Dương",
                "BranchsCodePos": "CS_0000236",
                "BranchsShortAdd": "Bưu điện TX Bến Cát, KP. 2, P. Mỹ Phước",
                "Districtid": "BDGC",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 188,
                "BranchsAdd": "Lô CC1 (góc đường D1-N1), P. Mỹ Phước, TX. Bến Cát, Bình Dương",
                "BranchsCodePos": "CS_0000237",
                "BranchsShortAdd": "Lô CC1, P. Mỹ Phước",
                "Districtid": "BDGC",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 267,
                "BranchsAdd": "Đường ĐT 741, KP Phú Nghị, Hòa Lợi, Bến Cát, Bình Dương",
                "BranchsCodePos": "CS_0000335",
                "BranchsShortAdd": "Đường ĐT 741, KP Phú Nghị, Hòa Lợi, Bến Cát, Bình Dương",
                "Districtid": "BDGC",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BDGD",
            "CityId": "BDG",
            "Districtname": "Huyện Phú Giáo",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BDGE",
            "CityId": "BDG",
            "Districtname": "Huyện Tân Uyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BDGF",
            "CityId": "BDG",
            "Districtname": "Thị xã Thuận An",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 95,
                "BranchsAdd": "242/1A Đại Lộ Bình Dương, KP. Thạnh Hòa B, P. An Thạnh, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000125",
                "BranchsShortAdd": "242/1A Đại Lộ Bình Dương, KP. Thạnh Hòa B, P. An Thạnh, Thuận An",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 103,
                "BranchsAdd": "32/1 Khu Phố 1A, P. An Phú, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000137",
                "BranchsShortAdd": "32/1 Khu Phố 1A, P. An Phú, Thuận An",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 163,
                "BranchsAdd": "27/7 Đường DT743, KP. Bình Phước, P. Bình Chuẩn, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000210",
                "BranchsShortAdd": "27/7 Đường DT743, KP. Bình Phước, P. Bình Chuẩn",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 207,
                "BranchsAdd": "Số 30/2 đường DT743, KP. Bình Quới A, P. Bình Chuẩn, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000270",
                "BranchsShortAdd": "Số 30/2, đuờng DT743, KP. Bình Quới A, P. Bình Chuẩn",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 209,
                "BranchsAdd": "51 Đường DT745, KP. Đông Tư, P. Lái Thiêu, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000269",
                "BranchsShortAdd": "51 Đường DT745, KP. Đông Tư, P. Lái Thiêu",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 259,
                "BranchsAdd": "Lầu 2 Lotte Bình Dương, KĐT Seasons Bình Dương, P. Lái Thiêu, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000327",
                "BranchsShortAdd": "Lầu 2 Lotte Bình Dương, KĐT Seasons Bình Dương, P.Lái Thiêu",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 338,
                "BranchsAdd": "11/1A, Bình Ðức 3, P. Bình Hòa, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000405",
                "BranchsShortAdd": "11/1A, Bình Ðức 3, P. Bình Hòa",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 380,
                "BranchsAdd": "Ô 1 Lô DC17, KDC Việt Sing, P. An Phú, TX. Thuận An, Bình Dương",
                "BranchsCodePos": "CS_0000446",
                "BranchsShortAdd": "Ô 1 Lô DC17, KDC Việt Sing, P. An Phú, TX. Thuận An, Bình Dương",
                "Districtid": "BDGF",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BDGG",
            "CityId": "BDG",
            "Districtname": "Huyện Dĩ An",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 104,
                "BranchsAdd": "1/30 Nguyễn An Ninh, KP. Bình Minh 2, TX. Dĩ An, Bình Dương",
                "BranchsCodePos": "CS_0000135",
                "BranchsShortAdd": "1/30 Nguyễn An Ninh, KP. Bình Minh 2, TX. Dĩ An",
                "Districtid": "BDGG",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 131,
                "BranchsAdd": "1/31 Đại lộ Độc Lập, KP. Nhị Đồng, TX. Dĩ An, Bình Dương",
                "BranchsCodePos": "CS_0000174",
                "BranchsShortAdd": "1/31 Đại lộ Độc Lập, KP. Nhị Đồng, TX. Dĩ An",
                "Districtid": "BDGG",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 384,
                "BranchsAdd": "521 Đường Dĩ An - Bình Đường 4, P.An Bình, Dĩ An, Bình Dương",
                "BranchsCodePos": "CS_0000449",
                "BranchsShortAdd": "521 Đường Dĩ An - Bình Đường 4, P.An Bình, Dĩ An, Bình Dương",
                "Districtid": "BDGG",
                "Cityid": "BDG",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "BPC",
        "Cityname": "Bình Phước",
        "AreaId": 1,
        "Top": 13,
        "ListBranchDistrictRivets": [{
            "DistrictId": "BPCA",
            "CityId": "BPC",
            "Districtname": "Thị Xã Đồng Xoài",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 75,
                "BranchsAdd": "906 Phú Riềng Đỏ, P. Tân Thiện, TX. Đồng Xoài, Bình Phước",
                "BranchsCodePos": "CS_0000107",
                "BranchsShortAdd": "906 Phú Riềng Đỏ, P. Tân Thiện, TX. Đồng Xoài",
                "Districtid": "BPCA",
                "Cityid": "BPC",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "BPCB",
            "CityId": "BPC",
            "Districtname": "Đồng Phú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPCD",
            "CityId": "BPC",
            "Districtname": "Lộc Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPCE",
            "CityId": "BPC",
            "Districtname": "Bù Đăng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPCF",
            "CityId": "BPC",
            "Districtname": "Bình Long",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPCG",
            "CityId": "BPC",
            "Districtname": "Bù Đốp",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPCH",
            "CityId": "BPC",
            "Districtname": "Chơn Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPCPHG",
            "CityId": "BPC",
            "Districtname": "Phước Long",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PHUR",
            "CityId": "BPC",
            "Districtname": "Phú Riềng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPGM",
            "CityId": "BPC",
            "Districtname": "Huyện Bù Gia Mập",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "BPHQ",
            "CityId": "BPC",
            "Districtname": "Huyện Hớn Quản",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "PTT",
        "Cityname": "Bình Thuận",
        "AreaId": 1,
        "Top": 14,
        "ListBranchDistrictRivets": [{
            "DistrictId": "PTT",
            "CityId": "PTT",
            "Districtname": "Thị Xã Lagi",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 240,
                "BranchsAdd": "Số 75 Thống Nhất, P. Phước Hội, TX. La Gi, Bình Thuận",
                "BranchsCodePos": "CS_0000311",
                "BranchsShortAdd": "Số 75 Thống Nhất, P. Phước Hội",
                "Districtid": "PTT",
                "Cityid": "PTT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "PTTB",
            "CityId": "PTT",
            "Districtname": "Tuy Phong",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTC",
            "CityId": "PTT",
            "Districtname": "Bắc Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTD",
            "CityId": "PTT",
            "Districtname": "Hàm Thuận Bắc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTE",
            "CityId": "PTT",
            "Districtname": "Hàm Thuận Nam",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTF",
            "CityId": "PTT",
            "Districtname": "Tánh Linh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTG",
            "CityId": "PTT",
            "Districtname": "Hàm Tân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTH",
            "CityId": "PTT",
            "Districtname": "Đức Linh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTI",
            "CityId": "PTT",
            "Districtname": "Phú Quý",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTTPHH",
            "CityId": "PTT",
            "Districtname": "Thành Phố Phan Thiết",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 133,
                "BranchsAdd": "80-82 Trưng Trắc, P. Đức Thắng, TP. Phan Thiết, Bình Thuận",
                "BranchsCodePos": "CS_0000178",
                "BranchsShortAdd": "80-82 Trưng Trắc, P. Đức Thắng, TP. Phan Thiết",
                "Districtid": "PTTPHH",
                "Cityid": "PTT",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 243,
                "BranchsAdd": "Lotte Phan Thiết - Tầng 2, Khu dân cư Hùng Vương 1, P. Phú Thuỷ, TP. Phan Thiết, Bình Thuận",
                "BranchsCodePos": "CS_0000303",
                "BranchsShortAdd": "Lotte Phan Thiết - Tầng 2, Khu dân cư Hùng Vương 1, P. Phú Thuỷ",
                "Districtid": "PTTPHH",
                "Cityid": "PTT",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 269,
                "BranchsAdd": "171 Lê Hồng Phong, P. Phú Trinh, TP. Phân Thiết, Bình Thuận",
                "BranchsCodePos": "CS_0000336",
                "BranchsShortAdd": "171 Lê Hồng Phong, P. Phú Trinh",
                "Districtid": "PTTPHH",
                "Cityid": "PTT",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 312,
                "BranchsAdd": "58 Trần Hưng Đạo, P. Phú Trinh, TP. Phan Thiết, Bình Thuận ",
                "BranchsCodePos": "CS_0000379",
                "BranchsShortAdd": "58 Trần Hưng Đạo, P. Phú Trinh",
                "Districtid": "PTTPHH",
                "Cityid": "PTT",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "CMU",
        "Cityname": "Cà Mau",
        "AreaId": 1,
        "Top": 15,
        "ListBranchDistrictRivets": [{
            "DistrictId": "CMUA",
            "CityId": "CMU",
            "Districtname": "TP. Cà Mau",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 231,
                "BranchsAdd": "132C Nguyễn Tất Thành, Khóm 7, P.8, TP. Cà Mau ",
                "BranchsCodePos": "CS_0000294",
                "BranchsShortAdd": "132C Nguyễn Tất Thành, Khóm 7, P.8",
                "Districtid": "CMUA",
                "Cityid": "CMU",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "CMUB",
            "CityId": "CMU",
            "Districtname": "Thới Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUC",
            "CityId": "CMU",
            "Districtname": "U Minh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUD",
            "CityId": "CMU",
            "Districtname": "Trần Văn Thời",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUE",
            "CityId": "CMU",
            "Districtname": "Cái Nước",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUF",
            "CityId": "CMU",
            "Districtname": "Đầm Dơi",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUG",
            "CityId": "CMU",
            "Districtname": "Ngọc Hiển",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUH",
            "CityId": "CMU",
            "Districtname": "Năm Căn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMUI",
            "CityId": "CMU",
            "Districtname": "Phú Tân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CMP5",
            "CityId": "CMU",
            "Districtname": "Phường 5",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 255,
                "BranchsAdd": "12 Trần Hưng Đạo, P.5, TP. Cà Mau",
                "BranchsCodePos": "CS_0000322",
                "BranchsShortAdd": "12 Trần Hưng Đạo, P.5",
                "Districtid": "CMP5",
                "Cityid": "CMU",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "VCA",
        "Cityname": "Cần Thơ",
        "AreaId": 1,
        "Top": 16,
        "ListBranchDistrictRivets": [{
            "DistrictId": "VCAA",
            "CityId": "VCA",
            "Districtname": "Ninh Kiều",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 154,
                "BranchsAdd": "61 Đường 30/4, P. Ninh Kiều, Q. Ninh Kiều, TP. Cần Thơ",
                "BranchsCodePos": "CS_0000200",
                "BranchsShortAdd": "61 Đường 30/4, P. Ninh Kiều",
                "Districtid": "VCAA",
                "Cityid": "VCA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 226,
                "BranchsAdd": "Lotte Cần Thơ (Lầu 1) - 84 Mậu Thân, P. An Hòa, Q. Ninh Kiều, TP. Cần Thơ",
                "BranchsCodePos": "CS_0000293",
                "BranchsShortAdd": "Lotte Cần Thơ (Lầu 1) - 84 Mậu Thân, P. An Hòa",
                "Districtid": "VCAA",
                "Cityid": "VCA",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 364,
                "BranchsAdd": "GH T-28, TTTM Sensen City, Số 1 ÐL Hòa Bình, Ninh Kiều, Cần Thơ",
                "BranchsCodePos": "CS_0000430",
                "BranchsShortAdd": "GH T-28, TTTM Sensen City, Số 1 ÐL Hòa Bình, Ninh Kiều, Cần Thơ",
                "Districtid": "VCAA",
                "Cityid": "VCA",
                "Status": "A",
                "GroupId": 4
            }]
        }, {
            "DistrictId": "VCAB",
            "CityId": "VCA",
            "Districtname": "Bình Thủy",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 271,
                "BranchsAdd": "120 Cách mạng tháng 8, P. Bùi Hữu Nghĩa, Q. Bình Thủy, Cần Thơ",
                "BranchsCodePos": "CS_0000338",
                "BranchsShortAdd": "120 Cách mạng tháng 8, P. Bùi Hữu Nghĩa",
                "Districtid": "VCAB",
                "Cityid": "VCA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VCAC",
            "CityId": "VCA",
            "Districtname": "Cái Răng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VCAE",
            "CityId": "VCA",
            "Districtname": "Ô Môn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VCAF",
            "CityId": "VCA",
            "Districtname": "Thốt Nốt",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VCAG",
            "CityId": "VCA",
            "Districtname": "Cờ Đỏ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VCAH",
            "CityId": "VCA",
            "Districtname": "Vĩnh Thạnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VCAI",
            "CityId": "VCA",
            "Districtname": "Phong Điền",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CTTL",
            "CityId": "VCA",
            "Districtname": "Huyện Thới Lai",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DLK",
        "Cityname": "Đắk Lắk",
        "AreaId": 2,
        "Top": 17,
        "ListBranchDistrictRivets": [{
            "DistrictId": "DLKA",
            "CityId": "DLK",
            "Districtname": "TP. Buôn Mê Thuột",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 29,
                "BranchsAdd": "06 Ngô Quyền, P. Thắng Lợi, TP. Buôn Mê Thuột, Đắk Lắk",
                "BranchsCodePos": "CS_0000032",
                "BranchsShortAdd": "06 Ngô Quyền, P. Thắng Lợi, TP. Buôn Mê Thuột",
                "Districtid": "DLKA",
                "Cityid": "DLK",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 195,
                "BranchsAdd": "59 Lê Thánh Tông, P. Thắng Lợi, TP. Buôn Mê Thuột, Đắk Lắk",
                "BranchsCodePos": "CS_0000248",
                "BranchsShortAdd": "59 Lê Thánh Tông, P. Thắng Lợi",
                "Districtid": "DLKA",
                "Cityid": "DLK",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 326,
                "BranchsAdd": "398 - 400 Lê Duẩn, P. Ea Tam, TP.Buôn Ma Thuột, Đắk Lắk",
                "BranchsCodePos": "CS_0000391",
                "BranchsShortAdd": "398 - 400 Lê Duẩn, P. Ea Tam",
                "Districtid": "DLKA",
                "Cityid": "DLK",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DLKB",
            "CityId": "DLK",
            "Districtname": "Ea H\u0027leo",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKC",
            "CityId": "DLK",
            "Districtname": "Ea Súp",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKD",
            "CityId": "DLK",
            "Districtname": "Krông Năng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKE",
            "CityId": "DLK",
            "Districtname": "Knông Buk",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKF",
            "CityId": "DLK",
            "Districtname": "Buôn Đôn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKG",
            "CityId": "DLK",
            "Districtname": "Cư M\u0027gar",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKH",
            "CityId": "DLK",
            "Districtname": "Ea Kar",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKI",
            "CityId": "DLK",
            "Districtname": "M\u0027Đrắc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKJ",
            "CityId": "DLK",
            "Districtname": "Krông Pắc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKL",
            "CityId": "DLK",
            "Districtname": "Krông Ana",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKM",
            "CityId": "DLK",
            "Districtname": "Krông Bông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLKP",
            "CityId": "DLK",
            "Districtname": "Lắk",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTL",
            "CityId": "DLK",
            "Districtname": "Phường Thắng Lợi",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLBH",
            "CityId": "DLK",
            "Districtname": "Thị xã Buôn Hồ",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 309,
                "BranchsAdd": "Số 452 – 452A Hùng Vương, P. An Bình, TX. Buôn Hồ, Đắk Lắk",
                "BranchsCodePos": "CS_0000378",
                "BranchsShortAdd": "Số 452 – 452A Hùng Vương, P. An Bình",
                "Districtid": "DLBH",
                "Cityid": "DLK",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DLCK",
            "CityId": "DLK",
            "Districtname": "Huyện Cư Kuin",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DNO",
        "Cityname": "Đắk Nông",
        "AreaId": 2,
        "Top": 18,
        "ListBranchDistrictRivets": [{
            "DistrictId": "DNO1",
            "CityId": "DNO",
            "Districtname": "Thị Xã Gia Nghĩa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 317,
                "BranchsAdd": "Coopmart Đắk Nông, đường Huỳnh Thúc Kháng, P. Nghĩa Thành, TX. Gia Nghĩa, Đắk Nông",
                "BranchsCodePos": "CS_0000387",
                "BranchsShortAdd": "Coopmart Đắk Nông, D. Huỳnh Thúc Kháng, P. Nghĩa Thành",
                "Districtid": "DNO1",
                "Cityid": "DNO",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 377,
                "BranchsAdd": "31 Huỳnh Thúc Kháng, P. Nghĩa Thành, T.X Gia Nghĩa, T. Ðắk Nông",
                "BranchsCodePos": "CS_0000443",
                "BranchsShortAdd": "31 Huỳnh Thúc Kháng, P. Nghĩa Thành, T.X Gia Nghĩa, T. Ðắk Nông",
                "Districtid": "DNO1",
                "Cityid": "DNO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNO2",
            "CityId": "DNO",
            "Districtname": "Đắk Song",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNO3",
            "CityId": "DNO",
            "Districtname": "Đắk Mil",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNO4",
            "CityId": "DNO",
            "Districtname": "Kiến Đức",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNO5",
            "CityId": "DNO",
            "Districtname": "Đắk Som",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNO6",
            "CityId": "DNO",
            "Districtname": "Đắk Glong",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNO7",
            "CityId": "DNO",
            "Districtname": "Cư Jút",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DBN",
        "Cityname": "Điện Biên",
        "AreaId": 3,
        "Top": 19,
        "ListBranchDistrictRivets": [{
            "DistrictId": "DBN",
            "CityId": "DBN",
            "Districtname": "Mường Chà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBNA",
            "CityId": "DBN",
            "Districtname": "TP. Điện Biên Phủ",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 351,
                "BranchsAdd": "Số 35, Võ Nguyên Giáp, Phường Nam Thanh, TP. Ðiện Biên Phủ, Điện Biên",
                "BranchsCodePos": "CS_0000417",
                "BranchsShortAdd": "Số 35, Võ Nguyên Giáp, P. Nam Thanh",
                "Districtid": "DBNA",
                "Cityid": "DBN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DBNB",
            "CityId": "DBN",
            "Districtname": "Lai châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBNC",
            "CityId": "DBN",
            "Districtname": "Thị xã Mường Lay",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBND",
            "CityId": "DBN",
            "Districtname": "Mường Nhé",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBNE",
            "CityId": "DBN",
            "Districtname": "Tủa Chùa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBNF",
            "CityId": "DBN",
            "Districtname": "Tuần Giáo",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBNG",
            "CityId": "DBN",
            "Districtname": "Điện Biên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DBNH",
            "CityId": "DBN",
            "Districtname": "Điện Biên Đông",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DTH",
        "Cityname": "Đồng Tháp",
        "AreaId": 1,
        "Top": 20,
        "ListBranchDistrictRivets": [{
            "DistrictId": "DTHA",
            "CityId": "DTH",
            "Districtname": "Thành Phố Cao Lãnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHC",
            "CityId": "DTH",
            "Districtname": "Tân Hồng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHD",
            "CityId": "DTH",
            "Districtname": "Hồng Ngự",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 160,
                "BranchsAdd": "Thửa đất số 195, Hùng Vương, P. An Thạnh, TX Hồng Ngự, Đồng Tháp",
                "BranchsCodePos": "CS_0000208",
                "BranchsShortAdd": "Hùng Vương,P. An Thạnh, Đồng Tháp",
                "Districtid": "DTHD",
                "Cityid": "DTH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DTHE",
            "CityId": "DTH",
            "Districtname": "Tam Nông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHF",
            "CityId": "DTH",
            "Districtname": "Thanh Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHG",
            "CityId": "DTH",
            "Districtname": "Tháp Mười",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHH",
            "CityId": "DTH",
            "Districtname": "Cao Lãnh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 152,
                "BranchsAdd": "228 - 230 Nguyễn Huệ, P.2, TP. Cao Lãnh, Đồng Tháp",
                "BranchsCodePos": "CS_0000197",
                "BranchsShortAdd": "228 - 230 Nguyễn Huệ, P.2",
                "Districtid": "DTHH",
                "Cityid": "DTH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DTHI",
            "CityId": "DTH",
            "Districtname": "Lấp Vò",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHJ",
            "CityId": "DTH",
            "Districtname": "Lai Vung",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHK",
            "CityId": "DTH",
            "Districtname": "Châu Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DTHSAD",
            "CityId": "DTH",
            "Districtname": "Thị Xã Sa Đéc",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DNA",
        "Cityname": "Đồng Nai",
        "AreaId": 1,
        "Top": 21,
        "ListBranchDistrictRivets": [{
            "DistrictId": "ANBH",
            "CityId": "DNA",
            "Districtname": "Huyện An Bình",
            "Top": 1,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNAB",
            "CityId": "DNA",
            "Districtname": "H.Tân Phú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNABHA",
            "CityId": "DNA",
            "Districtname": "TP. Biên Hoà",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 17,
                "BranchsAdd": "246 Phạm Văn Thuận, P. Thống Nhất, TP. Biên Hoà, Đồng Nai",
                "BranchsCodePos": "CS_0000020",
                "BranchsShortAdd": "246 Phạm Văn Thuận, P. Thống Nhất, TP. Biên Hoà",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 30,
                "BranchsAdd": "1A Đồng khởi, P. Trảng Dài, TP. Biên Hoà, Đồng Nai",
                "BranchsCodePos": "CS_0000033",
                "BranchsShortAdd": "1A Đồng khởi, P. Trảng Dài, TP. Biên Hoà",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 105,
                "BranchsAdd": "32-32A KP4, P. Long Bình, TP. Biên Hoà, Đồng Nai",
                "BranchsCodePos": "CS_0000132",
                "BranchsShortAdd": "32-32A KP4, P. Long Bình, TP. Biên Hoà",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 274,
                "BranchsAdd": "85 Phan Chu Trinh, P. Hòa Bình, TP. Biên Hòa, Đồng Nai",
                "BranchsCodePos": "CS_0000340",
                "BranchsShortAdd": "85 Phan Chu Trinh, P. Hòa Bình",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 258,
                "BranchsAdd": "Lầu 2 Lotte Đồng Nai, KTM Amata, QL.1A, P. Long Bình, TP. Biên Hòa, Đồng Nai",
                "BranchsCodePos": "CS_0000326",
                "BranchsShortAdd": "Lầu 2 Lotte Đồng Nai, KTM Amata, QL.1A, P.Long Bình",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 265,
                "BranchsAdd": "Vòng xoay 11, Ấp Vườn Dừa, Xã Phước Tân, TP. Biên Hòa, Đồng Nai",
                "BranchsCodePos": "CS_0000333",
                "BranchsShortAdd": "Vòng xoay 11, Ấp Vườn Dừa, Xã Phước Tân",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 289,
                "BranchsAdd": "15 Phùng Hưng, Xã Tam Phước, TP. Biên Hòa, Đồng Nai",
                "BranchsCodePos": "CS_0000360",
                "BranchsShortAdd": "15 Phùng Hưng, Xã Tam Phước",
                "Districtid": "DNABHA",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAC",
            "CityId": "DNA",
            "Districtname": "Định Quán",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 328,
                "BranchsAdd": "22 Ấp 114, TT. Ðịnh Quán, Ðồng Nai",
                "BranchsCodePos": "CS_0000395",
                "BranchsShortAdd": "Số 22, Ấp 114, T.T Ðịnh Quán",
                "Districtid": "DNAC",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAD",
            "CityId": "DNA",
            "Districtname": "Vĩnh Cừ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNAE",
            "CityId": "DNA",
            "Districtname": "Thống Nhất",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DNAF",
            "CityId": "DNA",
            "Districtname": "Thị Xã Long Khánh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 205,
                "BranchsAdd": "Đường Hùng Vương, P. Xuân An, TX. Long Khánh, Đồng Nai ",
                "BranchsCodePos": "CS_0000265",
                "BranchsShortAdd": "Đường Hùng Vương, P. Xuân An",
                "Districtid": "DNAF",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAG",
            "CityId": "DNA",
            "Districtname": "Xuân Lộc",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 276,
                "BranchsAdd": "2558, Ấp Tân Tiến, xã Xuân Hiệp, H. Xuân Lộc, Đồng Nai",
                "BranchsCodePos": "CS_0000347",
                "BranchsShortAdd": "2558, Ấp Tân Tiến, xã Xuân Hiệp",
                "Districtid": "DNAG",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAH",
            "CityId": "DNA",
            "Districtname": "Long Thành",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 182,
                "BranchsAdd": "Góc đường Lê Duẩn - Hai Bà Trưng, Khu Phước Hải, TT. Long Thành, Đồng Nai",
                "BranchsCodePos": "CS_0000235",
                "BranchsShortAdd": "Góc Lê Duẩn - Hai Bà Trưng, Khu Phước Hải",
                "Districtid": "DNAH",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAI",
            "CityId": "DNA",
            "Districtname": "Nhơn Trạch",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 122,
                "BranchsAdd": "Tổ 18, ấp 3, xã Hiệp Phước, H. Nhơn Trạch, Đồng Nai",
                "BranchsCodePos": "CS_0000156",
                "BranchsShortAdd": "Tổ 18, ấp 3, xã Hiệp Phước, H. Nhơn Trạch",
                "Districtid": "DNAI",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAJ",
            "CityId": "DNA",
            "Districtname": "Trảng Bom",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 329,
                "BranchsAdd": "232 Góc Đường 30/4 và 3/2, Khu phố 5, T.T Trảng Bom, Ðồng Nai",
                "BranchsCodePos": "CS_0000396",
                "BranchsShortAdd": "232 Góc Đường 30/4 và 3/2, Khu phố 5, T.T Trảng Bom",
                "Districtid": "DNAJ",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 376,
                "BranchsAdd": "1435 QL 1A, ấp Trà Cổ, Bình Minh, Trảng Bom, Ðồng Nai",
                "BranchsCodePos": "CS_0000442",
                "BranchsShortAdd": "1435 QL 1A, ấp Trà Cổ, Bình Minh, Trảng Bom, Ðồng Nai",
                "Districtid": "DNAJ",
                "Cityid": "DNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DNAK",
            "CityId": "DNA",
            "Districtname": "Cẩm Mỹ",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "GLI",
        "Cityname": "Gia Lai",
        "AreaId": 2,
        "Top": 22,
        "ListBranchDistrictRivets": [{
            "DistrictId": "GLIA",
            "CityId": "GLI",
            "Districtname": "Thành phố Pleiku",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 176,
                "BranchsAdd": "94-96-98 Trần Phú, P. Diên Hồng, TP. Pleiku, Gia Lai",
                "BranchsCodePos": "CS_0000225",
                "BranchsShortAdd": "94-96-98 Trần Phú, P. Diên Hồng",
                "Districtid": "GLIA",
                "Cityid": "GLI",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 229,
                "BranchsAdd": "01 Lê Duẩn, P. Phù Đổng, TP. Pleiku, Gia lai",
                "BranchsCodePos": "CS_0000283",
                "BranchsShortAdd": "01 Lê Duẩn, P. Phù Đổng",
                "Districtid": "GLIA",
                "Cityid": "GLI",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "GLIB",
            "CityId": "GLI",
            "Districtname": "KBang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIC",
            "CityId": "GLI",
            "Districtname": "Đắk Đoa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLID",
            "CityId": "GLI",
            "Districtname": "Mang Yang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIE",
            "CityId": "GLI",
            "Districtname": "Chư Păh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIF",
            "CityId": "GLI",
            "Districtname": "La Grai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIG",
            "CityId": "GLI",
            "Districtname": "Thị Xã An Khê",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 295,
                "BranchsAdd": "348 Quang Trung, P. An Phú, TX. An Khê, Gia Lai",
                "BranchsCodePos": "CS_0000362",
                "BranchsShortAdd": "348 Quang Trung, P. An Phú",
                "Districtid": "GLIG",
                "Cityid": "GLI",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "GLIH",
            "CityId": "GLI",
            "Districtname": "Kông Chro",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLII",
            "CityId": "GLI",
            "Districtname": "Đức Cơ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIJ",
            "CityId": "GLI",
            "Districtname": "Chư Prông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIK",
            "CityId": "GLI",
            "Districtname": "Chư Sê",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 268,
                "BranchsAdd": "Số 904B đường Hùng Vương, TT. Chư Sê, Gia Lai ",
                "BranchsCodePos": "CS_0000334",
                "BranchsShortAdd": "Số 904B đường Hùng Vương, TT. Chư Sê",
                "Districtid": "GLIK",
                "Cityid": "GLI",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "GLIL",
            "CityId": "GLI",
            "Districtname": "Ayun Pa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIM",
            "CityId": "GLI",
            "Districtname": "Krông Pa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIN",
            "CityId": "GLI",
            "Districtname": "Ia Pa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "GLIO",
            "CityId": "GLI",
            "Districtname": "Đắk Pơ",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "HPH",
        "Cityname": "Hải Phòng",
        "AreaId": 3,
        "Top": 23,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HPHA",
            "CityId": "HPH",
            "Districtname": "Hồng Bàng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HPHB",
            "CityId": "HPH",
            "Districtname": "Ngô Quyền",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 38,
                "BranchsAdd": "Big C - Lô 1/20 Khu Đô Thị Ngã 5, Sân Bay Cát Bi, Q. Ngô Quyền, TP. Hải Phòng",
                "BranchsCodePos": "CS_0000041",
                "BranchsShortAdd": "Big C - Lô 1/20 Khu Đô Thị Ngã 5, Sân Bay Cát Bi",
                "Districtid": "HPHB",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 2
            }, {
                "BranchsId": 136,
                "BranchsAdd": "112 - 114 Lạch Tray, P. Lạch Tray, Q. Ngô Quyền, TP. Hải Phòng",
                "BranchsCodePos": "CS_0000182",
                "BranchsShortAdd": "112 - 114 Lạch Tray, P. Lạch Tray, Q. Ngô Quyền",
                "Districtid": "HPHB",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 230,
                "BranchsAdd": "BigC Hải Phòng (Tầng trệt) - Lô 1/20 KĐT Ngã Năm, Q. Ngô Quyền, TP. Hải Phòng",
                "BranchsCodePos": "CS_0000296",
                "BranchsShortAdd": "BigC Hải Phòng (Tầng trệt) - Lô 1/20 KĐT Ngã Năm, Lê Hồng Phong",
                "Districtid": "HPHB",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 4
            }, {
                "BranchsId": 266,
                "BranchsAdd": "Số 2, Phạm Minh Đức, P. Máy Tơ, Q. Ngô Quyền, TP. Hải Phòng",
                "BranchsCodePos": "CS_0000320",
                "BranchsShortAdd": "Số 2, Phạm Minh Đức, P. Máy Tơ",
                "Districtid": "HPHB",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 297,
                "BranchsAdd": "280B Đà Nẵng, P. Vạn Mỹ, Q. Ngô Quyền, Hải Phòng",
                "BranchsCodePos": "CS_0000366",
                "BranchsShortAdd": "280B Đà Nẵng, P. Vạn Mỹ",
                "Districtid": "HPHB",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HPHC",
            "CityId": "HPH",
            "Districtname": "Lê Chân",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 246,
                "BranchsAdd": "266A Trần Nguyên Hãn, Q. Lê Chân, TP. Hải Phòng",
                "BranchsCodePos": "CS_0000313",
                "BranchsShortAdd": "266A Trần Nguyên Hãn, Q. Lê Chân",
                "Districtid": "HPHC",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HPHD",
            "CityId": "HPH",
            "Districtname": "Kiến An",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 381,
                "BranchsAdd": "Số 48 ,Trần Thành Ngọ, P.Trần Thành Ngọ, Q. Kiến An, TP. Hải Phòng",
                "BranchsCodePos": "CS_0000447",
                "BranchsShortAdd": "Số 48, Trần Thành Ngọ, Kiến An, Hải Phòng",
                "Districtid": "HPHD",
                "Cityid": "HPH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HPHF",
            "CityId": "HPH",
            "Districtname": "Huyện Thủy Nguyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HPHH",
            "CityId": "HPH",
            "Districtname": "Huyện An Lão",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HPHI",
            "CityId": "HPH",
            "Districtname": "Huyện Kiến Thụy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HPHK",
            "CityId": "HPH",
            "Districtname": "Huyện Vĩnh Bảo",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HPHL",
            "CityId": "HPH",
            "Districtname": "Huyện Cát Hải",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "HGG",
        "Cityname": "Hà Giang",
        "AreaId": 3,
        "Top": 24,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HGG",
            "CityId": "HGG",
            "Districtname": "Quang Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGA",
            "CityId": "HGG",
            "Districtname": "TP Hà Giang",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 198,
                "BranchsAdd": "118 Trần Hưng Đạo, P. Trần Phú, TP. Hà Giang",
                "BranchsCodePos": "CS_0000257",
                "BranchsShortAdd": "118 Trần Hưng Đạo, P. Trần Phú",
                "Districtid": "HGGA",
                "Cityid": "HGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HGGB",
            "CityId": "HGG",
            "Districtname": "Đồng Văn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGC",
            "CityId": "HGG",
            "Districtname": "Mào Vạc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGD",
            "CityId": "HGG",
            "Districtname": "Yên Minh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGE",
            "CityId": "HGG",
            "Districtname": "Quản Bạ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGF",
            "CityId": "HGG",
            "Districtname": "Bắc Mê",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGG",
            "CityId": "HGG",
            "Districtname": "Hoàng Su Phì",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGH",
            "CityId": "HGG",
            "Districtname": "Vị Xuyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGI",
            "CityId": "HGG",
            "Districtname": "Xín Mần",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HGGJ",
            "CityId": "HGG",
            "Districtname": "Bắc Quang",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 314,
                "BranchsAdd": "560 Trường Chinh, Tổ 4, H. Bắc Quang, Hà Giang",
                "BranchsCodePos": "CS_0000383",
                "BranchsShortAdd": "560 Trường Chinh, Tổ 4",
                "Districtid": "HGGJ",
                "Cityid": "HGG",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "HNM",
        "Cityname": "Hà Nam",
        "AreaId": 3,
        "Top": 25,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TPL",
            "CityId": "HNM",
            "Districtname": "Tp. Phủ Lý",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 171,
                "BranchsAdd": "Quốc Lộ 1A (Đường Lê Hoàn), P. Hai Bà Trưng, TP. Phủ Lý, Hà Nam",
                "BranchsCodePos": "CS_0000222",
                "BranchsShortAdd": "Quốc Lộ 1A (Đường Lê Hoàn), P. Hai Bà Trưng",
                "Districtid": "TPL",
                "Cityid": "HNM",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 275,
                "BranchsAdd": "48 Biên Hòa, Tổ 4, P. Minh Khai, TP. Phủ Lý, Hà Nam",
                "BranchsCodePos": "CS_0000342",
                "BranchsShortAdd": "48 Biên Hòa, Tổ 4, P. Minh Khai",
                "Districtid": "TPL",
                "Cityid": "HNM",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 355,
                "BranchsAdd": "136 Ðinh Tiên Hoàng, P. Thanh Châu, TP. Phủ Lý, Hà Nam",
                "BranchsCodePos": "CS_0000421",
                "BranchsShortAdd": "136 Ðinh Tiên Hoàng, P. Thanh Châu, TP. Phủ Lý, Hà Nam",
                "Districtid": "TPL",
                "Cityid": "HNM",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HNA",
            "CityId": "HNM",
            "Districtname": "Huyện Duy Tiên",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 307,
                "BranchsAdd": "158 Nguyễn Hữu Tiến, TT. Đồng Văn, H. Duy Tiên, Hà Nam",
                "BranchsCodePos": "CS_0000376",
                "BranchsShortAdd": "158 Nguyễn Hữu Tiến, TT. Đồng Văn",
                "Districtid": "HNA",
                "Cityid": "HNM",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "HAT",
        "Cityname": "Hà Tĩnh",
        "AreaId": 2,
        "Top": 26,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HATB",
            "CityId": "HAT",
            "Districtname": "Thị Xã Hồng Lĩnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATC",
            "CityId": "HAT",
            "Districtname": "Nghi Xuân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATD",
            "CityId": "HAT",
            "Districtname": "Đức Thọ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATE",
            "CityId": "HAT",
            "Districtname": "Hương Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATF",
            "CityId": "HAT",
            "Districtname": "Vũ Quang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATG",
            "CityId": "HAT",
            "Districtname": "Can Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATH",
            "CityId": "HAT",
            "Districtname": "Thạch Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATI",
            "CityId": "HAT",
            "Districtname": "Cẩm Xuyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATJ",
            "CityId": "HAT",
            "Districtname": "Hương Khê",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATK",
            "CityId": "HAT",
            "Districtname": "Kỳ Anh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HATT",
            "CityId": "HAT",
            "Districtname": "TP Hà Tĩnh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 236,
                "BranchsAdd": "74 Trần Phú, P. Bắc Hà, TP. Hà Tĩnh",
                "BranchsCodePos": "CS_0000280",
                "BranchsShortAdd": "74 Trần Phú, P. Bắc Hà",
                "Districtid": "HATT",
                "Cityid": "HAT",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "HDG",
        "Cityname": "Hải Dương",
        "AreaId": 3,
        "Top": 27,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HDGA",
            "CityId": "HDG",
            "Districtname": "TP. Hải Dương",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 120,
                "BranchsAdd": "412 Trần Hưng Ðạo, P. Ngọc Châu, TP. Hải Dương",
                "BranchsCodePos": "CS_0000153",
                "BranchsShortAdd": "412 Trần Hưng Ðạo, P. Ngọc Châu, TP. Hải Dương",
                "Districtid": "HDGA",
                "Cityid": "HDG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HDGB",
            "CityId": "HDG",
            "Districtname": "Chí Linh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 189,
                "BranchsAdd": "Số 2 Nguyễn Thái Học, P. Sao Đỏ, TX. Chí Linh, Hải Dương ",
                "BranchsCodePos": "CS_0000243",
                "BranchsShortAdd": "Số 2 Nguyễn Thái Học, P. Sao Đỏ",
                "Districtid": "HDGB",
                "Cityid": "HDG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HDGC",
            "CityId": "HDG",
            "Districtname": "Nam Sách",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGD",
            "CityId": "HDG",
            "Districtname": "Thanh Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGE",
            "CityId": "HDG",
            "Districtname": "Kinh Môn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGF",
            "CityId": "HDG",
            "Districtname": "Kim Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGG",
            "CityId": "HDG",
            "Districtname": "Gia Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGH",
            "CityId": "HDG",
            "Districtname": "Tứ Kỳ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGI",
            "CityId": "HDG",
            "Districtname": "Cẩm Giàng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGJ",
            "CityId": "HDG",
            "Districtname": "Bình Giang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGK",
            "CityId": "HDG",
            "Districtname": "Thanh Miện",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HDGL",
            "CityId": "HDG",
            "Districtname": "Ninh Giang",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "HBH",
        "Cityname": "Hoà Bình",
        "AreaId": 3,
        "Top": 28,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HBHA",
            "CityId": "HBH",
            "Districtname": "Thành Phố Hòa Bình",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 222,
                "BranchsAdd": "720 Tổ 3, P. Đồng Tiến, TP. Hòa Bình",
                "BranchsCodePos": "CS_0000281",
                "BranchsShortAdd": "720 Tổ 3, P. Đồng Tiến",
                "Districtid": "HBHA",
                "Cityid": "HBH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HBHB",
            "CityId": "HBH",
            "Districtname": "Đà Bắc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHC",
            "CityId": "HBH",
            "Districtname": "Mai Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHD",
            "CityId": "HBH",
            "Districtname": "Kỳ Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHE",
            "CityId": "HBH",
            "Districtname": "Lương Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHF",
            "CityId": "HBH",
            "Districtname": "Kim Bôi",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHG",
            "CityId": "HBH",
            "Districtname": "Tân Lạc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHH",
            "CityId": "HBH",
            "Districtname": "Lạc Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHI",
            "CityId": "HBH",
            "Districtname": "Lạc Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHJ",
            "CityId": "HBH",
            "Districtname": "Yên Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HBHK",
            "CityId": "HBH",
            "Districtname": "Cao Phong",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "HUI",
        "Cityname": "Huế",
        "AreaId": 2,
        "Top": 29,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HUIA",
            "CityId": "HUI",
            "Districtname": "Thành phố Huế",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 129,
                "BranchsAdd": "186 Hùng Vương, P. An Cựu, TP. Huế, Thừa Thiên Huế",
                "BranchsCodePos": "CS_0000173",
                "BranchsShortAdd": "186 Hùng Vương, P. An Cựu, TP. Huế",
                "Districtid": "HUIA",
                "Cityid": "HUI",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 144,
                "BranchsAdd": "09 Trần Hưng Đạo, TP. Huế, Thừa Thiên Huế",
                "BranchsCodePos": "CS_0000190",
                "BranchsShortAdd": "09 Trần Hưng Đạo, TP. Huế",
                "Districtid": "HUIA",
                "Cityid": "HUI",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 191,
                "BranchsAdd": "101 Hùng Vương, P. Phú Hội, TP. Huế, Thừa Thiên Huế",
                "BranchsCodePos": "CS_0000245",
                "BranchsShortAdd": "101 Hùng Vương, P. Phú Hội",
                "Districtid": "HUIA",
                "Cityid": "HUI",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "HUIB",
            "CityId": "HUI",
            "Districtname": "H.Phong Điền",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUIC",
            "CityId": "HUI",
            "Districtname": "Quảng Điền",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUID",
            "CityId": "HUI",
            "Districtname": "Hương Trà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUIE",
            "CityId": "HUI",
            "Districtname": "Phú Quang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUIF",
            "CityId": "HUI",
            "Districtname": "Hương Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUIG1",
            "CityId": "HUI",
            "Districtname": "Nam Đông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUIG2",
            "CityId": "HUI",
            "Districtname": "Phú Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUIH",
            "CityId": "HUI",
            "Districtname": "A Lưới",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "NTG",
        "Cityname": "Khánh Hoà",
        "AreaId": 2,
        "Top": 30,
        "ListBranchDistrictRivets": [{
            "DistrictId": "NTGB",
            "CityId": "NTG",
            "Districtname": "Thị Xã Cam Ranh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 337,
                "BranchsAdd": "2038 Hùng Vương, P. Cam Lộc, TP. Cam Ranh, Khánh Hòa",
                "BranchsCodePos": "CS_0000404",
                "BranchsShortAdd": "2038 Hùng Vương, Phường Cam Lộc,",
                "Districtid": "NTGB",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 372,
                "BranchsAdd": "391 Hùng Vương, P. Cam Nghĩa, TP. Cam Ranh, Khánh Hòa",
                "BranchsCodePos": "CS_0000438",
                "BranchsShortAdd": "391 Hùng Vương, P. Cam Nghĩa, TP. Cam Ranh, Khánh Hòa",
                "Districtid": "NTGB",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "NTGC",
            "CityId": "NTG",
            "Districtname": "Vạn Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NTGD",
            "CityId": "NTG",
            "Districtname": "Ninh Hoà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NTGE",
            "CityId": "NTG",
            "Districtname": "Diên Khánh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 357,
                "BranchsAdd": "237 Hùng Vương, T.T Diên Khánh, H. Diên Khánh, Khánh Hòa",
                "BranchsCodePos": "CS_0000423",
                "BranchsShortAdd": "237 Hùng Vương, TT Diên Khánh, H. Diên Khánh, Khánh Hòa",
                "Districtid": "NTGE",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "NTGF",
            "CityId": "NTG",
            "Districtname": "Khánh Vĩnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NTGG",
            "CityId": "NTG",
            "Districtname": "Khánh Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NTGH",
            "CityId": "NTG",
            "Districtname": "Trường Sa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NTGNHA",
            "CityId": "NTG",
            "Districtname": "Thành phố Nha Trang",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 153,
                "BranchsAdd": "9H Lê Thánh Tôn, P. Lộc Thọ, TP. Nha Trang, Khánh Hoà",
                "BranchsCodePos": "CS_0000198",
                "BranchsShortAdd": "9H Lê Thánh Tôn, P. Lộc Thọ",
                "Districtid": "NTGNHA",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 164,
                "BranchsAdd": "Lô Số 4, KDT Vĩnh Điềm Trung, Xã Vĩnh Hiệp, TP. Nha Trang,Khánh Hòa",
                "BranchsCodePos": "CS_0000212",
                "BranchsShortAdd": "Lô Số 4, KDT Vĩnh Điềm Trung, Xã Vĩnh Hiệp",
                "Districtid": "NTGNHA",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 3
            }, {
                "BranchsId": 273,
                "BranchsAdd": "106 -108 Lê Hồng Phong, TP. Nha Trang, Khánh Hòa",
                "BranchsCodePos": "CS_0000341",
                "BranchsShortAdd": "106 -108 Lê Hồng Phong, TP. Nha Trang",
                "Districtid": "NTGNHA",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 319,
                "BranchsAdd": "VBOX Lotte Nha Trang - 58 Đường 23/10, P. Phương Sơn, TP. Nha Trang, Khánh Hòa",
                "BranchsCodePos": "CS_0000389",
                "BranchsShortAdd": "VBOX Lotte Nha Trang - 58 Đường 23/10, P. Phương Sơn",
                "Districtid": "NTGNHA",
                "Cityid": "NTG",
                "Status": "A",
                "GroupId": 4
            }]
        }]
    }, {
        "Cityid": "KTM",
        "Cityname": "Kon Tum",
        "AreaId": 2,
        "Top": 31,
        "ListBranchDistrictRivets": [{
            "DistrictId": "KTM",
            "CityId": "KTM",
            "Districtname": "Tu Mơ Rông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTMA",
            "CityId": "KTM",
            "Districtname": "Thị xã Kon Tum",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 139,
                "BranchsAdd": "665 Phan Đình Phùng, P. Duy Tân, TP. Kontum",
                "BranchsCodePos": "CS_0000184",
                "BranchsShortAdd": "665 Phan Đình Phùng, P. Duy Tân, TP. Kontum",
                "Districtid": "KTMA",
                "Cityid": "KTM",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "KTMB",
            "CityId": "KTM",
            "Districtname": "Đắk Glei",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTMC",
            "CityId": "KTM",
            "Districtname": "Ngọc Hồi",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTMD",
            "CityId": "KTM",
            "Districtname": "Đắk Tô",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTME",
            "CityId": "KTM",
            "Districtname": "Kon Plông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTMF",
            "CityId": "KTM",
            "Districtname": "Đắk Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTMG",
            "CityId": "KTM",
            "Districtname": "Sa Thầy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KTMH",
            "CityId": "KTM",
            "Districtname": "Kon Rẫy",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "KGG",
        "Cityname": "Kiên Giang",
        "AreaId": 1,
        "Top": 32,
        "ListBranchDistrictRivets": [{
            "DistrictId": "KGGA",
            "CityId": "KGG",
            "Districtname": "Thành phố Rạch Giá",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 61,
                "BranchsAdd": "259-261 Nguyễn Trung Trực, P. Vĩnh Bảo, TP. Rạch Giá, Kiên Giang",
                "BranchsCodePos": "CS_0000095",
                "BranchsShortAdd": "259-261 Nguyễn Trung Trực, P. Vĩnh Bảo, Rạch Giá",
                "Districtid": "KGGA",
                "Cityid": "KGG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 286,
                "BranchsAdd": "30 Điện Biên Phủ, P. Vĩnh Quang, TP. Rạch Giá, Kiên Giang",
                "BranchsCodePos": "CS_0000357",
                "BranchsShortAdd": "30 Điện Biên Phủ, P. Vĩnh Quang",
                "Districtid": "KGGA",
                "Cityid": "KGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "KGGB",
            "CityId": "KGG",
            "Districtname": "Thị xã Hà Tiên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGC",
            "CityId": "KGG",
            "Districtname": "Kiên Lương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGD",
            "CityId": "KGG",
            "Districtname": "Hòn Đất",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGE",
            "CityId": "KGG",
            "Districtname": "Tân Hiệp",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGF",
            "CityId": "KGG",
            "Districtname": "Huyện. Châu Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGG",
            "CityId": "KGG",
            "Districtname": "Giồng Riềng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGH",
            "CityId": "KGG",
            "Districtname": "Gò Quao",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGI",
            "CityId": "KGG",
            "Districtname": "An Biên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGJ",
            "CityId": "KGG",
            "Districtname": "An Minh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGK",
            "CityId": "KGG",
            "Districtname": "Vĩnh Thuận",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "KGGL",
            "CityId": "KGG",
            "Districtname": "Phú Quốc",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 361,
                "BranchsAdd": "231 Nguyễn Trung Trực, T.T Dương Ðông, Phú Quốc, Kiên Giang",
                "BranchsCodePos": "CS_0000427",
                "BranchsShortAdd": "231 Nguyễn Trung Trực, T.T Dương Ðông, Phú Quốc, Kiên Giang",
                "Districtid": "KGGL",
                "Cityid": "KGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "KGGM",
            "CityId": "KGG",
            "Districtname": "Kiên Hải",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "LCI",
        "Cityname": "Lào Cai",
        "AreaId": 3,
        "Top": 33,
        "ListBranchDistrictRivets": [{
            "DistrictId": "LCIA",
            "CityId": "LCI",
            "Districtname": "Thành phố Lào Cai",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 202,
                "BranchsAdd": "Số 28 Đ.Hoàng Sao (Tổ 9, đường Hoàng Liên), P. Duyên Hải, TP. Lào Cai",
                "BranchsCodePos": "CS_0000261",
                "BranchsShortAdd": "28 Hoàng Sao, P. Duyên Hải",
                "Districtid": "LCIA",
                "Cityid": "LCI",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LCIC",
            "CityId": "LCI",
            "Districtname": "Mường Khương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCID",
            "CityId": "LCI",
            "Districtname": "Bát Xát",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCIE",
            "CityId": "LCI",
            "Districtname": "Si Ma Cai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCIF",
            "CityId": "LCI",
            "Districtname": "Bắc Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCIG",
            "CityId": "LCI",
            "Districtname": "Bảo Thắng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCIH",
            "CityId": "LCI",
            "Districtname": "Sa Pa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCII",
            "CityId": "LCI",
            "Districtname": "Bảo Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCIK",
            "CityId": "LCI",
            "Districtname": "Văn Bàn",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "LSN",
        "Cityname": "Lạng Sơn",
        "AreaId": 3,
        "Top": 34,
        "ListBranchDistrictRivets": [{
            "DistrictId": "LSNA",
            "CityId": "LSN",
            "Districtname": "Thành phố Lạng Sơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 306,
                "BranchsAdd": "08 Trần Đăng Ninh, P. Vĩnh Trại, TP. Lạng Sơn",
                "BranchsCodePos": "CS_0000374",
                "BranchsShortAdd": "08 Trần Đăng Ninh, P. Vĩnh Trại",
                "Districtid": "LSNA",
                "Cityid": "LSN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LSNB",
            "CityId": "LSN",
            "Districtname": "Tràng Định",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNC",
            "CityId": "LSN",
            "Districtname": "Văn Lãng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSND",
            "CityId": "LSN",
            "Districtname": "Bình Gia",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNE",
            "CityId": "LSN",
            "Districtname": "Bắc Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNF",
            "CityId": "LSN",
            "Districtname": "Văn Quan",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNG",
            "CityId": "LSN",
            "Districtname": "Cao Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNH",
            "CityId": "LSN",
            "Districtname": "Lộc Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNI",
            "CityId": "LSN",
            "Districtname": "Chi Lăng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNJ",
            "CityId": "LSN",
            "Districtname": "Đình Lập",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LSNK",
            "CityId": "LSN",
            "Districtname": "Hữu Lũng",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "LCU",
        "Cityname": "Lai Châu",
        "AreaId": 3,
        "Top": 35,
        "ListBranchDistrictRivets": [{
            "DistrictId": "LCU",
            "CityId": "LCU",
            "Districtname": "Thành phố Lai Châu",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 204,
                "BranchsAdd": "Số 02 Trần Hưng Đạo, P. Đoàn Kết, TP. Lai Châu",
                "BranchsCodePos": "CS_0000262",
                "BranchsShortAdd": "02 Trần Hưng Đạo, P. Đoàn Kết",
                "Districtid": "LCU",
                "Cityid": "LCU",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LCUA",
            "CityId": "LCU",
            "Districtname": "Tam Đường",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCUB",
            "CityId": "LCU",
            "Districtname": "Phong Thổ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCUC",
            "CityId": "LCU",
            "Districtname": "Mường Tè",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCUD",
            "CityId": "LCU",
            "Districtname": "Sìn Hồ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LCUE",
            "CityId": "LCU",
            "Districtname": "Than Uyên",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "DLT",
        "Cityname": "Lâm Đồng",
        "AreaId": 2,
        "Top": 36,
        "ListBranchDistrictRivets": [{
            "DistrictId": "DLTA",
            "CityId": "DLT",
            "Districtname": "TP. Đà Lạt",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 166,
                "BranchsAdd": "Lô 8, TTTM BigC Đà Lạt, Hồ Tùng Mậu, P.10, TP. Đà Lạt, Lâm Đồng",
                "BranchsCodePos": "CS_0000211",
                "BranchsShortAdd": "Shop 08, Lô 8, Hồ Tùng Mậu, P.10 - BigC Đà Lạt",
                "Districtid": "DLTA",
                "Cityid": "DLT",
                "Status": "A",
                "GroupId": 3
            }, {
                "BranchsId": 251,
                "BranchsAdd": "83 Phan Đình Phùng, P.1, TP. Đà Lạt, Lâm Đồng",
                "BranchsCodePos": "CS_0000318",
                "BranchsShortAdd": "83 Phan Đình Phùng, P.1",
                "Districtid": "DLTA",
                "Cityid": "DLT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DLTB",
            "CityId": "DLT",
            "Districtname": "TP. Bảo Lộc",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 316,
                "BranchsAdd": "541A Trần Phú, P. Lộc Tiến, TP. Bảo Lộc, Lâm Đồng",
                "BranchsCodePos": "CS_0000386",
                "BranchsShortAdd": "541A Trần Phú, P. Lộc Tiến",
                "Districtid": "DLTB",
                "Cityid": "DLT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DLTC",
            "CityId": "DLT",
            "Districtname": "Lạc Dương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTD",
            "CityId": "DLT",
            "Districtname": "Đơn Dương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTE",
            "CityId": "DLT",
            "Districtname": "Đức Trọng",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 147,
                "BranchsAdd": "Khu phố 2, TT. Liên Nghĩa, H. Đức Trọng, Lâm Đồng",
                "BranchsCodePos": "CS_0000191",
                "BranchsShortAdd": "Khu phố 2, TT. Liên Nghĩa, H. Đức Trọng",
                "Districtid": "DLTE",
                "Cityid": "DLT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "DLTF",
            "CityId": "DLT",
            "Districtname": "Lâm Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTG",
            "CityId": "DLT",
            "Districtname": "H. Bảo Lâm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTH",
            "CityId": "DLT",
            "Districtname": "Di Linh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTI",
            "CityId": "DLT",
            "Districtname": "Đạ Huoai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTJ",
            "CityId": "DLT",
            "Districtname": "Đạ Tẻh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "DLTK",
            "CityId": "DLT",
            "Districtname": "Cát Tiên",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "LAN",
        "Cityname": "Long An",
        "AreaId": 1,
        "Top": 37,
        "ListBranchDistrictRivets": [{
            "DistrictId": "LANA",
            "CityId": "LAN",
            "Districtname": "Thành phố Tân An",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 57,
                "BranchsAdd": "CoopMart - 01 Mai Thị Tốt, P.2, TP. Tân An, Long An",
                "BranchsCodePos": "CS_0000049",
                "BranchsShortAdd": "CoopMart - 01 Mai Thị Tốt , P.2, TP. Tân An",
                "Districtid": "LANA",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 3
            }, {
                "BranchsId": 127,
                "BranchsAdd": "18 Nguyễn Trung Trực, P.2, TP. Tân An, Long An",
                "BranchsCodePos": "CS_0000169",
                "BranchsShortAdd": "18 Nguyễn Trung Trực, P.2, TP. Tân An",
                "Districtid": "LANA",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LANB",
            "CityId": "LAN",
            "Districtname": "Tân Hưng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANC",
            "CityId": "LAN",
            "Districtname": "Vĩnh Hưng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LAND",
            "CityId": "LAN",
            "Districtname": "Mộc Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANE",
            "CityId": "LAN",
            "Districtname": "Tân Thạnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANF",
            "CityId": "LAN",
            "Districtname": "Thạnh Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANG",
            "CityId": "LAN",
            "Districtname": "Đức Huệ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANH",
            "CityId": "LAN",
            "Districtname": "Đức Hòa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 155,
                "BranchsAdd": "315 tổ 12, ấp Bình Tiền 2, xã Đức Hòa Hạ, H. Đức Hòa, Long An ",
                "BranchsCodePos": "CS_0000201",
                "BranchsShortAdd": "315 tổ 12, ấp Bình Tiền 2, xã Đức Hoà Hạ",
                "Districtid": "LANH",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 200,
                "BranchsAdd": "148 Tỉnh Lộ 10, TT. Hậu Nghĩa, H. Đức Hoà, Long An",
                "BranchsCodePos": "CS_0000251",
                "BranchsShortAdd": "148 TL 10, TT. Hậu Nghĩa",
                "Districtid": "LANH",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LANI",
            "CityId": "LAN",
            "Districtname": "Bến Lức",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 143,
                "BranchsAdd": "131 Võ Công Tồn, TT. Bến Lức, Long An",
                "BranchsCodePos": "CS_0000189",
                "BranchsShortAdd": "131 Võ Công Tồn, TT. Bến Lức",
                "Districtid": "LANI",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LANJ",
            "CityId": "LAN",
            "Districtname": "Thủ Thừa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANK",
            "CityId": "LAN",
            "Districtname": "H..Châu Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANL",
            "CityId": "LAN",
            "Districtname": "Tân Trụ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "LANM",
            "CityId": "LAN",
            "Districtname": "Cần Đước",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 175,
                "BranchsAdd": "Quốc Lộ 50, KP. 3, TT. Cần Đước, Long An",
                "BranchsCodePos": "CS_0000226",
                "BranchsShortAdd": "Quốc Lộ 50, KP. 3, TT. Cần Đước",
                "Districtid": "LANM",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "LANN",
            "CityId": "LAN",
            "Districtname": "Cần Giuộc",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 174,
                "BranchsAdd": "107 Quốc Lộ 50, TT. Cần Giuộc, Long An",
                "BranchsCodePos": "CS_0000227",
                "BranchsShortAdd": "107 Quốc Lộ 50, TT. Cần Giuộc",
                "Districtid": "LANN",
                "Cityid": "LAN",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "NMD",
        "Cityname": "Nam Định",
        "AreaId": 3,
        "Top": 38,
        "ListBranchDistrictRivets": [{
            "DistrictId": "NMDA",
            "CityId": "NMD",
            "Districtname": "Thành phố Nam Định",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 294,
                "BranchsAdd": "51 Điện Biên, P. Cửa Bắc, TP. Nam Định",
                "BranchsCodePos": "CS_0000364",
                "BranchsShortAdd": "51 Điện Biên, P. Cửa Bắc",
                "Districtid": "NMDA",
                "Cityid": "NMD",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "NMDB",
            "CityId": "NMD",
            "Districtname": "Vụ Bản",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDC",
            "CityId": "NMD",
            "Districtname": "Mỹ Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDD",
            "CityId": "NMD",
            "Districtname": "Ý Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDE",
            "CityId": "NMD",
            "Districtname": "Nam Trực",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDF",
            "CityId": "NMD",
            "Districtname": "Trực Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDG",
            "CityId": "NMD",
            "Districtname": "Xuân Trường",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDH",
            "CityId": "NMD",
            "Districtname": "Giao Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDI",
            "CityId": "NMD",
            "Districtname": "Nghĩa Hưng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NMDJ",
            "CityId": "NMD",
            "Districtname": "Hải Hậu",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "NAN",
        "Cityname": "Nghệ An",
        "AreaId": 2,
        "Top": 39,
        "ListBranchDistrictRivets": [{
            "DistrictId": "NANB",
            "CityId": "NAN",
            "Districtname": "Thị xã Của Lò",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANC",
            "CityId": "NAN",
            "Districtname": "Quế Phong",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NAND",
            "CityId": "NAN",
            "Districtname": "Quỳ Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANE",
            "CityId": "NAN",
            "Districtname": "H. Kỳ Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANF",
            "CityId": "NAN",
            "Districtname": "Quỳ Hợp",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANG",
            "CityId": "NAN",
            "Districtname": "Nghĩa Đàn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANH",
            "CityId": "NAN",
            "Districtname": "Tương Dương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANI",
            "CityId": "NAN",
            "Districtname": "Quỳnh Lưu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANJ",
            "CityId": "NAN",
            "Districtname": "Tân Kỳ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANK",
            "CityId": "NAN",
            "Districtname": "Con Cuông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANL",
            "CityId": "NAN",
            "Districtname": "Yên Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANM",
            "CityId": "NAN",
            "Districtname": "Diễn Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANN",
            "CityId": "NAN",
            "Districtname": "Anh Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANO",
            "CityId": "NAN",
            "Districtname": "Đô Lương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANP",
            "CityId": "NAN",
            "Districtname": "Thanh Chương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANQ",
            "CityId": "NAN",
            "Districtname": "Nghi Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANR",
            "CityId": "NAN",
            "Districtname": "Nam Đàn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANS",
            "CityId": "NAN",
            "Districtname": "Hưng Nguyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NANVNH",
            "CityId": "NAN",
            "Districtname": "Thành phố Vinh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 87,
                "BranchsAdd": "87 Nguyễn Thị Minh Khai, P. Lê Mao, TP. Vinh, Nghệ An",
                "BranchsCodePos": "CS_0000115",
                "BranchsShortAdd": "87 Nguyễn Thị Minh Khai, P. Lê Mao, Vinh",
                "Districtid": "NANVNH",
                "Cityid": "NAN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 288,
                "BranchsAdd": "385 Nguyễn Văn Cừ, TP. Vinh, Nghệ An",
                "BranchsCodePos": "CS_0000339",
                "BranchsShortAdd": "385 Nguyễn Văn Cừ, TP. Vinh",
                "Districtid": "NANVNH",
                "Cityid": "NAN",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 323,
                "BranchsAdd": "51 Hồ Tùng Mậu, P. Trường Thi, TP. Vinh, Nghệ An",
                "BranchsCodePos": "CS_0000392",
                "BranchsShortAdd": "51 Hồ Tùng Mậu, P. Trường Thi",
                "Districtid": "NANVNH",
                "Cityid": "NAN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "NANT",
            "CityId": "NAN",
            "Districtname": "Thị xã Thái Hoà",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 360,
                "BranchsAdd": "200-202 Trần Hưng Ðạo, P. Hòa Hiếu, TX. Thái Hòa, Nghệ An",
                "BranchsCodePos": "CS_0000426",
                "BranchsShortAdd": "200-202 Trần Hưng Ðạo, P. Hòa Hiếu, TX. Thái Hòa, Nghệ An",
                "Districtid": "NANT",
                "Cityid": "NAN",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "PRG",
        "Cityname": "Ninh Thuận",
        "AreaId": 1,
        "Top": 40,
        "ListBranchDistrictRivets": [{
            "DistrictId": "PRGA",
            "CityId": "PRG",
            "Districtname": "Thị xã Phan Rang - Tháp Chàm",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 187,
                "BranchsAdd": "01 Yersin, P. Mỹ Hương, TP. Phan Rang - Tháp Chàm, Ninh Thuận",
                "BranchsCodePos": "CS_0000238",
                "BranchsShortAdd": "01 Yersin, P. Mỹ Hương",
                "Districtid": "PRGA",
                "Cityid": "PRG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 340,
                "BranchsAdd": "Số 753, Ðường 21/8, P. Bảo An, TP. Phan Rang - Tháp Chàm, Ninh Thuận",
                "BranchsCodePos": "CS_0000407",
                "BranchsShortAdd": "Số 753, Ðường 21/8, P. Bảo An",
                "Districtid": "PRGA",
                "Cityid": "PRG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "PRGB",
            "CityId": "PRG",
            "Districtname": "Ninh Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PRGC",
            "CityId": "PRG",
            "Districtname": "Bác Ái",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PRGD",
            "CityId": "PRG",
            "Districtname": "Ninh Hải",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PRGE",
            "CityId": "PRG",
            "Districtname": "Ninh Phước",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "NBH",
        "Cityname": "Ninh Bình",
        "AreaId": 3,
        "Top": 41,
        "ListBranchDistrictRivets": [{
            "DistrictId": "NBHA",
            "CityId": "NBH",
            "Districtname": "Thị xã Ninh Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBHB",
            "CityId": "NBH",
            "Districtname": "Thị xã Tam Điệp",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 212,
                "BranchsAdd": "Tổ 3, P. Trung Sơn, TP. Tam Điệp, Ninh Bình",
                "BranchsCodePos": "CS_0000273",
                "BranchsShortAdd": "Tổ 3, P. Trung Sơn, TP. Tam Điệp",
                "Districtid": "NBHB",
                "Cityid": "NBH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "NBHC",
            "CityId": "NBH",
            "Districtname": "Nho Quan",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBHD",
            "CityId": "NBH",
            "Districtname": "Gia Viễn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBHE",
            "CityId": "NBH",
            "Districtname": "Hoa Lư",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBHF",
            "CityId": "NBH",
            "Districtname": "Yên Mô",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBHG",
            "CityId": "NBH",
            "Districtname": "Yên Khánh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBHH",
            "CityId": "NBH",
            "Districtname": "Kim Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "NBNP",
            "CityId": "NBH",
            "Districtname": "Ninh Phúc",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "QBH",
        "Cityname": "Quảng Bình",
        "AreaId": 2,
        "Top": 42,
        "ListBranchDistrictRivets": [{
            "DistrictId": "QBHB",
            "CityId": "QBH",
            "Districtname": "Tuyên Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QBHC",
            "CityId": "QBH",
            "Districtname": "Minh Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QBHDOH",
            "CityId": "QBH",
            "Districtname": "Thành phố Đồng Hới",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 170,
                "BranchsAdd": "Tiểu khu 2, Trần Hưng Đạo, TP. Đồng Hới, Quảng Bình",
                "BranchsCodePos": "CS_0000215",
                "BranchsShortAdd": "Tiểu khu 2, Trần Hưng Đạo, Quảng Bình",
                "Districtid": "QBHDOH",
                "Cityid": "QBH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QBHE",
            "CityId": "QBH",
            "Districtname": "Bố Trạch",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QBHF",
            "CityId": "QBH",
            "Districtname": "Quảng Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QBHG",
            "CityId": "QBH",
            "Districtname": "Lệ Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QBBD",
            "CityId": "QBH",
            "Districtname": "Thị xã Ba Đồn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 293,
                "BranchsAdd": "93 Hùng Vương, P. Ba Đồn, TX. Ba Đồn, Quảng Bình",
                "BranchsCodePos": "CS_0000359",
                "BranchsShortAdd": "93 Hùng Vương, P. Ba Đồn",
                "Districtid": "QBBD",
                "Cityid": "QBH",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "QNA",
        "Cityname": "Quảng Nam",
        "AreaId": 2,
        "Top": 43,
        "ListBranchDistrictRivets": [{
            "DistrictId": "QNA",
            "CityId": "QNA",
            "Districtname": "Phú Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAA",
            "CityId": "QNA",
            "Districtname": "TP Tam Kỳ",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 177,
                "BranchsAdd": "7 Phan Chu Trinh, P. Phước Hòa, TP. Tam Kỳ, Quảng Nam",
                "BranchsCodePos": "CS_0000228",
                "BranchsShortAdd": "7 Phan Chu Trinh, P. Phước Hoà",
                "Districtid": "QNAA",
                "Cityid": "QNA",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNAB",
            "CityId": "QNA",
            "Districtname": "Thị xã Hội An",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAC",
            "CityId": "QNA",
            "Districtname": "Đông Giang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAD",
            "CityId": "QNA",
            "Districtname": "Đại Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAE",
            "CityId": "QNA",
            "Districtname": "Điện Bàn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAF",
            "CityId": "QNA",
            "Districtname": "Duy Xuyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAG",
            "CityId": "QNA",
            "Districtname": "Nam Giang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAH",
            "CityId": "QNA",
            "Districtname": "Thăng Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAI",
            "CityId": "QNA",
            "Districtname": "Quế Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAJ",
            "CityId": "QNA",
            "Districtname": "Hiệp Đức",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAK",
            "CityId": "QNA",
            "Districtname": "Tiên Phước",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAL",
            "CityId": "QNA",
            "Districtname": "Phước Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAM",
            "CityId": "QNA",
            "Districtname": "Núi Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAN",
            "CityId": "QNA",
            "Districtname": "Nam Trà My",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAO",
            "CityId": "QNA",
            "Districtname": "Tây Giang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNAP",
            "CityId": "QNA",
            "Districtname": "Bắc Trà My",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "XNG",
        "Cityname": "Quảng Ngãi",
        "AreaId": 2,
        "Top": 44,
        "ListBranchDistrictRivets": [{
            "DistrictId": "XNGA",
            "CityId": "XNG",
            "Districtname": "TP. Quảng Ngãi",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 150,
                "BranchsAdd": "472 Quang Trung, P. Nguyễn Nghiêm, TP. Quảng Ngãi",
                "BranchsCodePos": "CS_0000192",
                "BranchsShortAdd": "472 Quang Trung, P. Nguyễn Nghiêm, Quảng Ngãi",
                "Districtid": "XNGA",
                "Cityid": "XNG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 221,
                "BranchsAdd": "02 Hai Bà Trưng, TP. Quảng Ngãi",
                "BranchsCodePos": "CS_0000288",
                "BranchsShortAdd": "02 Hai Bà Trưng, TP. Quảng Ngãi",
                "Districtid": "XNGA",
                "Cityid": "XNG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "XNGB",
            "CityId": "XNG",
            "Districtname": "Lý Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGC",
            "CityId": "XNG",
            "Districtname": "Bình Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGD",
            "CityId": "XNG",
            "Districtname": "Trà Bồng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGE",
            "CityId": "XNG",
            "Districtname": "Sơn Tịnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGF",
            "CityId": "XNG",
            "Districtname": "Sơn Tây",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGG",
            "CityId": "XNG",
            "Districtname": "Sơn Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGH",
            "CityId": "XNG",
            "Districtname": "Tư Nghĩa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGI",
            "CityId": "XNG",
            "Districtname": "Nghĩa Hành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGJ",
            "CityId": "XNG",
            "Districtname": "Minh Long",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGK",
            "CityId": "XNG",
            "Districtname": "Mộ Đức",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGL",
            "CityId": "XNG",
            "Districtname": "Đức Phổ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGM",
            "CityId": "XNG",
            "Districtname": "Ba Tơ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "XNGN",
            "CityId": "XNG",
            "Districtname": "Tây Trà",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "QNH",
        "Cityname": "Quảng Ninh",
        "AreaId": 3,
        "Top": 45,
        "ListBranchDistrictRivets": [{
            "DistrictId": "QNHA",
            "CityId": "QNH",
            "Districtname": "Thành phố Hạ Long",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 379,
                "BranchsAdd": "Tổ 9, KP 6, P. Giếng Ðáy, TP. Hạ Long, Quảng Ninh",
                "BranchsCodePos": "CS_0000445",
                "BranchsShortAdd": "Tổ 9, KP 6, P. Giếng Ðáy",
                "Districtid": "QNHA",
                "Cityid": "QNH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNHC",
            "CityId": "QNH",
            "Districtname": "Thị xã Uông Bí",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHCPH",
            "CityId": "QNH",
            "Districtname": "Thị xã Cẩm Phả",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHE",
            "CityId": "QNH",
            "Districtname": "Bình Liêu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHF",
            "CityId": "QNH",
            "Districtname": "Hải Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHG",
            "CityId": "QNH",
            "Districtname": "Đầm Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHH",
            "CityId": "QNH",
            "Districtname": "Tiên Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHI",
            "CityId": "QNH",
            "Districtname": "Ba Chế",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHJ",
            "CityId": "QNH",
            "Districtname": "Vân Đồn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHK",
            "CityId": "QNH",
            "Districtname": "Hoành Bồ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHL1",
            "CityId": "QNH",
            "Districtname": "Đông Triều",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 279,
                "BranchsAdd": "81 Vĩnh Thông, P. Mạo Khê, TX. Đông Triều, Quảng Ninh",
                "BranchsCodePos": "CS_0000351",
                "BranchsShortAdd": "81 Vĩnh Thông, P. Mạo Khê",
                "Districtid": "QNHL1",
                "Cityid": "QNH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QNHL3",
            "CityId": "QNH",
            "Districtname": "Yên Hưng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHM",
            "CityId": "QNH",
            "Districtname": "Cô Tô",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QNHMOC",
            "CityId": "QNH",
            "Districtname": "Thị xã Móng Cái",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "QUT",
        "Cityname": "Quảng Trị",
        "AreaId": 2,
        "Top": 46,
        "ListBranchDistrictRivets": [{
            "DistrictId": "QUT",
            "CityId": "QUT",
            "Districtname": "Cồn Cỏ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QUTA",
            "CityId": "QUT",
            "Districtname": "Thành Phố Đông Hà",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 190,
                "BranchsAdd": "169 Lê Duẩn, P. Đông Lễ, TP. Đông Hà, Quảng Trị",
                "BranchsCodePos": "CS_0000242",
                "BranchsShortAdd": "169 Lê Duẩn, P. Đông Lễ",
                "Districtid": "QUTA",
                "Cityid": "QUT",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 353,
                "BranchsAdd": "Số 11 Trần Hưng Ðạo, P.1, TP.Ðông Hà, Tỉnh Quảng Trị",
                "BranchsCodePos": "CS_0000419",
                "BranchsShortAdd": "Số 11 Trần Hưng Ðạo, P.1, TP.Ðông Hà, Tỉnh Quảng Trị",
                "Districtid": "QUTA",
                "Cityid": "QUT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QUTB",
            "CityId": "QUT",
            "Districtname": "Thị xã Quảng Trị",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 299,
                "BranchsAdd": "Số 242 Trần Hưng Đạo, P.2, TX. Quảng Trị",
                "BranchsCodePos": "CS_0000367",
                "BranchsShortAdd": "242 Trần Hưng Đạo, P.2",
                "Districtid": "QUTB",
                "Cityid": "QUT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QUTC",
            "CityId": "QUT",
            "Districtname": "Vĩnh Linh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QUTD",
            "CityId": "QUT",
            "Districtname": "Gio Linh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QUTE",
            "CityId": "QUT",
            "Districtname": "Cam Lộ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QUTF",
            "CityId": "QUT",
            "Districtname": "Triệu phong",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QUTG",
            "CityId": "QUT",
            "Districtname": "Hải Lăng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "QUTH",
            "CityId": "QUT",
            "Districtname": "Hướng Hóa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 292,
                "BranchsAdd": "105 Lê Duẩn, TT. Khe Sanh, H. Hướng Hoá, Quảng Trị",
                "BranchsCodePos": "CS_0000345",
                "BranchsShortAdd": "105 Lê Duẩn, TT. Khe Sanh",
                "Districtid": "QUTH",
                "Cityid": "QUT",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "QUTI",
            "CityId": "QUT",
            "Districtname": "Đa Krông",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "PYN",
        "Cityname": "Phú Yên",
        "AreaId": 2,
        "Top": 47,
        "ListBranchDistrictRivets": [{
            "DistrictId": "PYNA",
            "CityId": "PYN",
            "Districtname": "Thành phố Tuy Hòa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYNB",
            "CityId": "PYN",
            "Districtname": "Đồng Xuân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYNC",
            "CityId": "PYN",
            "Districtname": "Sông Cầu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYND",
            "CityId": "PYN",
            "Districtname": "Tuy An",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYNE",
            "CityId": "PYN",
            "Districtname": "Sơn Hòa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYNF",
            "CityId": "PYN",
            "Districtname": "Tuy Hòa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYNG",
            "CityId": "PYN",
            "Districtname": "Sông Hinh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PYNH",
            "CityId": "PYN",
            "Districtname": "Phú Hòa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 339,
                "BranchsAdd": "Số 383 Nguyễn Huệ, P.5, TP. Tuy Hòa, Phú Yên",
                "BranchsCodePos": "CS_0000406",
                "BranchsShortAdd": "Số 383 Nguyễn Huệ, P.5, TP. Tuy Hòa",
                "Districtid": "PYNH",
                "Cityid": "PYN",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "PTO",
        "Cityname": "Phú Thọ",
        "AreaId": 3,
        "Top": 48,
        "ListBranchDistrictRivets": [{
            "DistrictId": "PTOA",
            "CityId": "PTO",
            "Districtname": "Thành phố Việt Trì",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 107,
                "BranchsAdd": "1606A Hùng Vương, Khu 11, P. Gia Cẩm, TP. Việt Trì, Phú Thọ",
                "BranchsCodePos": "CS_0000134",
                "BranchsShortAdd": "1606A, Hùng Vương, Khu 11, P. Gia Cẩm, Việt Trì",
                "Districtid": "PTOA",
                "Cityid": "PTO",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 167,
                "BranchsAdd": "2871 Hùng Vương, P. Vân Cơ, TP. Việt Trì, Phú Thọ",
                "BranchsCodePos": "CS_0000213",
                "BranchsShortAdd": "2871 Hùng Vương, P. Vân Cơ",
                "Districtid": "PTOA",
                "Cityid": "PTO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "PTOB",
            "CityId": "PTO",
            "Districtname": "Thị xã Phú Thọ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOC",
            "CityId": "PTO",
            "Districtname": "Đoan Hùng",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 369,
                "BranchsAdd": "Khu Tân Thành, T.T Ðoan Hùng, H. Ðoan Hùng, T. Phú Thọ",
                "BranchsCodePos": "CS_0000435",
                "BranchsShortAdd": "Khu Tân Thành, T.T Ðoan Hùng, H. Ðoan Hùng, T. Phú Thọ",
                "Districtid": "PTOC",
                "Cityid": "PTO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "PTOD",
            "CityId": "PTO",
            "Districtname": "Hạ Hòa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOE",
            "CityId": "PTO",
            "Districtname": "Thanh Ba",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOF",
            "CityId": "PTO",
            "Districtname": "Phù Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOG",
            "CityId": "PTO",
            "Districtname": "Lâm Thao",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOH",
            "CityId": "PTO",
            "Districtname": "Cẩm Khê",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOI",
            "CityId": "PTO",
            "Districtname": "Yên Lập",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOJ",
            "CityId": "PTO",
            "Districtname": "H.Tam Nông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOK",
            "CityId": "PTO",
            "Districtname": "Thanh Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "PTOL",
            "CityId": "PTO",
            "Districtname": "Thanh Sơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 368,
                "BranchsAdd": "Số 27, Phố Ba Mỏ, T.T Thanh Sơn, H. Thanh Sơn, T. Phú Thọ",
                "BranchsCodePos": "CS_0000434",
                "BranchsShortAdd": "Số 27, Phố Ba Mỏ, T.T Thanh Sơn, H. Thanh Sơn, T. Phú Thọ",
                "Districtid": "PTOL",
                "Cityid": "PTO",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "STG",
        "Cityname": "Sóc Trăng",
        "AreaId": 1,
        "Top": 49,
        "ListBranchDistrictRivets": [{
            "DistrictId": "STGA",
            "CityId": "STG",
            "Districtname": "Thành Phố Sóc Trăng",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 223,
                "BranchsAdd": "01 Trần Hưng Đạo, P.3, TP. Sóc Trăng",
                "BranchsCodePos": "CS_0000287",
                "BranchsShortAdd": "01 Trần Hưng Đạo, P.3",
                "Districtid": "STGA",
                "Cityid": "STG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "STGB",
            "CityId": "STG",
            "Districtname": "Kế Sách",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGC",
            "CityId": "STG",
            "Districtname": "Long Phú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGCLO",
            "CityId": "STG",
            "Districtname": "Cù Lao",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGD",
            "CityId": "STG",
            "Districtname": "Mỹ Tú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGE",
            "CityId": "STG",
            "Districtname": "Mỹ Xuyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGF",
            "CityId": "STG",
            "Districtname": "Thạnh Trị",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGG",
            "CityId": "STG",
            "Districtname": "Vĩnh Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "STGI",
            "CityId": "STG",
            "Districtname": "Huyện Ngã Năm",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "SQH",
        "Cityname": "Sơn La",
        "AreaId": 3,
        "Top": 50,
        "ListBranchDistrictRivets": [{
            "DistrictId": "SQHA",
            "CityId": "SQH",
            "Districtname": "TP. Sơn La",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 261,
                "BranchsAdd": "343 Chu Văn Thịnh, P. Chiềng Lề, TP, Sơn La",
                "BranchsCodePos": "CS_0000328",
                "BranchsShortAdd": "343 Chu Văn Thịnh, P. Chiềng Lề",
                "Districtid": "SQHA",
                "Cityid": "SQH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "SQHB",
            "CityId": "SQH",
            "Districtname": "Quỳnh Nhai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHC",
            "CityId": "SQH",
            "Districtname": "Mường La",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHD",
            "CityId": "SQH",
            "Districtname": "Thuận Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHE1",
            "CityId": "SQH",
            "Districtname": "Phù Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHE2",
            "CityId": "SQH",
            "Districtname": "Bắc Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHG",
            "CityId": "SQH",
            "Districtname": "Mai Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHH",
            "CityId": "SQH",
            "Districtname": "Sông Mã",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHI",
            "CityId": "SQH",
            "Districtname": "Yên Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHJ",
            "CityId": "SQH",
            "Districtname": "Mộc Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "SQHK",
            "CityId": "SQH",
            "Districtname": "Sốp Cộp",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "TNH",
        "Cityname": "Tây Ninh",
        "AreaId": 1,
        "Top": 51,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TNHA",
            "CityId": "TNH",
            "Districtname": "Thành Phố Tây Ninh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 142,
                "BranchsAdd": "374 Cách Mạng Tháng 8, P.3, TP. Tây Ninh",
                "BranchsCodePos": "CS_0000186",
                "BranchsShortAdd": "374 Cách Mạng Tháng 8, P.3",
                "Districtid": "TNHA",
                "Cityid": "TNH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TNHB",
            "CityId": "TNH",
            "Districtname": "Tân Biên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNHC",
            "CityId": "TNH",
            "Districtname": "Tân Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNHD",
            "CityId": "TNH",
            "Districtname": "Dương Minh Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNHE",
            "CityId": "TNH",
            "Districtname": "Châu Thành - Tây Ninh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNHF",
            "CityId": "TNH",
            "Districtname": "Hòa Thành",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 238,
                "BranchsAdd": "4-6-8, Phạm Hùng, TT. Hòa Thành, Tây Ninh",
                "BranchsCodePos": "CS_0000308",
                "BranchsShortAdd": "4-6-8, Phạm Hùng, TT. Hòa Thành",
                "Districtid": "TNHF",
                "Cityid": "TNH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TNHG",
            "CityId": "TNH",
            "Districtname": "Bến Cầu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNHH",
            "CityId": "TNH",
            "Districtname": "Gò Dầu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNHI",
            "CityId": "TNH",
            "Districtname": "Trảng Bàng",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 247,
                "BranchsAdd": "180 Quốc lộ 22, TT. Trảng Bàng, Tây Ninh",
                "BranchsCodePos": "CS_0000271",
                "BranchsShortAdd": "180 Quốc lộ 22, TT. Trảng Bàng, Tây Ninh",
                "Districtid": "TNHI",
                "Cityid": "TNH",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "TBH",
        "Cityname": "Thái Bình",
        "AreaId": 3,
        "Top": 52,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TBHA",
            "CityId": "TBH",
            "Districtname": "Thành phố Thái Bình",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 145,
                "BranchsAdd": "368 Lý Bôn, P. Tiền Phong (tầng 1 bến xe khách Hàng Hòa), TP. Thái Bình",
                "BranchsCodePos": "CS_0000193",
                "BranchsShortAdd": "368 Lý Bôn, P. Tiền Phong, Thái Bình",
                "Districtid": "TBHA",
                "Cityid": "TBH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TBHB",
            "CityId": "TBH",
            "Districtname": "Quỳnh Phụ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TBHC",
            "CityId": "TBH",
            "Districtname": "Hưng Hà",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TBHD",
            "CityId": "TBH",
            "Districtname": "Thái Thụy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TBHE",
            "CityId": "TBH",
            "Districtname": "Đông Hưng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TBHF",
            "CityId": "TBH",
            "Districtname": "Vũ Thư",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TBHG",
            "CityId": "TBH",
            "Districtname": "Kiến Xương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TBHH",
            "CityId": "TBH",
            "Districtname": "Tiền Hải",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 278,
                "BranchsAdd": "168 Tiêu Hoàng, TT. Tiền Hải, Thái Bình",
                "BranchsCodePos": "CS_0000350",
                "BranchsShortAdd": "168 Tiêu Hoàng, TT. Tiền Hải",
                "Districtid": "TBHH",
                "Cityid": "TBH",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "TNN",
        "Cityname": "Thái Nguyên",
        "AreaId": 3,
        "Top": 53,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TNNA",
            "CityId": "TNN",
            "Districtname": "Thành phố Thái Nguyên",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 148,
                "BranchsAdd": "477 Lương Ngọc Quyến, TP. Thái Nguyên",
                "BranchsCodePos": "CS_0000195",
                "BranchsShortAdd": "477 Lương Ngọc Quyến, TP. Thái Nguyên",
                "Districtid": "TNNA",
                "Cityid": "TNN",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TNNB",
            "CityId": "TNN",
            "Districtname": "Thị xã Sông Công",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNNC",
            "CityId": "TNN",
            "Districtname": "Định Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNND",
            "CityId": "TNN",
            "Districtname": "Võ Nhai",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNNE",
            "CityId": "TNN",
            "Districtname": "Phú Lương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNNF",
            "CityId": "TNN",
            "Districtname": "Đồng Hỷ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNNG",
            "CityId": "TNN",
            "Districtname": "Đại Từ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNNH",
            "CityId": "TNN",
            "Districtname": "Phú Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TNNI",
            "CityId": "TNN",
            "Districtname": "Phổ Yên",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 192,
                "BranchsAdd": "Tiểu khu 4, TT. Ba Hàng, H. Phổ Yên, Thái Nguyên",
                "BranchsCodePos": "CS_0000247",
                "BranchsShortAdd": "Tiểu khu 4, TT. Ba Hàng",
                "Districtid": "TNNI",
                "Cityid": "TNN",
                "Status": "A",
                "GroupId": 1
            }]
        }]
    }, {
        "Cityid": "THO",
        "Cityname": "Thanh Hoá",
        "AreaId": 2,
        "Top": 54,
        "ListBranchDistrictRivets": [{
            "DistrictId": "THOA1",
            "CityId": "THO",
            "Districtname": "Thành phố Thanh Hóa",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 281,
                "BranchsAdd": "620B-620C Quang Trung 2, P. Đông Vệ, TP. Thanh Hóa",
                "BranchsCodePos": "CS_0000352",
                "BranchsShortAdd": "620B-620C Quang Trung 2, P. Đông Vệ",
                "Districtid": "THOA1",
                "Cityid": "THO",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 325,
                "BranchsAdd": "418 Bà Triệu, P. Đông Thọ, TP. Thanh Hóa",
                "BranchsCodePos": "CS_0000393",
                "BranchsShortAdd": "418 Bà Triệu, P. Đông Thọ",
                "Districtid": "THOA1",
                "Cityid": "THO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "THOa2",
            "CityId": "THO",
            "Districtname": "Tĩnh Gia",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 277,
                "BranchsAdd": "33 Quang Trung, Tiểu khu 6, TT. Tĩnh Gia, Thanh Hóa",
                "BranchsCodePos": "CS_0000344",
                "BranchsShortAdd": "33 Quang Trung, Tiểu khu 6",
                "Districtid": "THOa2",
                "Cityid": "THO",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 356,
                "BranchsAdd": "Thôn Bắc Hải, Hải Thượng, Tĩnh Gia, Thanh Hóa",
                "BranchsCodePos": "CS_0000422",
                "BranchsShortAdd": "Thôn Bắc Hải, Hải Thượng, Tĩnh Gia, Thanh Hóa",
                "Districtid": "THOa2",
                "Cityid": "THO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "THOC",
            "CityId": "THO",
            "Districtname": "Bỉm Sơn",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 254,
                "BranchsAdd": "01 Trần Phú, P. Ngọc Trạo, TX. Bỉm Sơn, Thanh Hoá",
                "BranchsCodePos": "CS_0000319",
                "BranchsShortAdd": "01 Trần Phú, P. Ngọc Trạo",
                "Districtid": "THOC",
                "Cityid": "THO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "THOD1",
            "CityId": "THO",
            "Districtname": "Sầm Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOD2",
            "CityId": "THO",
            "Districtname": "Mường Lát",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOE",
            "CityId": "THO",
            "Districtname": "Quan Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOF",
            "CityId": "THO",
            "Districtname": "Quan Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOG",
            "CityId": "THO",
            "Districtname": "Bá Thước",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOH",
            "CityId": "THO",
            "Districtname": "Cẩm Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOI",
            "CityId": "THO",
            "Districtname": "Lanh Chánh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOJ",
            "CityId": "THO",
            "Districtname": "Thạch Thành",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOK",
            "CityId": "THO",
            "Districtname": "Ngọc Lạc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOL",
            "CityId": "THO",
            "Districtname": "Thường Xuân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOM",
            "CityId": "THO",
            "Districtname": "Như Xuân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THON",
            "CityId": "THO",
            "Districtname": "Như Thanh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOO",
            "CityId": "THO",
            "Districtname": "Vĩnh Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOP",
            "CityId": "THO",
            "Districtname": "Hà Trung",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOQ",
            "CityId": "THO",
            "Districtname": "Nga Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOR",
            "CityId": "THO",
            "Districtname": "Yên Định",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOS",
            "CityId": "THO",
            "Districtname": "Thọ Xuân",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOT",
            "CityId": "THO",
            "Districtname": "Hậu Lộc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOU",
            "CityId": "THO",
            "Districtname": "Thiệu Hoá",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOV",
            "CityId": "THO",
            "Districtname": "Hoằng Hoá",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOW",
            "CityId": "THO",
            "Districtname": "Đông Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOX",
            "CityId": "THO",
            "Districtname": "Triệu Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "THOY",
            "CityId": "THO",
            "Districtname": "Quảng Xương",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 282,
                "BranchsAdd": "31 Phố Tân Phong, TT. Quảng Xương, TP. Thanh Hoá",
                "BranchsCodePos": "CS_0000353",
                "BranchsShortAdd": "31 Phố Tân Phong, TT. Quảng Xương",
                "Districtid": "THOY",
                "Cityid": "THO",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "THOZ",
            "CityId": "THO",
            "Districtname": "Nông Cống",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "TGG",
        "Cityname": "Tiền Giang",
        "AreaId": 1,
        "Top": 55,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TGGA",
            "CityId": "TGG",
            "Districtname": "Thành phố Mỹ Tho",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 97,
                "BranchsAdd": "289 Ấp Bắc, P.5, TP. Mỹ Tho, Tiền Giang",
                "BranchsCodePos": "CS_0000128",
                "BranchsShortAdd": "289 Ấp Bắc, P.5, TP. Mỹ Tho",
                "Districtid": "TGGA",
                "Cityid": "TGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TGGB",
            "CityId": "TGG",
            "Districtname": "Thị xã Gò Công",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 203,
                "BranchsAdd": "Số 7, Nguyễn Huệ, Khóm 1, P.1, TX. Gò Công, Tiền Giang",
                "BranchsCodePos": "CS_0000263",
                "BranchsShortAdd": "Số 7, Nguyễn Huệ, Khóm 1, P.1",
                "Districtid": "TGGB",
                "Cityid": "TGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TGGC",
            "CityId": "TGG",
            "Districtname": "Tân Phước",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TGGD",
            "CityId": "TGG",
            "Districtname": "Châu Thành - TG",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 300,
                "BranchsAdd": "Ấp Tân Phong, X. Tân Lý Tây, H. Châu Thành, Tiền Giang",
                "BranchsCodePos": "CS_0000369",
                "BranchsShortAdd": "Ấp Tân Phong, X. Tân Lý Tây",
                "Districtid": "TGGD",
                "Cityid": "TGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TGGE",
            "CityId": "TGG",
            "Districtname": "Cai Lậy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TGGF",
            "CityId": "TGG",
            "Districtname": "Chợ Gạo",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TGGG1",
            "CityId": "TGG",
            "Districtname": "Gò Công Đông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TGGG2",
            "CityId": "TGG",
            "Districtname": "Cái Bè",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 296,
                "BranchsAdd": "Khu 3, TT. Cái Bè, H. Cái Bè, Tiền Giang",
                "BranchsCodePos": "CS_0000365",
                "BranchsShortAdd": "Khu 3, Thị trấn Cái Bè",
                "Districtid": "TGGG2",
                "Cityid": "TGG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TGGH",
            "CityId": "TGG",
            "Districtname": "Gò Công Tây",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "TVH",
        "Cityname": "Trà Vinh",
        "AreaId": 1,
        "Top": 56,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TVHA",
            "CityId": "TVH",
            "Districtname": "Thành Phố Trà Vinh",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 162,
                "BranchsAdd": "12 Điện Biên Phủ, P.6, TP. Trà Vinh",
                "BranchsCodePos": "CS_0000209",
                "BranchsShortAdd": "12 Điện Biên Phủ, P.6",
                "Districtid": "TVHA",
                "Cityid": "TVH",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TVHB",
            "CityId": "TVH",
            "Districtname": "Càng Long",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TVHC",
            "CityId": "TVH",
            "Districtname": "Châu Thành - TV",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TVHD",
            "CityId": "TVH",
            "Districtname": "Cầu Kè",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TVHE",
            "CityId": "TVH",
            "Districtname": "Tiểu Cần",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TVHF",
            "CityId": "TVH",
            "Districtname": "Cầu Ngang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TVHG",
            "CityId": "TVH",
            "Districtname": "Trà Cú",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TVHH",
            "CityId": "TVH",
            "Districtname": "Duyên Hải",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "TQG",
        "Cityname": "Tuyên Quang",
        "AreaId": 3,
        "Top": 57,
        "ListBranchDistrictRivets": [{
            "DistrictId": "TQGA",
            "CityId": "TQG",
            "Districtname": "Thành Phố Tuyên Quang",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 197,
                "BranchsAdd": "133- 135 Bình Thuận, P. Tân Quang, TP. Tuyên Quang",
                "BranchsCodePos": "CS_0000258",
                "BranchsShortAdd": "133- 135 Bình Thuận, P. Tân Quang",
                "Districtid": "TQGA",
                "Cityid": "TQG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "TQGB",
            "CityId": "TQG",
            "Districtname": "Hà Nang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TQGC",
            "CityId": "TQG",
            "Districtname": "Chiêm Hóa",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TQGD",
            "CityId": "TQG",
            "Districtname": "Hàm Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TQGE",
            "CityId": "TQG",
            "Districtname": "Yên Sơn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "TQGF",
            "CityId": "TQG",
            "Districtname": "Sơn Dương",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "VLG",
        "Cityname": "Vĩnh Long",
        "AreaId": 1,
        "Top": 58,
        "ListBranchDistrictRivets": [{
            "DistrictId": "VLGA",
            "CityId": "VLG",
            "Districtname": "Thành phố Vĩnh Long",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 108,
                "BranchsAdd": "01 Nguyễn Huệ, P.2, TP. Vĩnh Long",
                "BranchsCodePos": "CS_0000131",
                "BranchsShortAdd": "01 Nguyễn Huệ, P.2, TP. Vĩnh Long",
                "Districtid": "VLGA",
                "Cityid": "VLG",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 287,
                "BranchsAdd": "Số 64, Trần Phú, P.4, Vĩnh Long",
                "BranchsCodePos": "CS_0000355",
                "BranchsShortAdd": "Số 64, Trần Phú, P.4",
                "Districtid": "VLGA",
                "Cityid": "VLG",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VLGB",
            "CityId": "VLG",
            "Districtname": "Long Hồ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VLGC",
            "CityId": "VLG",
            "Districtname": "Măng Thít",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VLGD",
            "CityId": "VLG",
            "Districtname": "Bình Minh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VLGE",
            "CityId": "VLG",
            "Districtname": "Tam Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VLGF",
            "CityId": "VLG",
            "Districtname": "Trà Ôn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VLGG",
            "CityId": "VLG",
            "Districtname": "Vũng Liêm",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "VPC",
        "Cityname": "Vĩnh Phúc",
        "AreaId": 3,
        "Top": 59,
        "ListBranchDistrictRivets": [{
            "DistrictId": "VPC1",
            "CityId": "VPC",
            "Districtname": "Thị xã Phúc Yên",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 138,
                "BranchsAdd": "90 Trần Hưng Đạo, P.Trưng Trắc, TX.Phúc Yên, Vĩnh Phúc",
                "BranchsCodePos": "CS_0000183",
                "BranchsShortAdd": "90 Trần Hưng Đạo, P. Trưng Trắc, TX. Phúc Yên",
                "Districtid": "VPC1",
                "Cityid": "VPC",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 285,
                "BranchsAdd": "TDP 3, Trường Chinh, P. Đông Xuân, TX. Phúc Yên, Vĩnh Phúc",
                "BranchsCodePos": "CS_0000358",
                "BranchsShortAdd": "TDP 3, Trường Chinh, P. Đông Xuân",
                "Districtid": "VPC1",
                "Cityid": "VPC",
                "Status": "A",
                "GroupId": 1
            }, {
                "BranchsId": 308,
                "BranchsAdd": "123 Xuân Mai, P. Phúc Thắng, TX. Phúc Yên, Vĩnh Phúc",
                "BranchsCodePos": "CS_0000377",
                "BranchsShortAdd": "123 Xuân Mai, P. Phúc Thắng",
                "Districtid": "VPC1",
                "Cityid": "VPC",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VPC2",
            "CityId": "VPC",
            "Districtname": "Tam Đảo",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VPCA",
            "CityId": "VPC",
            "Districtname": "Thành phố Vĩnh Yên",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 106,
                "BranchsAdd": "01 Mê Linh, P. Liên Bảo, TP. Vĩnh Yên, Vĩnh Phúc",
                "BranchsCodePos": "CS_0000130",
                "BranchsShortAdd": "01 Mê Linh, P. Liên Bảo, TP. Vĩnh Yên",
                "Districtid": "VPCA",
                "Cityid": "VPC",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VPCB",
            "CityId": "VPC",
            "Districtname": "Lập Thạch",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 298,
                "BranchsAdd": "Khu 11, TT. Lập Thạch, H. Lập Thạch, Vĩnh Phúc",
                "BranchsCodePos": "CS_0000368",
                "BranchsShortAdd": "Khu 11, Thị Trấn Lập Thạch",
                "Districtid": "VPCB",
                "Cityid": "VPC",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "VPCC",
            "CityId": "VPC",
            "Districtname": "Tam Dương",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VPCD",
            "CityId": "VPC",
            "Districtname": "Bình Xuyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VPCE",
            "CityId": "VPC",
            "Districtname": "Vĩnh Tường",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VPCF",
            "CityId": "VPC",
            "Districtname": "Yên Lạc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "VPCG",
            "CityId": "VPC",
            "Districtname": "Mê Linh",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "YBI",
        "Cityname": "Yên Bái",
        "AreaId": 3,
        "Top": 60,
        "ListBranchDistrictRivets": [{
            "DistrictId": "YBIA",
            "CityId": "YBI",
            "Districtname": "Thành phố Yên Bái",
            "Top": 0,
            "LiBranchRivets": [{
                "BranchsId": 109,
                "BranchsAdd": "763 Điện Biên, P. Đồng Tâm, TP. Yên Bái",
                "BranchsCodePos": "CS_0000140",
                "BranchsShortAdd": "763 Điện Biên, P. Đồng Tâm, TP. Yên Bái",
                "Districtid": "YBIA",
                "Cityid": "YBI",
                "Status": "A",
                "GroupId": 1
            }]
        }, {
            "DistrictId": "YBIB",
            "CityId": "YBI",
            "Districtname": "Thị xã Nghĩa Lộ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBIC",
            "CityId": "YBI",
            "Districtname": "Lục Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBID",
            "CityId": "YBI",
            "Districtname": "Văn Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBIE",
            "CityId": "YBI",
            "Districtname": "Mù Cang Chải",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBIF1",
            "CityId": "YBI",
            "Districtname": "Trấn Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBIF2",
            "CityId": "YBI",
            "Districtname": "Yên Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBIH",
            "CityId": "YBI",
            "Districtname": "Văn Chấn",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "YBII",
            "CityId": "YBI",
            "Districtname": "Trạm Tấu",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "CBG",
        "Cityname": "Cao Bằng",
        "AreaId": 3,
        "Top": 1000,
        "ListBranchDistrictRivets": [{
            "DistrictId": "CBGA",
            "CityId": "CBG",
            "Districtname": "Thị Xã Cao Bằng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGB",
            "CityId": "CBG",
            "Districtname": "Bảo Lạc",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGC",
            "CityId": "CBG",
            "Districtname": "Bảo Lâm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGD",
            "CityId": "CBG",
            "Districtname": "Hà Quảng",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGE",
            "CityId": "CBG",
            "Districtname": "Thông Nông",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGF",
            "CityId": "CBG",
            "Districtname": "Trà Lĩnh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGG",
            "CityId": "CBG",
            "Districtname": "Trùng Khánh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGH",
            "CityId": "CBG",
            "Districtname": "Nguyên Bình",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGI",
            "CityId": "CBG",
            "Districtname": "Hoà An",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGJ",
            "CityId": "CBG",
            "Districtname": "Quảng Uyên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGK",
            "CityId": "CBG",
            "Districtname": "Hạ Lang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGL",
            "CityId": "CBG",
            "Districtname": "Thạch An",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "CBGM",
            "CityId": "CBG",
            "Districtname": "Phục Hoà",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "HTY",
        "Cityname": "Hà Tây",
        "AreaId": 3,
        "Top": 1000,
        "ListBranchDistrictRivets": []
    }, {
        "Cityid": "HUG",
        "Cityname": "Hậu Giang",
        "AreaId": 1,
        "Top": 1000,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HUGA",
            "CityId": "HUG",
            "Districtname": "Thị xã Vị Thanh",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUGB",
            "CityId": "HUG",
            "Districtname": "Vị Thủy",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUGC",
            "CityId": "HUG",
            "Districtname": "Long Mỹ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUGE",
            "CityId": "HUG",
            "Districtname": "Châu Thành A",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUGF",
            "CityId": "HUG",
            "Districtname": "Châu Thành - HG",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HUGG",
            "CityId": "HUG",
            "Districtname": "Phụng Hiệp",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }, {
        "Cityid": "HYN",
        "Cityname": "Hưng Yên",
        "AreaId": 3,
        "Top": 1000,
        "ListBranchDistrictRivets": [{
            "DistrictId": "HYNA",
            "CityId": "HYN",
            "Districtname": "Thị Xã Hưng Yên",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNB",
            "CityId": "HYN",
            "Districtname": "Văn Lâm",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNC",
            "CityId": "HYN",
            "Districtname": "Mỹ Hào",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYND",
            "CityId": "HYN",
            "Districtname": "Yên Mỹ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNE",
            "CityId": "HYN",
            "Districtname": "Văn Giang",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNF",
            "CityId": "HYN",
            "Districtname": "Khoái Châu",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNG",
            "CityId": "HYN",
            "Districtname": "Ân Thi",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNH",
            "CityId": "HYN",
            "Districtname": "Kim Động",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNI",
            "CityId": "HYN",
            "Districtname": "Phù Cừ",
            "Top": 0,
            "LiBranchRivets": []
        }, {
            "DistrictId": "HYNJ",
            "CityId": "HYN",
            "Districtname": "Tiên Lữ",
            "Top": 0,
            "LiBranchRivets": []
        }]
    }];
