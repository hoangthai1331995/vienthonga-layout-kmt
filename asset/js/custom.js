 $(document).ready(function () {
     $('.popup-with-zoom-anim').magnificPopup({
         type: 'inline',

         fixedContentPos: false,
         fixedBgPos: true,

         overflowY: 'auto',

         closeBtnInside: true,
         preloader: false,

         midClick: true,
         removalDelay: 300,
         mainClass: 'my-mfp-zoom-in',

         callbacks: {
             open: function () {

                 $('.scroll').perfectScrollbar('update');

             },
             close: function () {}
         }
     });



     var goToTop = {
        goToTopEl: $('#gotoTop'),

        init: function() {
            goToTop.goToTopEl.click(function() {
                $('body,html').stop(true).animate({
                    scrollTop: 0
                }, 400);
                return false;
            });

            $(window).scroll(function() {
                if ($(window).scrollTop() > 450) {
                    goToTop.goToTopEl.fadeIn();
                } else {
                    goToTop.goToTopEl.fadeOut();
                }
            });
        },
    };

     goToTop.init();



 });

 function horiNav(type, offset) {

     if ($(".horinav").length) {

         var hori = $(".horinav__list");
         var horiActive = hori.find(".active").eq(0);

         // defind width
         var wContainer = $(".horinav").width(); //ob
         var wActive = horiActive.width(); //a1a2
         var wScrollLeft = hori.scrollLeft();
         var wLeftActive = horiActive.offset().left + wScrollLeft; //a0a1 = Oa1
         var wRightActive = wLeftActive + wActive; //a0a2 = Oa2
         var wMargin = hori.offset().left;
         var x;


         switch (type) {
             case 'left':
                 x = wLeftActive - wMargin - offset;
                 hori.addClass("visible").animate({
                     scrollLeft: x
                 }, 200);
                 break;
             case 'right':
                 x = wLeftActive - (wContainer - wActive - offset);
                 hori.addClass("visible").animate({
                     scrollLeft: x
                 }, 200);
                 break;
             default:

                 x = wLeftActive - (wContainer - wActive) / 2 - wMargin;
                 hori.addClass("visible").animate({
                     scrollLeft: x
                 }, 200);
                 break;
         }

     }


 }

 horiNav();

 function handlePerfectscroll() {
     $('.scroll').each(function () {
         $(this).perfectScrollbar({
             suppressScrollX: true
         });
     });
 }

 handlePerfectscroll();

//  Countdown Timers

//  if ($('.countdown').length) {
//      $('.countdown').each(function () {
//          var date = $(this).attr('data-date');
//          $(this).countdown(date, function (event) {
//              $(this).html(event.strftime(
//                  '<div class="element"> <div class="num">%D</div> <div class="txt">Ngày</div> </div>' +
//                  '<div class="element"> <div class="num">%H</div> <div class="txt">Giờ</div> </div>' +
//                  '<div class="element"> <div class="num">%M</div> <div class="txt">Phút</div> </div>' +
//                  '<div class="element"> <div class="num">%S</div> <div class="txt">Giây</div> </div>'

//              ));
//          });
//      });
//  }

//  if ($('.countdown2').length) {
//      $('.countdown2').each(function () {
//          var date = $(this).attr('data-date');
//          $(this).countdown(date, function (event) {
//              var $this = $(this).html(event.strftime('' +
//                  '<div class="element"><span class="box-timer">%D</span>  <span class="label">Ngày</span></div> ' +
//                  '<div class="element"><span class="box-timer">%H</span>  <span class="label">Giờ</span></div> ' +
//                  '<div class="element"><span class="box-timer">%M</span> <span class="label">Phút</span></div> ' +
//                  '<div class="element"><span class="box-timer">%S</span> <span class="label">Giây</span></div>'));
//          });
//      });
//  }




//  $(".product-slick").slick({
//      dots: false,
//      infinite: false,
//      slidesToShow: 1,
//      adaptiveHeight: true,
//      autoplay: true,
//      arrows: true,
//      autoplaySpeed: 5000,
//      prevArrow: '<div class="product-slick-prev"> <img src="https://cdn1.vienthonga.vn/image/2017/3/1/100000_prev.png" alt=""></div>',
//      nextArrow: '<div class="product-slick-next"><img src="https://cdn1.vienthonga.vn/image/2017/3/1/100000_next.png" alt=""></div>'
//  });


//   $(".sp-slide").slick({
//      dots: true,
//      infinite: false,
//      speed: 300,
//      slidesToShow: 4,
//      slidesToScroll: 1,
//      adaptiveHeight: true,
//      arrows: true,

//      responsive: [{
//              breakpoint: 1024,
//              settings: {
//                  slidesToShow: 4,
//                  slidesToScroll: 1,
//              }
//          }, {
//              breakpoint: 600,
//              settings: {
//                  slidesToShow: 3,
//                  slidesToScroll: 1
//              }
//          }, {
//              breakpoint: 480,
//              settings: {
//                  slidesToShow: 1,
//                  slidesToScroll: 1
//              }
//          }

//      ]
//  });


$('#listTab-giasoc').on('click','li',function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');    

})

$("#modal-datmua").on("hidden.bs.modal", function () {
    app2.modalData.listColor = [];
    app2.modalData.selectColor = '';
    app2.pushObjModal.ProductColorcode = '';
});
$('.sp-slide4').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
  $(".tab-slide .active").removeClass("active");
  $( "#tab-slide-" + nextSlide ).addClass( "active" );
});
$('.href-scroll').on('click', function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 56)
        }, 600);
        $( "#menu1" ).click();
        return false;
      }
    }
});