const site = {
    port:        8088,        // cổng server local sẻ sử dụng
    contentRoot: './content', // thư mục chứa content file cho metalsmith
    buildRoot:   './build',   // thư mục chứa output của metalsmith
    layoutRoot:  './layout',  // thư mục layout của handlebars

    // thư mục chứa style của site, sẽ build vào ${buildRoot}/css/
    styleRoot: './style',

    // thư mục chứa style của site, sẽ build vào ${buildRoot}/js/
    scriptRoot: './script',

    // thư mục chứa các script, css, fonts, image của vendor
    // tât cả sẽ được copy (giữ nguyên câu trúc) qua ${buildRoot}
    // ở chế độ production cũng sẽ không minify
    assetRoot: './asset',

    //thư mục chứa tất các các file json chứa dữ liệu dùng chung, không định nghĩa được trong file .md
    //gồm 3 file json chính
    //global.json chứa thông tin chung về website
    //menu.json chứa thông tin về menu của website
    //footer.json chứa thông tin về footer của website
    metadataRoot : './content/metadata',
    metadata: {

        /**
         * Link Go live 
         */
             // url: 'https://vienthonga.vn/',
             //    urlCurrentEvent: 'oppo-a7.html',
        
        /**
         * Link Testing
         */
            // url: 'https://event.vienthonga.vn/',
            // urlCurrentEvent: 'huawei-mate-20.html',
            
        /**
         * Link Dev Local
         */
            url: './',
         	urlCurrentEvent: './',
         



        /*title:'Giới thiệu bạn',
        metaDescription: 'Giới thiệu bạn',
        metaKeywords: 'Giới thiệu bạn',
        metaAuthor:'Vien Thong A'*/

        title:'Đặt trước Oppo A7 - Thiết kế thời thượng, Năng lượng tối đa',
        metaDescription: 'Từ ngày 15/08 - 24/8 đặt trước Oppo A7 thiết kế thời thượng, năng lượng tối đa tại Viễn Thông A nhận ngay quà tặng hấp dẫn  trả góp 0%. Liên hệ : 1900 6766',
        metaKeywords: 'OPPO A7, A7, smartphone OPPO A7, điện thoại OPPO A7, OPPO 2018, OPPO, điện thoại OPPO, smartphone OPPO, pin khủng, camera kép, màn hình giọt nước',

        metaAuthor:'Vien Thong A',
        metaImage:"",
        updateTime:"17/10/2018 10:17:06 AM"
    }



};

site.script = {
    concat:     true,     // concat == true sẽ nhập các file script lại thành 1 file duy nhất
    concatName: 'app.js', // tên của file script sau khi nhập, mặc định là app.js
    files:      [
        // ví dụ
        "bower_components/jquery/dist/jquery.js",
        "bower_components/jquery.easing/js/jquery.easing.js",
        "bower_components/bootstrap-sass/assets/javascripts/bootstrap.js",
        "bower_components/slick-carousel/slick/slick.js",
        "bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js",
        "bower_components/magnific-popup/dist/jquery.magnific-popup.js",
        "bower_components/sweetalert/dist/sweetalert.min.js",
        // "bower_components/lazysizes/lazysizes.min.js",
        "bower_components/jquery.countdown/dist/jquery.countdown.min.js",
        // "asset/js/parallax.min.js",
        // muốn concat đúng thứ tự thì phải define path
        `${site.scriptRoot}/!(app).js` // các file có tên khác 'app.js'
    ]
};

site.style = {
    sass:         {
        // đường dẫn tơi các thư viện sass, có thể load bằng @import
        includePaths: [
            'bower_components',
            // ví dụ
            'bower_components/bootstrap-sass/assets/stylesheets',
            // "bower_components/motion-ui/src",
            // "bower_components/SpinKit/scss"
        ]
    },
    autoprefixer: {
        browsers: ['last 2 version', '> 1%' ,'ios 7']
    }
};

// define và config các plugin của metalsmith
site.metalsmith = {
    'metalsmith-metadata-directory': {
      'directory': `${site.metadataRoot}/**/*.json`
    },

    'metalsmith-drafts':        {
        '_enable': false
    },
    'metalsmith-matters':       {
        '_enable': true,
        'delims':  ['---json', '---'],
        'options': {
            'lang': 'json'
        }
    },

    'metalsmith-markdown':      {
        '_enable':     true,
        'smartypants': true,
        'smartLists':  true,
        'gfm':         true,
        'tables':      true
    },

    'metalsmith-collections':   {
        '_enable': true,
        // collection theo file pattern + test limit
        'blog':    {
            'pattern': 'blog/**/*.html',
            'sortBy':  'date',
            'reverse': true,
        },
        // collection theo key trong metadata `"collection": "baiviet"`
        'baiviet': {
            'sortBy':  'date',
            'reverse': true
        }
    },



    'metalsmith-pagination':    {
        '_enable': true,
        'collections.blog':    {
            'perPage':   1,
            'layout':    'blog-list.html',
            'first':     'blog/index.html',
            'path':      'blog/:num/index.html',
            'noPageOne': true,
            'pageMetadata': {
              'title': 'Title of metalsmith-pagination file site.js'
            }
        },
        // // test filter
        // 'collections.baiviet': {
        //     'perPage':   1,
        //     'layout':    'blog.html',
        //     'first':     'baiviet/index.html',
        //     'path':      'baiviet/:num/index.html',
        //     'filter':    meta => {
        //         return meta.dacbiet === false;
        //     },
        //     'noPageOne': true
        // }
    },

    'metalsmith-permalinks':    {
        '_enable':  true,
        // default config
        'pattern':  ':slug',
        'relative': false,
        // config rieng cho 1 collection
        linksets:   [{
            match:   {collection: 'blog'},
            pattern: 'blog/:slug'
        }]
    },

    'metalsmith-layouts':       {
        '_enable':   true,
        'engine':    'handlebars',
        'directory': `${site.layoutRoot}`,
        'partials':  `${site.layoutRoot}/partial`
    },

    'metalsmith-html-minifier': {
        '_enable':               true,
        'removeAttributeQuotes': false,
        'keepClosingSlash':      true,
        'removeRedundantAttributes': false
    }
};

module.exports = site;
